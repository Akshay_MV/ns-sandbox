declare module "@salesforce/apex/util.getPickListValues" {
  export default function getPickListValues(param: {ObjName: any, FieldName: any}): Promise<any>;
}
