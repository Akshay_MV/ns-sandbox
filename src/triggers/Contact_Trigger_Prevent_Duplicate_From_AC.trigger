trigger Contact_Trigger_Prevent_Duplicate_From_AC on Contact (before insert, before update) {

    Map<String, Contact> contactMap = new Map<String, Contact>();
    for (Contact Contact : System.Trigger.new) {        
        // Make sure we don't treat an AC_ID__c that  
        // isn't changing during an update as a duplicate.  
        if ((Contact.AC_ID__c != null) && (System.Trigger.isInsert || (Contact.AC_ID__c !=System.Trigger.oldMap.get(Contact.Id).AC_ID__c))) {
           // Make sure another new Contact isn't also a duplicate  
            if (contactMap.containsKey(Contact.AC_ID__c)) {
                Contact.AC_ID__c.addError('A Contact with this AC_ID');
            } else {
                contactMap.put(Contact.AC_ID__c, Contact);
            }
        }
    }   
    // Using a single database query, find all the Contacts in      
    // the database that have the same AC_ID__c address as any     
    // of the Contacts being inserted or updated.     
    for (Contact contact : [SELECT AC_ID__c FROM Contact WHERE AC_ID__c IN :contactMap.KeySet()]) {
        Contact newContact = contactMap.get(Contact.AC_ID__c);
        newContact.AC_ID__c.addError('A Contact with this AC_ID');
    }
}