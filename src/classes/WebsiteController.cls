/*******************************************
 * Visualforce page: Nature's Select
 * Description: 
 * 
*******************************************/
public class WebsiteController {
    
    // BANNER 
    public Boolean showBanner {get;set;}

    // LIST OF PRODUCTS
    public List<Product2> productList {get;set;}
    public List<Product2> productSearchList {get;set;}
    public List<Product2> quickShopList {get;set;}
    public List<Product2> featuredProductList {get;set;}
    public List<Product2> AllProduct {get;set;}
    public List<Testimonial__c> testimonialList {get;set;}

    public List<Website_Filter_Data__c> websiteFilterDataList {get; set;}
    
    // INITIALIZE ALL VARIABLES
    public Website_Filter_Data__c HomePageMainText {get;set;}//HOMEPAGE Image Text
    public List<Website_Filter_Data__c> TrustRibbon {get;set;}//TRUST RIBBON LOGOS
    public List<Website_Filter_Data__c> WellnessSolutionText {get;set;}//WellnesSolutionText
    public List<Website_Filter_Data__c> WellnessSolutionImg {get;set;}//WellnessSolutionImages
    public List<Website_Filter_Data__c> PetFoodText {get;set;}//Pet Food TEXT
    public List<Website_Filter_Data__c> PetFoodImg {get;set;}//Pet Food Images
    public List<Website_Filter_Data__c> RethinkingText {get;set;}//Rethinking Pet Food text
    public List<Website_Filter_Data__c> RethinkingImg {get;set;}//Rethinking Pet Food Text
    public List<Website_Filter_Data__c> WhyUsHeading {get;set;}//Heading and subheading of WHYUS
    public List<Website_Filter_Data__c> WhyUsContent {get;set;}//Content of Whyus
    // public List<Website_Filter_Data__c> WhyUsContentImg {get;set;}//Images of Why us Content
    public List<Website_Filter_Data__c> NeedAdviceText {get;set;}//Need Advice TExt
    public List<Website_Filter_Data__c> NeedAdviceImg {get;set;}//Need Advice Images
    public Website_Filter_Data__c MealsNumber {get;set;}//Meals Delivered Number
    public String MealsDeliveredNumber {get;set;}    

    public void init(){
        
        showBanner = true;

        productList = new List<Product2>();
        productSearchList = new List<Product2>();
        quickShopList = new List<Product2>();
        featuredProductList = new List<Product2>();
        AllProduct = new List<Product2>();
        testimonialList = new List<Testimonial__c>();
    }

    // CONSTRUCTOR
    public WebsiteController(){
        init();
        MainImageText();
        TrustRibbonLogos();
        WellnessSolution();
        PetFood();
        Rethinking();
        WhyUs();
        NeedAdvice();
        MealsNumber();
        // QueryAllData();

        productList = database.query(util.queryData('Product2','',' ORDER BY CreatedDate DESC LIMIT 10'));
        productSearchList.addAll(productList);
        // testimonialList = database.query(util.QueryData('Testimonial__c','',''));
        testimonialList = [SELECT Id, Name, Title__c, Description__c, Date__c, verified_customer__c, Address__c, Star_Rating__c, Review_Image__c, Customer__c,Customer__r.Name FROM Testimonial__c];
        System.debug('Rating-->'+testimonialList[0].Star_Rating__c);
        // Check wether banner should be shown or not.
        // showBanner = Database.query(util.queryData('ObjName', 'parentFields', ' LIMIT 1 '));
    }

    public void QueryAllData(){
        websiteFilterDataList = new List<Website_Filter_Data__c>();
        websiteFilterDataList = [ SELECT Id,Text_Data__c,Font_Color__c, Font_Size__c FROM Website_Filter_Data__c ];

        // HomePageMainText = 
    }

    // METHOD TO SEARCH PRODUCT
    public List<Product2> searchProduct(String Category){
        List<Product2> filteredCategoryProductList = new List<Product2>();
        for(Product2 pr : AllProduct){
            if(pr.Slick__Cart_Subcategory__c == Category){
                filteredCategoryProductList.add(pr);
            }
        }

        return filteredCategoryProductList;
    }

    // TO DISPLAY THE PRODUCT ON QUICK SHOP
    public void displayProduct(String Category, String section){
        
        // NEED TO DEFINE WHICH PRODUCT SHOULD SHOW UP ON THE QUICK SHOP SECTION
        // TBD...
        quickShopList = database.query(util.queryData('Product2','',' ORDER BY CreatedDate DESC LIMIT 10'));

        featuredProductList = database.query(util.queryData('Product2','',' ORDER BY CreatedDate DESC LIMIT 10'));
    }

    public void getAllProduct(){
        AllProduct = database.query(util.queryData('Product2','',' ORDER BY CreatedDate DESC LIMIT 10'));
    }

    //Get HomePage Image TEXT
    public void MainImageText(){
        HomePageMainText = new Website_Filter_Data__c();
        HomePageMainText = [SELECT Id,Text_Data__c,Font_Color__c, Font_Size__c, Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Home Page Image Text' AND Website_Filter__r.is_active__c = true];
    }

    public void TrustRibbonLogos(){
        TrustRibbon = new List<Website_Filter_Data__c>();
        TrustRibbon = [SELECT Id,Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Trust Ribbon Logo' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
    }

    public void WellnessSolution(){
        WellnessSolutionText = new List<Website_Filter_Data__c>();
        WellnessSolutionText = [SELECT Id,Text_Data__c,Font_Size__c,Font_Color__c,Sub_Text_Data__c,Sub_Font_Color__c,Sub_Font_Size__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Wellness Solutions Text' AND is_Active__c = true ORDER BY CreatedDate LIMIT 1];

        WellnessSolutionImg = new List<Website_Filter_Data__c>();
        WellnessSolutionImg = [SELECT Id,Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Wellness Solutions Images' AND is_Active__c = true ORDER BY CreatedDate];
    }

    public void PetFood(){
        PetFoodText = new List<Website_Filter_Data__c>();
        PetFoodText = [SELECT Id,Text_Data__c,Font_Size__c,Font_Color__c,Sub_Text_Data__c,Sub_Font_Size__c,Sub_Font_Color__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Pet Food Text' AND is_Active__c = true ORDER BY CreatedDate LIMIT 1];

        PetFoodImg = new List<Website_Filter_Data__c>();
        PetFoodImg = [SELECT Id,Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Pet Food Image' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
    }

    public void Rethinking(){
        RethinkingText = new List<Website_Filter_Data__c>();
        RethinkingText = [SELECT Id,Text_Data__c,Font_Size__c,Font_Color__c,Sub_Font_Color__c,Sub_Font_Size__c,Sub_Text_Data__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Rethinking Text' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate LIMIT 1];

        RethinkingImg = new List<Website_Filter_Data__c>();
        RethinkingImg = [SELECT Id,Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Rethinking Images' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
    }

    public void WhyUs(){
        WhyUsHeading = new List<Website_Filter_Data__c>();
        WhyUsHeading = [SELECT Id,Text_Data__c,Font_Size__c,Font_Color__c,Sub_Font_Color__c,Sub_Font_Size__c,Sub_Text_Data__c  FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Why Us Headings' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate LIMIT 1];

        WhyUsContent = new List<Website_Filter_Data__c>();
        WhyUsContent = [SELECT Id,Text_Data__c,Font_Size__c,Font_Color__c,Url__c,Sub_Font_Size__c,Sub_Font_Color__c,Sub_Text_Data__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Why us Content' AND is_Active__c = true ORDER BY CreatedDate];

        // WhyUsContentImg = new List<Website_Filter_Data__c>();
        // WhyUsContentImg = [SELECT Id,Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Why Us Content Images' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
    }

    public void NeedAdvice(){
        NeedAdviceText = new List<Website_Filter_Data__c>();
        NeedAdviceText = [SELECT Id,Text_Data__c,Font_Size__c,Font_Color__c,Sub_Text_Data__c,Sub_Font_Size__c,Sub_Font_Color__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Need Advice Text' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate LIMIT 1];

        NeedAdviceImg = new List<Website_Filter_Data__c>();
        NeedAdviceImg = [SELECT Id,Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Need Advice Images' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
    }

    public void MealsNumber(){
        MealsNumber = new Website_Filter_Data__c();
        MealsNumber = [SELECT Id,Text_Data__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Meals Delivered Number' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
        List<String> test = MealsNumber.Text_Data__c.split(',');        
        MealsDeliveredNumber = test[0];
        

    }
}