@isTest
global class TestCalloutMock implements HttpCalloutMock{  
    global HttpResponse respond(HTTPRequest req){
	        HttpResponse res = new HttpResponse();
	        res.setBody('{}');
	        res.setStatus('OK');
	        res.setStatusCode(200);
	         res.setBody('<Subscriber><subscriber_id><FirstName>kumar</FirstName><LastName>R</LastName><Email>ar@gmail.com</Email><Phone>7777777777</Phone><ReasonforContact>Associate of Arts in Business</ReasonforContact></lead><lead><FirstName>Balaji</FirstName><LastName>Ram</LastName><Email>ram@gmail.com</Email><Phone>999999991</Phone><ReasonforContact>Associate of Arts in Busines1s</ReasonforContact></lead></Subscriber>');
	        return res;
    }
}