@isTest
private class Test_NSTransGood_Ctrl{
    @isTest
    private static void testmethod1(){
        
        String ResponseCode;
        String Response;
        String ContactId;
        String AdultOrPuppy;
        Boolean shouldRedirect;
        String redirectUrl;
        
        Contact con = new Contact();
        con.LastName='lastname';
        insert con;
        
        PageReference pageRef = Page.NSTransGood;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('Response', 'Update');
        System.currentPageReference().getParameters().put('ResponseCode', 'Smooth');
        //System.currentPageReference().getParameters().put('ResponseCode', 'ContactMe');
        //System.currentPageReference().getParameters().put('ResponseCode', 'Issue');
        System.currentPageReference().getParameters().put('ContactId', con.Id);
        //System.currentPageReference().getParameters().put('AdultOrPuppy', 'Adult');
        //System.currentPageReference().getParameters().put('AdultOrPuppy', 'Puppy');
        
        
        Contact con2 = new Contact();
        con2.Id=con.Id;
        con2.GetM20Multi__c = System.now();
        update con2;
        if(Response!=null && Response == 'Update'){
           Contact con3 = new Contact();
           con3.Id=ContactId;
           con3.GetM20Multi__c = System.now();
           update con3;
        }
        If(ResponseCode!=null && ResponseCode=='Smooth')
        {
            Feedback__c feedback_Obj = new Feedback__c();
            feedback_Obj.NCTrans_Good__c=ResponseCode;
            feedback_Obj.Content_Reference__c ='Survey Email';
            feedback_Obj.Contact__c = ContactId;
            feedback_Obj.Time_Stamp__c =System.now();
            insert feedback_Obj; 
        }
        if(AdultOrPuppy!=null && AdultOrPuppy == 'Adult'){
           System.debug('Inside adult condition');
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.Content_Reference__c = 'Survey Email2CloneB';
           feedback_Obj.MultiPro__c ='MultiPro';
           feedback_Obj.FFS2Multi__c = System.now();
           feedback_Obj.Time_Stamp__c =System.now();
           feedback_Obj.Contact__c = ContactId;
           insert feedback_Obj;
           redirectUrl = 'https://nschicago.secure.force.com/NSCart__cart?quantity=1&id=01t0c000006JBUDAA4&add=1';
        }
	    NSTransGood_Ctrl controller = new NSTransGood_Ctrl();
        controller.InitPage();
    }
    @isTest
     private static void testmethod2(){
         PageReference pageRef = Page.NSTransGood;
         Test.setCurrentPage(pageRef);
         Contact con = new Contact();
         con.LastName='lastname';
         insert con;
         String AdultOrPuppy;
         String ContactId;
         String redirectUrl;
         String ResponseCode;
         System.currentPageReference().getParameters().put('ContactId', con.Id);
         System.currentPageReference().getParameters().put('AdultOrPuppy', 'Puppy');
         if(AdultOrPuppy!=null && AdultOrPuppy == 'Puppy'){
           System.debug('Inside adult condition');
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.Content_Reference__c = 'Survey Email2CloneB';
           feedback_Obj.MultiPro__c ='MultiPro';
           feedback_Obj.FFS2Multi__c = System.now();
           feedback_Obj.Time_Stamp__c =System.now();
           feedback_Obj.Contact__c = ContactId;
           insert feedback_Obj;
           redirectUrl = 'https://nschicago.secure.force.com/NSCart__cart?quantity=1&id=01t0c000006JBUDAA4&add=1';
        }
	    NSTransGood_Ctrl controller = new NSTransGood_Ctrl();
        controller.InitPage();
     }
    @isTest
    private static void testmethod3(){
         PageReference pageRef = Page.NSTransGood;
         Test.setCurrentPage(pageRef);
         Contact con = new Contact();
         con.LastName='lastname';
         insert con;
         String AdultOrPuppy;
         String ContactId;
         String redirectUrl;
         String ResponseCode;
         System.currentPageReference().getParameters().put('ContactId', con.Id);
        System.currentPageReference().getParameters().put('Response', 'Update');
         System.currentPageReference().getParameters().put('ResponseCode', 'Issues');
        if(ResponseCode!=null && ResponseCode == 'Issues'){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.Content_Reference__c ='Survey Email';
           feedback_Obj.NCTrans_Issues__c =ResponseCode;
           feedback_Obj.Contact__c = ContactId;
           feedback_Obj.Time_Stamp__c =System.now();
           insert feedback_Obj;
        }  
	    NSTransGood_Ctrl controller = new NSTransGood_Ctrl();
        controller.InitPage();
     }
    @isTest
    private static void testmethod4(){
         PageReference pageRef = Page.NSTransGood;
         Test.setCurrentPage(pageRef);
         Contact con = new Contact();
         con.LastName='lastname';
         insert con;
         String AdultOrPuppy;
         String ContactId;
         String redirectUrl;
         String ResponseCode;
         System.currentPageReference().getParameters().put('ContactId', con.Id);
         System.currentPageReference().getParameters().put('ResponseCode', 'ContactMe');
        if(ResponseCode!=null && ResponseCode == 'ContactMe'){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.Content_Reference__c ='Survey Email';
           feedback_Obj.NCTrans_Issues__c =ResponseCode;
           feedback_Obj.Contact__c = ContactId;
           feedback_Obj.Time_Stamp__c =System.now();
           insert feedback_Obj;
        }  
	    NSTransGood_Ctrl controller = new NSTransGood_Ctrl();
        controller.InitPage();
     }
   @isTest
     private static void testmethod5(){
         PageReference pageRef = Page.NSTransGood;
         Test.setCurrentPage(pageRef);
         Contact con = new Contact();
         con.LastName='lastname';
         insert con;
         String AdultOrPuppy;
         String ContactId;
         String redirectUrl;
         String ResponseCode;
         System.currentPageReference().getParameters().put('ContactId', con.Id);
         System.currentPageReference().getParameters().put('AdultOrPuppy', 'Adult');
         if(AdultOrPuppy!=null && AdultOrPuppy == 'Adult'){
           System.debug('Inside adult condition');
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.Content_Reference__c = 'Survey Email2CloneB';
           feedback_Obj.MultiPro__c ='MultiPro';
           feedback_Obj.FFS2Multi__c = System.now();
           feedback_Obj.Time_Stamp__c =System.now();
           feedback_Obj.Contact__c = ContactId;
           insert feedback_Obj;
           redirectUrl = 'https://nschicago.secure.force.com/NSCart__cart?quantity=1&id=01t0c000006JBUDAA4&add=1';
        }
	    NSTransGood_Ctrl controller = new NSTransGood_Ctrl();
        controller.InitPage();
     }
     private static testmethod void testvalidate(){
        PageReference pageRef = Page.Select_Email_Template;
        Test.setCurrentPage(pageRef);
        Account newAcc = new Account();
        newAcc.name='test';
        insert newAcc;
        Contact con=new Contact();
        con.lastname='Testing';
        con.email='test@test.com';
        insert con;
        Select_Email_Template_Ctrl sc = new Select_Email_Template_Ctrl();
        pageRef.getParameters().put('id',con.Id);
        sc.SendMail();
        sc.Cancel();
        sc.getMyPersonalTemplateOptions();
    }
}