global class MealsDeliveredSchedular implements Schedulable{
    global void execute(SchedulableContext sc){        
        Website_Filter_Data__c meals = new Website_Filter_Data__c();
        meals = [SELECT Id,Text_Data__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Meals Delivered Number' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
        List<String> MealsTextData = meals.Text_Data__c.split(',');
        String MNumber = MealsTextData[0];
        String MIncrement = MealsTextData[1];
        Integer n = Integer.valueOf(MNumber) + Integer.valueOf(MIncrement);
        MNumber = string.valueOf(n);
        meals.Text_Data__c = MNumber + ',' + MIncrement;        
        update meals;
    }   
}