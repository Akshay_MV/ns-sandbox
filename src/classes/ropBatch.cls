/**
 * ropBatch Class
 * @author: M. Wicherski, MK Partners 11/2012
 * @Description: Sends out ROP Mailer
 */

/*
	To Run:
			ropBatch job = new ropBatch();
			Id processId = Database.executeBatch(job);
	
	To Check Status:
			AsyncApexJob j = [select ApexClassId, CompletedDate, CreatedById, CreatedDate, 
								Id, JobItemsProcessed, JobType, MethodName, NumberOfErrors, Status, 
								TotalJobItems from AsyncApexJob where Id =:processId];
	
	Info about this Class
			ApexClass ac = [select ApiVersion, Body, BodyCrc, CreatedById, CreatedDate, Id, 
								IsValid, LastModifiedById, LastModifiedDate, LengthWithoutComments, Name, 
								NamespacePrefix, Status, SystemModstamp from ApexClass where Id = :j.ApexClassId];
*/
global with sharing class ropBatch  {
/*
	public static date runDate = system.today();

	// Queries to be used
	static datetime past24hours = system.now().addhours(-24);

	public static string getQueryString(){
		String x = 'select id,';
		x+= ' c.Days_till_Next_Food__c,';
		x+= ' ROP_Trigger_Field__c,';
		x+= ' ROP_Trigger_Field_5day__c';
		x+= ' from Contact c';
		x+= ' where c.active__c = true';
		x+= ' AND c.HasOptedOutOfEmail = false';
		x+= ' AND ((c.Days_till_Next_Food__c = 5';
		x+= ' AND c.Auto_Reorder__c = false)';
		x+= ' OR c.Days_till_Next_Food__c = 7)';
		
		system.debug('QUERY STRING IS: ' +x);
		return x;
	}

	public static final String DATA_SET_QUERY = getQueryString();

	// Query to fetch the data required by the job
	private String jquery; 

	global ropBatch() {
		this(DATA_SET_QUERY);
	}

	public class BatchApexJobException extends Exception {}
	public class InvalidObjectInstanceException extends Exception {}

	global ropBatch(String query) {
		try {
	    	// If no query set, throw exception
	    	if (query == null) {
	    		throw new BatchApexJobException('Query string is null.');
	    	}
	    	jquery = query;
	    	system.debug('QUERY RESULT: '+Database.query(jquery));
	    	
	    	//Check if the job is running already
	        ApexClass ac = [select Name from ApexClass where Name = 'ropBatch'];
	        List<AsyncApexJob> aajs = [select Status from AsyncApexJob where ApexClassId = :ac.Id and 
	        							JobType = 'batchApex' and Status in ('Queued', 'Processing')];
	        if (!aajs.isEmpty()) {
	        	throw new BatchApexJobException(aajs.size() + ' ropBatch job(s) already running or processing.');
			}
    	}
    	catch (Exception e) {
            throw e;
        }
	}

    //Method required by the framework to start the job execution.
    global Database.QueryLocator start(Database.BatchableContext ctx) {
        Database.QueryLocator qLocator = null;
		try {
            qLocator = Database.getQueryLocator(jquery);
        }
        catch (Exception e) {
            throw e;
        }
        return qLocator;
    }

	public static void exec (list<Contact> customers) {
		try {
			for (Contact c : customers){
				if (c.Days_till_Next_Food__c == 7) {
					c.ROP_Trigger_Field__c = system.now();
				}
				else if (c.Days_till_Next_Food__c == 5) {
					c.ROP_Trigger_Field_5day__c = system.now();
				}
			}
			database.update(customers,false);
		}
		catch (Exception e) {
			throw e; 
		}
	}

    //Method required by the framework for the actual job execution. 
	global void execute(Database.BatchableContext ctx, List<SObject> sobjects) {
		list<Contact> thisList = toCollection(sobjects);
		exec(thislist);
	}

	//Method required and invoked by the framework when a batch job is done.
	global void finish(Database.BatchableContext ctx) {

	}
	
	//Method to convert list of SObjects to a list of opportunities
	public List<Contact> toCollection(SObject[] sobjects) {
		List<Contact> thisList = new List<Contact>();
		for (SObject obj : sobjects) {
			if (obj instanceof Contact) {
				thisList.add((Contact) obj);
			} else {
				throw new InvalidObjectInstanceException('Invalid object instance in cursor.');
			}
		}
		return thisList;
    }*/
}