global class urlRewriter implements Site.UrlRewriter {

    global PageReference mapRequestUrl(PageReference myFriendlyUrl){
        String url = myFriendlyUrl.getUrl();
        if( url.contains('referral') && !url.contains('nscart') ){
			return new PageReference(url.replace('referral','nscart__referral'));
        }
        return null;
    }
    global List<PageReference> generateUrlFor(List<PageReference> mySalesforceUrls){
    	return mySalesforceUrls;
  	}

}