public class NSSiteController {
    
    // CONSTRUCTOR
    public NSSiteController(){
        init();
    }
    
    // TO INITIALISE ALL THE PUBLIC VARIABLE
    public static void init(){
        
    }
    
    // INITIAL METHOD TO GET DATA
    public static void main(){
        Cookie cook = new Cookie('Counter', '1', null, -1, false);
    }
    
    // RETURNS THE LIST OF OBJECT, PROVIDE OBJECT AND CONDITIONS...
    public static List<sObject> queryData(String ObjectName, String parentFields, String Conditions){
        return Database.query(Util.queryData(ObjectName, parentFields, Conditions));
    }
}