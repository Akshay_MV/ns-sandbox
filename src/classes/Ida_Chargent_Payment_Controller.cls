/** This controller for the page 'Ida Chargent Payment', the page 'Ida Payment ErrorPage' 
  * and the page 'Ida Chargent Payment Step2'.
  * The controller will receive the credit card information from the 'Ida Chargent Payment' page,
  * when anybody click the button 'PLACE YOUR ORDER', it will jump to the page'Ida Chargent Payment Step2',
  * and the chargent method will be excuted. 
 */
public with sharing class Ida_Chargent_Payment_Controller 
{   
    /*public CustomerInfo customer {get; set;}
    private sObject orderRequest;
    public Ida_Chargent_Payment_Controller()
    {
        initCustomerInfo();
    }
    private void initCustomerInfo()
    {
        initOrderRequest();
        copyOrderRequest();
    }
    
    private void initOrderRequest()
    {
        String orderRequestId = ApexPages.currentpage().getParameters().get('id');               
        orderRequestId = (orderRequestId == null) ? '' : orderRequestId;
        
        String queryString = 'select Id, ChargentSFA__Charge_Amount__c, ChargentSFA__Billing_Address__c, ChargentSFA__Billing_City__c, ChargentSFA__Billing_Country__c, ' +
                              'ChargentSFA__Billing_Email__c, ChargentSFA__Billing_First__c, ChargentSFA__Billing_Last__c, ChargentSFA__Billing_Phone__c, ChargentSFA__Billing_Postcode__c, ' +
                              'ChargentSFA__Billing_State__c, ChargentSFA__Card_Type__c, ChargentSFA__Card_Month__c, ChargentSFA__Card_Year__c, ChargentSFA__Card_Number__c, ' +
                              'Ida_Shipping_First_Name__c, ' + 'Ida_Shipping_Last_Name__c, ' + 'Ida_Occasion__c, ' + 'Ida_Message__c, ' + 'Ida_Delivery_Location_Type__c, ' +
                              'Ida_Shipping_State__c, '+'Ida_Business_Name__c, ' + 'Ida_Shipping_ZipCode__c, ' + 'Ida_Shipping_Address_Line1__c, ' +
                              'Ida_Shipping_Address_Line2__c, ' + 'Ida_Shipping_Country__c, ' + 'Ida_Shipping_Phone__c, ' + 'Ida_Shipping_City__c, ' +
                              'Ida_Billing_First_Name__c, ' + 'Ida_Billing_Last_Name__c, ' + 'Ida_Billing_Email__c, ' + 'Ida_Billing_Address_Line1__c, ' +
                              'Ida_Billing_City__c, ' + 'Ida_Billing_State__c, ' + 'Ida_Billing_ZipCode__c, ' + 'Ida_Billing_Country__c, ' + 'Ida_Billing_Phone__c, ' +
                              'Ida_Payment_Method__c, ' + 'Ida_Total_Amount__c from Opportunity where Id = \'' + String.escapeSingleQuotes(orderRequestId) + '\' limit 1';
        
        try
        {
            orderRequest = Database.query(queryString);
        }
        catch(Exception ex)
        {
            orderRequest = new Opportunity();
        }                
    }
    private void copyOrderRequest()
    {
        customer = new CustomerInfo();
        customer.BillingFirstName = (String)orderRequest.get('Ida_Billing_First_Name__c');
        customer.BillingLastName = (String)orderRequest.get('Ida_Billing_Last_Name__c');
        customer.BillingEmail = (String)orderRequest.get('Ida_Billing_Email__c');
        customer.BillingAddress = (String)orderRequest.get('Ida_Billing_Address_Line1__c');
        customer.BillingCity = (String)orderRequest.get('Ida_Billing_City__c');
        customer.BillingState = (String)orderRequest.get('Ida_Billing_State__c');
        customer.BillingZipCode = (String)orderRequest.get('Ida_Billing_ZipCode__c');
        customer.BillingCountry = (String)orderRequest.get('Ida_Billing_Country__c');
        customer.BillingPhone = (String)orderRequest.get('Ida_Billing_Phone__c');
    }
    public List<SelectOption> getCardOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Visa', 'Visa'));
        options.add(new SelectOption('Mastercard', 'Mastercard'));
        options.add(new SelectOption('Discover', 'Discover'));
        options.add(new SelectOption('AMEX', 'AMEX'));
        options.add(new SelectOption('MC Eurocard', 'MC Eurocard'));
        options.add(new SelectOption('UK Maestro', 'UK Maestro'));
        options.add(new SelectOption('JCB Card', 'JCB Card'));
        options.add(new SelectOption('Solo', 'Solo'));
        options.add(new SelectOption('Electron', 'Electron'));
        return options;
    }
    public List<SelectOption> getExpirationYearOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        Integer currentYear = Date.today().year();
        for(Integer i = 0; i < 20; i++)
        {
            options.add(new SelectOption(String.valueOf(currentYear + i), String.valueOf(currentYear + i)));
        }
        return options; 
    }
    
    public List<SelectOption> getExpirationMonthOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('1', '1'));
        options.add(new SelectOption('2', '2'));
        options.add(new SelectOption('3', '3'));
        options.add(new SelectOption('4', '4'));
        options.add(new SelectOption('5', '5'));
        options.add(new SelectOption('6', '6'));
        options.add(new SelectOption('7', '7'));
        options.add(new SelectOption('8', '8'));
        options.add(new SelectOption('9', '9'));
        options.add(new SelectOption('10', '10'));
        options.add(new SelectOption('11', '11'));
        options.add(new SelectOption('12', '12'));
        return options;
    }
    public List<SelectOption> getStateOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult result = Schema.sObjectType.Opportunity.fields.Ida_Billing_State__c;
        for(Schema.PicklistEntry entry : result.getPicklistValues())
        {
            options.add(new SelectOption(entry.getValue(), entry.getValue()));
        }
        return options;
    }    
    
    public PageReference submit()
    {
        try
        {
            updateOrderRequest();
            PageReference pageRef = new PageReference('/apex/IdaChargentPaymentStep2?id=' + orderRequest.get('id')); 
            pageRef.setRedirect(true);
            return pageRef;
        }
        catch(Exception ex)
        {
            return null;
        }
    }            
    
    private void updateOrderRequest()
    {
        orderRequest.put('ChargentSFA__Billing_First__c', customer.BillingFirstName);
        orderRequest.put('ChargentSFA__Billing_Last__c', customer.BillingLastName);
        orderRequest.put('ChargentSFA__Billing_Phone__c', customer.BillingPhone);
        orderRequest.put('ChargentSFA__Billing_Address__c', customer.BillingAddress);
        orderRequest.put('ChargentSFA__Billing_City__c', customer.BillingCity);
        orderRequest.put('ChargentSFA__Billing_Country__c', customer.BillingCountry);
        orderRequest.put('ChargentSFA__Billing_State__c', customer.BillingState);
        orderRequest.put('ChargentSFA__Billing_Postcode__c', customer.BillingZipCode);
        orderRequest.put('ChargentSFA__Billing_Zip__c', customer.BillingZipCode);        
        orderRequest.put('ChargentSFA__Billing_Email__c', customer.BillingEmail);
        orderRequest.put('ChargentSFA__Card_Type__c', customer.CreditCard.CardType);
        orderRequest.put('ChargentSFA__Card_Month__c', customer.CreditCard.ExpirationMonth);
        orderRequest.put('ChargentSFA__Card_Year__c', customer.CreditCard.ExpirationYear);
        orderRequest.put('ChargentSFA__Card_Number__c', customer.CreditCard.CardNumber);
        orderRequest.put('ChargentSFA__Card_Security__c', customer.CreditCard.SecurityCode);
        orderRequest.put('ChargentSFA__Charge_Amount__c', (Decimal)orderRequest.get('Ida_Total_Amount__c'));
        orderRequest.put('ChargentSFA__Payment_Method__c', 'Credit Card');
        
        orderRequest.put('Ida_Billing_First_Name__c', customer.BillingFirstName);
        orderRequest.put('Ida_Billing_Last_Name__c', customer.BillingLastName);
        orderRequest.put('Ida_Billing_Email__c', customer.BillingEmail);
        orderRequest.put('Ida_Billing_Address_Line1__c', customer.BillingAddress);
        orderRequest.put('Ida_Billing_City__c', customer.BillingCity);
        orderRequest.put('Ida_Billing_State__c', customer.BillingState);
        orderRequest.put('Ida_Billing_ZipCode__c', customer.BillingZipCode);
        orderRequest.put('Ida_Billing_Country__c', customer.BillingCountry);
        orderRequest.put('Ida_Billing_Phone__c', customer.BillingPhone);
        orderRequest.put('Ida_Payment_Method__c', 'Chargent');
        
        update orderRequest;
    }        
    public class CustomerInfo
    {
        public String BillingFirstName {get; set;}
        public String BillingLastName {get; set;}
        public String BillingEmail {get; set;}
        public String BillingAddress {get; set;}
        public String BillingCity {get; set;}
        public String BillingState {get; set;}
        public String BillingZipCode {get; set;}
        public String BillingCountry {get; set;}
        public String BillingPhone {get; set;}
        public CreditCardInfo CreditCard {get;set;}
        
        public CustomerInfo()
        {
            CreditCard = new CreditCardInfo();
        }
    }
    public class CreditCardInfo
    {
        public String CardType {get; set;}        
        public String CardNumber {get; set;}
        public String SecurityCode {get; set;}
        public String ExpirationYear {get; set;}
        public String ExpirationMonth {get; set;}
    }   
    @isTest
    static void test_Ida_Chargent_Payment_Controller()
    {
    	try {
	        Ida_Chargent_Payment_Controller testController = new Ida_Chargent_Payment_Controller();
	        sObject newOrder = new Opportunity(Name = 'testOrder', StageName = 'Prospecting', CloseDate = Date.today());
	        insert newOrder;
	        PageReference testPage = new PageReference('/apex/IdaChargentPayment?id='+newOrder.Id);
	        Test.setCurrentPage(testPage);
	        testController = new Ida_Chargent_Payment_Controller();
	        testController.getExpirationMonthOptions();
	        testController.getExpirationYearOptions();
	        testController.submit();
	        List<Opportunity> orderList = [select Id, ChargentSFA__Billing_Email__c, ChargentSFA__Billing_Postcode__c, ChargentSFA__Card_Number__c from Opportunity where Id = :newOrder.Id limit 1];
    	} catch (exception e){ }
    }*/
}