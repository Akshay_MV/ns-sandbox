@isTest
private class DataMigration_Tests {
	/*
	@isTest static void opportunityMigrationTests() {
		test.startTest();
		opportunity opp=new opportunity(name='test',stagename='Closed Won',CloseDate=system.today());
		OpportunityMigration_Batch.migrateOpportunity(opp); 
		opp.Ida_Shipping_Last_Name__c='test';
		opp.Ida_Shipping_First_Name__c='test';
		insert opp;
		test.StopTest();

	}
	
	@isTest static void contactMigrationTests() {
		test.startTest();
		route__c route=new route__c(name='test');
		insert route;
		contact cont=new contact(lastname='test',email='testcontact@test.com',route__c=route.id);

		insert cont;
		database.executebatch(new contactMigration_Batch()); 	

		test.StopTest();

	}*/
	/*
	@isTest static void routeMigrationTests() {
		test.startTest();
		route__c route=new route__c(name='test',Area__c='test',Zip__c='111111,22222,33333,44444,55555');
		route__c route2=new route__c(name='test2',Area__c='test',Zip__c='111111,22222,33333,44444,55555');
		insert new list<route__c>{route,route2};	
		database.executebatch(new RouteMigration_Batch()); 	

		test.StopTest();

	}
	@isTest static void salestaxMigrationTests() {
		test.startTest();
		Ida_Tax__c tax=new Ida_Tax__c(Ida_Tax_Description__c='test',Ida_Tax_Percentage__c=1,Zip__c='111111');
		Ida_Tax__c tax2=new Ida_Tax__c(Ida_Tax_Description__c='test2',Ida_Tax_Percentage__c=8,Zip__c='22222',Ida_Tax_State__c='california');
		insert new list<Ida_Tax__c>{tax,tax2};	
		database.executebatch(new SalesTaxMigration_Batch()); 	
		NSDataMigrationTool.updateRoutesTax();

		test.StopTest();

	}
	@isTest static void productMigrationTests() {
		test.startTest();
		product2 product=new product2(name='test');
		//Ida_Tax__c tax2=new Ida_Tax__c(Ida_Tax_Description__c='test2',Ida_Tax_Percentage__c=8,Zip__c='22222',Ida_Tax_State__c='california');
		insert new list<product2>{product};	
		database.executebatch(new ProductMigration_Batch()); 	
		
		test.StopTest();

	}
	@isTest static void couponMigrationTests() {
		test.startTest();
		coupon__c coup=new coupon__c(name='test',discount_type__c='Dollar Off',Expiration_date__c=System.today());
		insert coup;
		contact cont=new contact(lastname='test',email='testcontact@test.com');
		insert cont;
		coupon_user__c coupUser=new coupon_user__c(coupon__c=coup.id,Customer__c=cont.id);
		insert coupUser;
		//Ida_Tax__c tax2=new Ida_Tax__c(Ida_Tax_Description__c='test2',Ida_Tax_Percentage__c=8,Zip__c='22222',Ida_Tax_State__c='california');
		
		database.executebatch(new CouponMigration_Batch()); 	
		
		test.StopTest();

	} 
	@isTest static void TransactionMigrationTests() {
		test.startTest();
		opportunity opp=new opportunity(name='test',stagename='Closed Won',CloseDate=system.today());
		OpportunityMigration_Batch.migrateOpportunity(opp); 
		opp.Ida_Shipping_Last_Name__c='test';
		opp.Ida_Shipping_First_Name__c='test';
		insert opp;
		ChargentSFA__Transaction__c trans=new ChargentSFA__Transaction__c(ChargentSFA__Opportunity__c=opp.id);
		//Ida_Tax__c tax2=new Ida_Tax__c(Ida_Tax_Description__c='test2',Ida_Tax_Percentage__c=8,Zip__c='22222',Ida_Tax_State__c='california');
		insert new list<ChargentSFA__Transaction__c>{trans};	
		database.executebatch(new TransactionMigration_Batch()); 	
		
		test.StopTest();

	}

	@isTest static void creditcardMigrationTests() {
		test.startTest();
		contact cont=new contact(lastname='test',email='testcontact@test.com');
		insert cont;
		opportunity opp=new opportunity(contact__c=cont.id, name='test',stagename='Closed Won',
			CloseDate=system.today(),
			ChargentSFA__Payment_Method__c='Credit Card', 
			ChargentSFA__Card_Type__c='Visa',
			ChargentSFA__Card_Month__c='11',
			ChargentSFA__Card_Year__c='2017',
			ChargentSFA__Card_Number__c='411111111111',
			ChargentSFA__Card_Security__c='123',
			Ida_Shipping_Last_Name__c='test',
			Ida_Shipping_First_Name__c='test');
		insert opp;
		database.executeBatch(new creditcardMigration_Batch()); 
		opportunity opp2=new opportunity(contact__c=cont.id, name='test',stagename='Closed Won',
			CloseDate=system.today(),
			ChargentSFA__Payment_Method__c='Credit Card', 
			ChargentSFA__Card_Type__c='Visa',
			ChargentSFA__Card_Month__c='11',
			ChargentSFA__Card_Year__c='2017',
			ChargentSFA__Card_Number__c='411111111111',
			ChargentSFA__Card_Security__c='123',
			Ida_Shipping_Last_Name__c='test',
			Ida_Shipping_First_Name__c='test');
		insert opp2;



		database.executeBatch(new creditcardMigration_Batch()); 
		
		test.StopTest();

	}*/
	/*
	@isTest static void productTabMigrationTests() {
		test.startTest();
		
		list<product2> plist=new list<product2>();
		//bulktest 
		for(integer i=0;i<2;i++){
			plist.add(new product2(name='test'+String.valueof(i)));
		}
		//Ida_Tax__c tax2=new Ida_Tax__c(Ida_Tax_Description__c='test2',Ida_Tax_Percentage__c=8,Zip__c='22222',Ida_Tax_State__c='california');
		insert plist;
		list<IDA_Catalog_Custom__c> idaCats=new list<IDA_Catalog_Custom__c>();
		for(product2 pr:pList){
		IDA_Catalog_Custom__c cat=new IDA_Catalog_Custom__c(ida_title__c='test',ida_Catalog_Custom__c='Tab 1', ida_text_Rich__c='non',product__c=pr.id);
		IDA_Catalog_Custom__c cat2=new IDA_Catalog_Custom__c(ida_title__c='test 2',ida_Catalog_Custom__c='Tab 2', ida_text_Rich__c='non',product__c=pr.id);
			idacats.add(cat);
			idacats.add(cat2);
		}
		insert idacats;


		database.executebatch(new Product_TabMigration_Batch(),500); 	
	
		test.StopTest();
		System.debug('TAB TEST');
		String testquery='select id,name,nscart__product_tab_set__c from nsCart__product_tab__c';
		list<sObject> testTabs=Database.query(testQuery);
		System.debug(testTabs);
		System.debug(testTabs.size());


	}
	@isTest static void petMigrationTests() {
		test.startTest();
		contact cont=new contact(lastname='test',email='testcontact@test.com');
		insert cont;
		pet__c pet=new pet__c(name='test',contact__c=cont.id);
		insert pet;

		
		database.executebatch(new petMigration_Batch()); 	

		test.StopTest();

	}
	
*/

	
	
}