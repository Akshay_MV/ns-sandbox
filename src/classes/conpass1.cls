public class conpass1 {
    
    public static String Password {get;set;}
    public static String confirmPassword {get;set;}
    public static Boolean success {get;set;}
    
    public String PageMsg{get;set;}

    public PageReference change(){
        Site_User__c pass = new Site_User__c();
        Id x = ApexPages.currentPage().getParameters().get('id');
        Site_User__c var = new Site_User__c();
        var = [SELECT Id,Password__c FROM Site_User__c WHERE Id=: x ];
        
        if(Password == null || Password == '' ){
            PageMsg = 'Enter password';
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Enter password'));
            return null;
        }else if(! Pattern.matches('[a-zA-Z0-9@*#]{8,20}',Password) || !Pattern.matches('[a-zA-Z0-9@*#]{8,20}',confirmPassword)){
            PageMsg = 'Please enter the Valid  Password';
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter the Valid  Password'));
            return null;
        }else if(confirmPassword == null || confirmPassword == ''){
            PageMsg = 'Please confirm your Password';
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please confirm your Password'));
            return null;
        }else if(!Password.equals(confirmPassword)){
            PageMsg = 'Password does not match';
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Password does not match'));
            return null;
        }else if(var.Password__c == Password ){
            PageMsg = 'New Password can not be same as old password';
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'New Password can not be same as old password'));
            return null;
        }else{
            pass.Id = ApexPages.currentPage().getParameters().get('id');
            pass.Password__c = Password;
            update pass;
            PageMsg = 'Update Successfully';
            //apexpages.addMessage(new ApexPages.message(Apexpages.Severity.CONFIRM,'Update Successfully'));
            success = true;

            PageReference pr = new PageReference('/apex/LoginPage?passwordUpdate=true');
        return pr;
        }
    }
    
}