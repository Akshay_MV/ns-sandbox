/*
Transfer data from IDA, chargent and legacy fields to NSCart Contact Fields
dynamic sobject to allow compile without package dependency.
disable workflows and triggers before executing.
*/
global class ContactMigration_Batch {
	/*
	//String query='select '+ NSDataMigrationTool.queryAllFields('Contact')+' from Contact';

  // limit it to a certain date
  String query='select '+ NSDataMigrationTool.queryAllFields('Contact')+' from Contact where createddate<2014-11-21T00:00:00Z';
	global Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 

	global ContactMigration_Batch() {  
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		list<Contact> contactUpdates= new list<Contact>();
   		set<id> oldrouteIds=new set<id>();
   		for(sObject sOb:Scope){
   			Contact cont = (Contact)sOb;
            migrateContact(cont);
   			if(cont.route__c!=null){
   				oldRouteIds.add(cont.route__c);
   			} 			
   			contactUpdates.add(cont); 
   		} 
  		//Query NSCART__route__c
     if(gd.containsKey('NSCart__Route__c')){
         Schema.SObjectType routeType = gd.get('NSCart__Route__c');
     		map<String,id> newRouteIdMap=new map<String,id>();
         String rtquery='select id,nscart__old_route_id_2__c from NSCart__Route__c where nscart__old_route_id_2__c in:oldRouteIds';
     		for(sObject newRt: database.query(rtquery)){

     			newRouteIdMap.put((String)newRT.get('nscart__old_Route_id_2__c'),newRt.id);
     		}
     		for(Contact cont:contactUpdates){
      			if(newRouteIdmap.containsKey(cont.route__c)){
      				cont.put('NSCart__Route__c',newRouteIdMap.get(cont.route__c));
      			}
      	}
      }
   		update contactUpdates;		
	}
	*/
	global void finish(Database.BatchableContext BC) {
		
	}
	/*
   global contact migrateContact(contact cont){

    //All fields

      cont.put('NSCart__Active__c',cont.Active__c);
      cont.put('NSCart__At_Show__c',cont.At_Show__c);
      cont.put('NSCart__Auto_Order_Reset_Reason__c',cont.Auto_Order_Reset_Reason__c);
      cont.put('NSCart__Auto_Order_Reset_Date__c',cont.Auto_Order_Reset_Date__c);
      cont.put('NSCart__Auto_Reorder__c',cont.Auto_Reorder__c);
      cont.put('NSCart__Call_Requested__c',cont.Call_Requested__c);
      cont.put('NSCart__Company_Owned_Employed__c',cont.Company_Owned_Employed__c);
      cont.put('NSCart__Competitive_Food_Brand__c',cont.Competitive_Food_Brand__c);
      cont.put('NSCart__Competitive_Food_Recipe__c',cont.Competitive_Food_Recipe__c);
      cont.put('NSCart__Confirmed_ROP_Contact_Method__c',cont.Confirmed_ROP_Contact_Method__c);
      cont.put('NSCart__CRITICAL_NOTE_NEXT_ORDER__c',cont.CRITICAL_NOTE_NEXT_ORDER__c);
      cont.put('NSCart__Driver_Delivery_Instructions__c',cont.Driver_Delivery_Instructions__c);
      cont.put('NSCart__Drop_Locations__c',cont.Drop_Locations__c);
      //cont.put('NSCart_ Duplicate_Checker__c',cont.Duplicate_Checker__c);
      cont.put('NSCart__Alternate_Email__c',cont.Email_2__c);
      //cont.put('NSCart_ Email_is_NONE__c',cont.Email_is_NONE__c);
      cont.put('NSCart__Referral__c',cont.Refferal__c);
      cont.put('NSCart__First_Date_Ordered__c',cont.First_Date_Ordered__c);
      cont.put('NSCart__First_Delivery_Date__c',cont.First_Order_Date__c);
      cont.put('NSCart__Freq_Week_set_date__c',cont.Freq_Wk_set_date__c);
      cont.put('NSCart__Frequency__c',cont.Freq__c);
      cont.put('NSCart__Freq_date__c',cont.Freq_date__c);
      cont.put('NSCart__Frequency_Trigger__c',cont.Frequency_Trigger__c);

      cont.put('NSCart__Heard_Method__c',cont.Heard_Method__c);


      cont.put('NSCart__INACTIVE_REASON__c',cont.INACTIVE_REASON__c);
      cont.put('NSCart__Inactive_Detail__c',cont.Intactive_Detail__c);
      cont.put('NSCart__Internal_Customer_Notes__c',cont.Internal_Customer_Notes__c);
      cont.put('NSCart__Internal_Payment_Notes__c',cont.Internal_Payment_Notes__c);
      cont.put('NSCart__Last_Date_Ordered__c',cont.Last_Date_Ordered__c);
      cont.put('NSCart__Last_Delivery_Date__c',cont.Last_Order_Date__c);
      cont.put('NSCart__Last_Order_Created_Date_from_Oppty__c',cont.Last_Order_Created_Date_from_Oppty__c);
      cont.put('NSCart__Lead_Source_Detail__c',cont.Lead_Source_Detail__c);
      cont.put('NSCart__Referral_Name__c',cont.Referral_Name__c);
      cont.put('NSCart__Name_of_Biz_Partner__c',cont.Name_of_Biz_Partner__c);
      cont.put('NSCart__Next_Order_Date__c',cont.Next_Order_Date__c);
      cont.put('NSCart__No_OCN_Contact__c',cont.No_OCN_Contact__c);
      cont.put('NSCart__NS_Employee_Program_who_Referred__c',cont.NS_Employee_Program_who_Referred__c);
      cont.put('NSCart__OCN_Alert_Sent__c',cont.OCN_Alert_Sent__c);
      cont.put('NSCart__OCN_Text_Phone__c',cont.OCN_Text_Phone__c);
      cont.put('NSCart__Old_Customer_ID__c',cont.Old_Customer_ID__c);
      cont.put('NSCart__One_Click_Done_Order_Request__c',cont.One_Click_Done_Order_Request__c);
     //cont.put('NSCart_ OverDueValue__c',cont.OverDueValue__c);
      cont.put('NSCart__Ready_for_Text_opt_in__c',cont.Ready_for_Text_opt_in__c);
      cont.put('NSCart__Referring_Affiliate_Biz_Name__c',cont.Referring_Affiliate_Biz_Name__c);
      cont.put('NSCart__Remove_from_ROP__c',cont.Remove_from_ROP__c);
      cont.put('NSCart__Reorder_Rate__c',cont.Reorder_Rate__c);
      cont.put('NSCart__Repeat__c',cont.Repeat__c);
      cont.put('NSCart__ROP_2_Notify_10_Day__c',cont.ROP_2_Notify_10_Day__c);
      cont.put('NSCart__ROP_Contact_Method_OCNBoth__c',cont.ROP_Contact_Method_OCNBoth__c);
      cont.put('NSCart__ROP_Contact_Method_Phone__c',cont.ROP_Contact_Method_Phone__c);
      cont.put('NSCart__ROP_Contact_Method_OCNText__c',cont.ROP_Contact_Method_OCNText__c);
      cont.put('NSCart__ROP_Trigger_Field__c',cont.ROP_Trigger_Field__c);
      //cont.put('NSCart__RO_Rate_Alert__c',cont.RO_Rate_Alert__c);
      //cont.put('NSCart__Route__c',cont.Route__c);//** route different object id...
      cont.put('NSCart__Total_Consumption_Rate__c',cont.Total_Consumption_Rate__c);
      cont.put('NSCart__Referral_Credited__c',cont.Referral_Credited__c); 

 //new fields 12/2014
   //Doesnt exist in chicago cont.put('NSCart__Balance_Due__c',cont.Balance_Due__c);
    cont.put('NSCart__Breed__c',cont.Breed__c);
    cont.put('NSCart__Breed_2__c',cont.Breed2__c);
    cont.put('NSCart__Breed_3__c',cont.Breed3__c);
    cont.put('NSCart__Breed_4__c',cont.Breed4__c);
    cont.put('NSCart__Consumption_Day__c',cont.Consumption_Day__c);
    cont.put('NSCart__Consumption_Day_2__c',cont.Consumption_Day2__c);
    cont.put('NSCart__Consumption_Day_3__c',cont.Consumption_Day3__c);
    cont.put('NSCart__Consumption_Day_4__c',cont.Consumption_Day4__c);
   //Doesnt exist in chicago cont.put('NSCart__Inactive_Date__c',cont.Inactive_Date__c);
   //Doesnt exist in chicago cont.put('NSCart__Inactive_Reason_Details__c',cont.Inactive_Reason_Details__c);
    cont.put('NSCart__Issue_1__c',cont.Issue_1__c);
    cont.put('NSCart__Issue_1_2__c',cont.Issue_1_2__c);
    cont.put('NSCart__Issue_1_3__c',cont.Issue_1_3__c);
    cont.put('NSCart__Issue_1_4__c',cont.Issue_1_4__c);
    cont.put('NSCart__Issue_2__c',cont.Issue_2__c);
    cont.put('NSCart__Issue_2_2__c',cont.Issue_2_2__c);
    cont.put('NSCart__Issue_2_3__c',cont.Issue_2_3__c);
    cont.put('NSCart__Issue_2_4__c',cont.Issue_2_4__c);
    cont.put('NSCart__Issue_Other__c',cont.Issue_Other__c);
    cont.put('NSCart__JVDDC__c',cont.JVDDC__c);
    cont.put('NSCart__KMV_d__c',cont.KMV_d__c);
    cont.put('NSCart__Method__c',cont.Method__c);
    cont.put('NSCart__Newsletter__c',cont.Newsletter__c);
    cont.put('NSCart__Pet_2_Name__c',cont.Pet2_Name__c);
    cont.put('NSCart__Pet_2_Type__c',cont.Pet2_Type__c);
    cont.put('NSCart__Pet_3_Name__c',cont.Pet3_Name__c);
    cont.put('NSCart__Pet_3_Type__c',cont.Pet3_Type__c);
    cont.put('NSCart__Pet_4_Name__c',cont.Pet4_Name__c);
    cont.put('NSCart__Pet_4_Type__c',cont.Pet4_Type__c);
    cont.put('NSCart__Pets_Name__c',cont.Pets_Name__c);
    cont.put('NSCart__Pet_Type__c',cont.Pet_Type__c);
    cont.put('NSCart__Preferred_Payment_Method__c',cont.Preferred_Payment_Method__c);
   //Doesnt exist in chicago cont.put('NSCart__Salesperson__c',cont.Salesperson__c);
   //Doesnt exist in chicago cont.put('NSCart__Show_Code__c',cont.Show_Code__c);
   //Doesnt exist in chicago cont.put('NSCart__Show_Name__c',cont.Show_Name__c);
   //Doesnt exist in chicago cont.put('NSCart__Show_Year__c',cont.Show_Year__c);

      return cont; 
   }


	*/
}