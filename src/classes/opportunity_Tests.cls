@isTest(SeeAllData=true) 
private class opportunity_Tests {
/*
	static Account testAccount = testHelper_Methods.account_InsertTest();
	static Contact testContact = testHelper_Methods.contact_InsertTest(testAccount);
	static Opportunity testOpportunity = testHelper_Methods.opportunity_InsertTest(testAccount, testContact);
	static Pricebook2 testPricebook = testHelper_Methods.pricebook_InsertTest();
	static Product2 testProduct = testHelper_Methods.product_InsertTest();
	static PricebookEntry testPricebookEntry = testHelper_Methods.pricebookEntry_InsertTest(testPricebook, testProduct);
	//static Ida_Coupon__c testIdaCoupon = testHelper_Methods.idaCoupon_insertTest(testProduct);

	static testMethod void opportunity_Methods_UnitTest() {
		test.startTest();
			Opportunity testOpportunity2 = new Opportunity();
			testOpportunity2.AccountId = testAccount.Id;
			testOpportunity2.Amount = 0;
			testOpportunity2.CloseDate = system.today();
			testOpportunity2.Contact__c = testContact.Id;
			testOpportunity2.Delivery_Date__c = system.today();
			testOpportunity2.Discount__c = '1%';
			testOpportunity2.Ida_Applied_Coupon__c = 'test';
			testOpportunity2.Name = string.valueOf(system.now());
			testOpportunity2.StageName = 'Closed Won';
//			insert testOpportunity2;
			
			testOpportunity.Use_Information_On_File__c = true;
			testOpportunity.Delivery_Date__c = system.today().addDays(1);
			testOpportunity.ChargentSFA__Payment_Method__c = 'E-Check';
			testOpportunity.StageName = 'Closed Won';
			update testOpportunity;

			testOpportunity.StageName = 'Prospecting';
			update testOpportunity;

			testOpportunity.ChargentSFA__Payment_Method__c = 'Credit Card';
			testOpportunity.StageName = 'Closed Won';
			testContact.Use_Card__c = 'Card 1';
			update testContact;
			update testOpportunity;

			testOpportunity.StageName = 'Prospecting';
			update testOpportunity;

			testOpportunity.StageName = 'Closed Won';
			testContact.Use_Card__c = 'Card 2';
			update testContact;
			update testOpportunity;


		test.stopTest();        
	}

	static testMethod void opportunity_Extension_UnitTest() {
		Opportunity testOpportunity2 = testOpportunity.clone(true,false);

		OpportunityLineItem testOpportunityLineItem1 = testHelper_Methods.opportunityLineItem_InsertTest(testOpportunity, testPricebookEntry); 
		testOpportunity.StageName = 'Closed Won';
		update testOpportunity;

		test.startTest();
			apexPages.Standardcontroller sc = new apexPages.Standardcontroller(testOpportunity2);
			opportunity_Extension ext = new opportunity_Extension(sc);
			ext.oppty = testOpportunity2;
			ext.oppty.Contact__c = testContact.Id;
			ext.queryContact(testContact.Id);
			ext.oppty.amount = 0;
			ext.getItemsOnSale();
			ext.opportunity_New_Onload();

			system.debug(testProduct);
			system.debug(testPricebookEntry);
			//ext.searchResults = new List<opportunity_Extension.productWrapper>();
			ext.searchProducts();
			ext.searchTerm = 'Test';
			ext.searchProducts();

			ext.billToAddress = 'mailing';
			ext.updateBillToAddress();
			ext.billToAddress = 'other';
			ext.updateBillToAddress();
			ext.shipToAddress = 'mailing';
			ext.updateShipToAddress();
			ext.shipToAddress = 'other';
			ext.updateShipToAddress();
			ext.updateTotals();
			ext.addProductsToOrder();
			ext.submitOrder();

			opportunity_Extension.productWrapper pw = new opportunity_Extension.productWrapper();
			pw.oli = testOpportunityLineItem1;
			pw.priceBookEntry = testPricebookEntry;
			pw.product = testProduct;
			pw.Qty = 1;
			system.debug(pw.Qty);
			system.debug(testPricebookEntry);
			pw.lineTotal = pw.Qty * testPricebookEntry.UnitPrice;


			ext.searchResults = new List<opportunity_Extension.productWrapper>();
			ext.searchResults.add(pw);

			ext.lastOrderItems = new List<opportunity_Extension.productWrapper>();
			ext.lastOrderItems.add(pw);

			ext.past12MonthsItems = new List<opportunity_Extension.productWrapper>();
			ext.past12MonthsItems.add(pw);

			ext.opptyProducts = new List<opportunity_Extension.productWrapper>();
			ext.opptyProducts.add(pw);
			ext.addProductsToOrder();

		test.stopTest();
		try {
			insert testOpportunity2;
		} catch (exception e){ }
	}

	/*static testMethod void reOrder_Controller_UnitTest(){
		test.startTest();

			PageReference p = page.reOrder;
			p.getParameters().put('id',testContact.Id);
			p.getParameters().put('recontact','');
			test.setCurrentPage(p);
			reOrder_Controller con = new reOrder_Controller();
			con.reOrderonLoadAction();

			p = page.reOrder;
			p.getParameters().put('id',testContact.Id);
			p.getParameters().put('recontact','1');
			test.setCurrentPage(p);
			con = new reOrder_Controller();
			con.reOrderonLoadAction();

			p = page.reOrder;
			p.getParameters().put('id',testContact.Id);
			p.getParameters().put('recontact','2');
			test.setCurrentPage(p);
			con = new reOrder_Controller();
			con.reOrderonLoadAction();

			p = page.reOrder;
			p.getParameters().put('id',testContact.Id);
			p.getParameters().put('recontact','3');
			test.setCurrentPage(p);
			con = new reOrder_Controller();
			con.reOrderonLoadAction();

			p = page.reOrder;
			p.getParameters().put('id',testContact.Id);
			p.getParameters().put('recontact','call');
			test.setCurrentPage(p);
			con = new reOrder_Controller();
			con.reOrderonLoadAction();

		test.stopTest();
	}*/


	/*static testMethod void reOrderPromptCommunication_Controller_UnitTest(){
		List<Opportunity> opptys = [Select Id, Contact__c from Opportunity where Contact__c != null and IsWon = true and HasOpportunityLineItem = true limit 1];
		if ( opptys.size() > 0){
			testOpportunity = opptys[0];
		} else {
			OpportunityLineItem testOpportunityLineItem1 = testHelper_Methods.opportunityLineItem_InsertTest(testOpportunity, testPricebookEntry); 
			testOpportunity.StageName = 'Closed Won';
			update testOpportunity;
		}

		test.startTest();
			reOrderPromptCommunication_Controller con = new reOrderPromptCommunication_Controller();
			con.thisId = testOpportunity.Contact__c;
			con.getEmailDetails();
		test.stopTest();
	}*/
}