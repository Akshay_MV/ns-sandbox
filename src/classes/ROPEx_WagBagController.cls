public class ROPEx_WagBagController {
    
    Public String anchorName{get;set;}
    public String responseCode {get;set;}
    public String conId {get;set;}
    public String contactids {get;set;}
    public Boolean shouldRedirect {public get; private set;}
    public String redirectUrl {public get; private set;}
    public List<Opportunity> oppIds{
        get{
            List<Opportunity> oppIds = [Select Id,NSCart__Last_Delivery_Date_from_Contact__c,NSCart__Next_Delivery_Date__c,NSCart__Contact__r.NSCart__Last_Delivery_Date__c,NSCart__Contact__r.NSCart__Next_Order_Date__c,NSCart__Contact__c from Opportunity where NSCart__Contact__c=:conId ORDER BY CreatedDate DESC Limit 1];
            return oppIds;
        }
        set;
    
    }
    List<OpportunityLineItem> listOfLineItems{
        get{
            List<OpportunityLineItem> listOfLineItems = [SELECT Quantity, UnitPrice, TotalPrice,PricebookEntry.Name, PricebookEntry.Product2.Family FROM OpportunityLineItem where OpportunityId IN: oppIds];
            return listOfLineItems;
        }
        set;
    }

    public List<OpportunityLineItem> listOfOppLineItems{
        get{
                    List<Opportunity> oppIds = [Select Id,NSCart__Last_Delivery_Date_from_Contact__c,NSCart__Next_Delivery_Date__c from Opportunity where NSCart__Contact__c=:conId ORDER BY CreatedDate DESC Limit 1];
                    List<OpportunityLineItem> listOfOppLineItems=[SELECT Quantity, UnitPrice, TotalPrice,PricebookEntry.Name, PricebookEntry.Product2.Family FROM OpportunityLineItem where OpportunityId IN: oppIds];
                    return listOfOppLineItems;
            }set;
    }
    public Date lastDeliveryDate{get;set;}
    public Date nextAvailableDeliveryDate{get;set;}
    
    
    public ROPEx_WagBagController() {
        responseCode = ApexPages.currentPage().getParameters().get('ResponseCode');
        conId = ApexPages.currentPage().getParameters().get('contactId');
        if(String.isNotBlank(conId)){
            contactids=conId;
        }
        
        shouldRedirect = false;
        
    }
    
    public void InitPage() {
        Contact contact =[Select Id, Email from Contact where Id=:conId];
        shouldRedirect = true;
        if(responseCode!=null ){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.ROP_Feedback__c='WagBag';
           feedback_Obj.Content_Reference__c ='ROPEx WagBag';
           feedback_Obj.Contact__c = conId;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
                insert feedback_Obj;
               // redirectUrl = 'https://nschicago.secure.force.com/nscart__voila';
                redirectUrl = 'https://nschicago.secure.force.com/TY_ROPEx';
               
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }
        }
        if(responseCode!=null && responseCode == 'OneClickDone'){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.ROP_Feedback__c='OCD';
           feedback_Obj.Content_Reference__c ='ROPEx WagBag';
           feedback_Obj.Contact__c = conId;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
                insert feedback_Obj;
                redirectUrl = 'https://nschicago.secure.force.com/SecondChanceWagBag?contactId='+conId;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }
        }
        if(responseCode!=null && responseCode == 'remindInWeek'){
            String base = Url.getSalesforceBaseUrl().toExternalForm();
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.ROP_Feedback__c='Push1';
           feedback_Obj.Content_Reference__c ='ROPEx WagBag';
           feedback_Obj.Contact__c = conId;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
                insert feedback_Obj;
                redirectUrl = base+'/apex/Push1LandPage';
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }
        }
        if(responseCode!=null && responseCode == 'preload'){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.ROP_Feedback__c='Cart';
           feedback_Obj.Content_Reference__c ='ROPEx WagBag';
           feedback_Obj.Contact__c = conId;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
                insert feedback_Obj;
                redirectUrl = 'https://chicago.naturesselectpetfood.com/interfaces/reorder/cart?e='+contact.Email;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }
        }
        if(responseCode!=null && responseCode == 'text'){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.ROP_Feedback__c='TextReq';
           feedback_Obj.Content_Reference__c ='ROPEx WagBag';
           feedback_Obj.Contact__c = conId;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
               insert feedback_Obj;
               redirectUrl = System.Label.TextMessagePromptClick;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }
        }
        if(responseCode!=null && responseCode == 'remindMe'){
            String base = Url.getSalesforceBaseUrl().toExternalForm();
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.ROP_Feedback__c='Push1';
           feedback_Obj.Content_Reference__c ='ROPEx WagBag';
           feedback_Obj.Contact__c = conId;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
               insert feedback_Obj;
               redirectUrl = 'https://nschicago.secure.force.com/Push1LandPage';
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }
        }
    }
    Public PageReference redirectToVoilla(){
        system.debug('inside method');
        anchorName=ApexPages.currentPage().getParameters().get('anchorName');
        System.debug('anchorName>>>>>>'+anchorName);
        if(anchorName=='Yes'){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.ROP_Feedback__c='WagBag2';
           feedback_Obj.Content_Reference__c ='ROPEx WagBag';
           feedback_Obj.Contact__c = contactids;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
               insert feedback_Obj;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }
        }
        System.debug('contactids>>>>'+contactids);
        System.debug('conId>>>>'+conId);
        //PageReference pg=new PageReference('https://nschicago.secure.force.com/nscart__voila');
         PageReference pg=new PageReference('https://nschicago.secure.force.com/TY_ROPEx');
        return pg;
    }
    Public PageReference redirectToLchat(){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.ROP_Feedback__c='LiveChat';
           feedback_Obj.Content_Reference__c ='ROPEx WagBag';
           feedback_Obj.Contact__c = contactids;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
                insert feedback_Obj;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }
        System.debug('contactids>>>>'+contactids);
        System.debug('conId>>>>'+conId);
        PageReference pg=new PageReference('https://quality.livechatinc.com/4152091');
        return pg;
    }

}