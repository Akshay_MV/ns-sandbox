@isTest
private class Test_ROPEx_WagBagController{
    @isTest
    private static void testmethod1(){
        
        String ResponseCode;
        String Response;
        String ContactId;
        String AdultOrPuppy;
        Boolean shouldRedirect;
        String redirectUrl;
        
        Contact con = new Contact();
        con.LastName='lastname';
        insert con;
        
        PageReference pageRef = Page.SecondChanceWagBag;
        Test.setCurrentPage(pageRef);
        List<String> lstResCode = new List<String>();
        lstResCode.add('repeatOrder');
        lstResCode.add('OneClickDone');
        lstResCode.add('remindInWeek');
        lstResCode.add('preload');
        lstResCode.add('text');
        lstResCode.add('remindMe');
        Contact con2 = new Contact();
        con2.Id=con.Id;
        con2.GetM20Multi__c = System.now();
        update con2;
        if(Response!=null && Response == 'Update'){
           Contact con3 = new Contact();
           con3.Id=ContactId;
           con3.GetM20Multi__c = System.now();
           update con3;
        }
        // Test.startTest();
            for(String strings:lstResCode){
                System.currentPageReference().getParameters().put('ResponseCode', strings);
                System.currentPageReference().getParameters().put('contactId', con.Id);
        	    ROPEx_WagBagController controller = new ROPEx_WagBagController();
                controller.InitPage();
            }
        // Test.stopTest();
        
    }

    @isTest
    private static void testmethod2(){
        Contact con = new Contact();
        con.LastName='lastname';
        insert con;
        ApexPages.currentPage().getParameters().put('contactId',con.Id);
        ROPEx_WagBagController controller = new ROPEx_WagBagController();
        Test.startTest();
            ApexPages.currentPage().getParameters().put('anchorName','Yes');
            controller.redirectToVoilla();
            ApexPages.currentPage().getParameters().put('anchorName','chat');
            controller.redirectToLchat();
        Test.stopTest();
    }

}