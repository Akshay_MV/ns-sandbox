/**
 * taskCleanup_Batch_Scheduler Class
 * @author: M. Wicherski, MK Partners 11/2012
 * @Description: Control method for scheduling of taskCleanup_Batch job(s)
 */

/*
	To Run:
		taskCleanup_Batch_Scheduler m = new taskCleanup_Batch_Scheduler();
		String sch  =  '0 0 * * * ?';
		String jobId = system.schedule('taskCleanup_Batch', sch, m);

	To Check Status:
		CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
 
*/

global  class  taskCleanup_Batch_Scheduler{
	/*global  void  execute(SchedulableContext  sc)  {
		taskCleanup_Batch b = new taskCleanup_Batch();
		database.executebatch(b,1);
	}

	@isTest 
	static void taskCleanup_Batch_Scheduler_Test() {
		taskCleanup_Batch job = new taskCleanup_Batch();
		Id processId = Database.executeBatch(job);
		
		contact c1 = new Contact();
		c1.lastname = 'test1';
		c1.Last_Order_Date__c = system.today();
		
		insert c1; 
		
		Task t = new Task();
		t.Subject = 'DEBUG';
		t.Description = '';
		t.Status = 'Debug';
		t.WhoId = c1.id;
		
		insert t;
		
		string x = ropBatch.getQueryString();
		list<Contact> customers = new list<Contact>{c1};
		ropBatch.exec(customers);
		
		taskCleanup_Batch_Scheduler m = new taskCleanup_Batch_Scheduler();
		String sch  =  '0 0 * * * ?';
		String jobId = system.schedule('test_taskCleanup_Batch', sch, m);  
	}*/

}