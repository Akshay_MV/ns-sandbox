public class SignUpController 
{
    public static String FirstName {get;set;}
    public static String LastName {get;set;}
    public static String Email {get;set;}
    public static String Password {get;set;}
    public static String conPassword {get;set;}
    public static Boolean success{get;set;}
    
    // Page Message
    public String PageMsg {get;set;}
    
    public pagereference Save()
    {
        System.debug(FirstName + ' FirstName');
        System.debug(LastName + ' LastName');
        System.debug(Email+ ' Email');
        System.debug(Password + ' pass');
        System.debug(conPassword + ' conPassword');
        

        List<Contact>conList = [SELECT Id,Email FROM Contact WHERE Email =: Email];
        List<Site_User__c>siteUserList = [SELECT Id,UserName__c FROM Site_User__c WHERE UserName__c =: Email];
        if(conList.size() > 0 && siteUserList.size() == 0)
        {
            PageMsg='This Email is already registerd !';
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'This Email is already registerd '));
            if(FirstName == null || FirstName == ''){
                PageMsg='Please enter first name';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter FirstName'));
                return null;
            }else if(LastName == null || LastName == ''){
                PageMsg='Please enter last name';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter LastName'));
                return null;
            }else if(Email == null || Email ==  ''){
                PageMsg='Please enter email';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter Email')); 
                return null;
            }else if(!Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}[.]{0,1}[a-zA-Z]{0,2}', Email)){
                PageMsg='Please enter valid email';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please check your Email')); 
                return null;
            }else if(Password == null || Password ==  ''){
                PageMsg='Please enter password';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter Password'));
                return null;
            }
            // else if(!Pattern.matches('[a-zA-Z0-9@*#]{8,20}',Password)){
            //     PageMsg='Please enter the valid  password';                
            //     return null;   
            // }
            else if(Password != conPassword){
                PageMsg='Password does not match';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Password does not match!'));
                return null;                
            }else if(conPassword == null || conPassword == ''){
                PageMsg='Please confirm your password';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please confirm your Password'));
                return null;
            }else{
                Site_User__c stUser = new Site_User__c();
                stUser.UserName__c	= Email;
                stUser.Password__c  = Password;
                stuser.Contact__c   = conList[0].Id;
                insert stUser;
                success= true;
                Pagereference pr = new Pagereference('/apex/LoginPage');
                pr.setRedirect(true);
                return pr;
            }
        }
        else if(siteUserList.size() > 0 && conList.size() > 0){
            PageMsg='This Email is already registerd';
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'This Email is already registerd '));
            return null;
        }else if(siteUserList.size() == 0 && conList.size() == 0)
        {
            if(FirstName == null || FirstName == ''){
                PageMsg='Please enter first name';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter FirstName'));
                return null;
            }else if(LastName == null || LastName == ''){
                PageMsg='Please enter last name';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter LastName'));
                return null;
            }else if(Email == null || Email ==  ''){
                PageMsg='Please enter email';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter Email')); 
                return null;
            }else if(!Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}[.]{0,1}[a-zA-Z]{0,2}', Email)){
                PageMsg='Please enter the valid Email';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please check your Email')); 
                return null;
            // }else if(!Pattern.matches('[a-zA-Z0-9 @#$]',Password)){
            //     PageMsg='Not valid password use Atleast One UpperCase , One LowerCase , One SpecialCase , One Number';
            //     return null;
            }else if(Password == null || Password ==  ''){
                PageMsg='Please enter password';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter Password'));
                return null;
           
            }else if(conPassword == null || conPassword == ''){
                PageMsg='Please confirm your password';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please confirm your Password'));
                return null;
            }else{                 
                Contact con = new Contact();
                con.LastName = LastName;
                con.FirstName = FirstName;
                con.Email = Email;
                insert con;
                
                Site_User__c stUser = new Site_User__c();
                stUser.UserName__c	= Email;
                stUser.Password__c  = Password;
                stuser.Contact__c   = con.Id;
                insert stUser;
                success = true;
                Pagereference pr = new Pagereference('/apex/LoginPage');
                pr.setRedirect(false);
                return pr;
            }
        }else{
            return null;
        }
    }  
    public PageReference LoginPage(){
        PageReference pr = new PageReference('/NaturesSelect/LoginPage');
        return pr;
    }
}