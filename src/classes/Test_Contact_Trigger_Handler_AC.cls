@isTest
private class Test_Contact_Trigger_Handler_AC {  
            public static Map<string, string> MapMappingTable = new map<string,string>();
    private static testMethod void callFutureMethod() {
       

        
            Contact con = new Contact(LastName = 'Test', NSCart__INACTIVE_REASON__c = 'VET FOOD');
            insert con;      
            List<Id> setconId = new List<ID>();
            setconId.add(con.id);
            try{
                    Test.startTest();
                    //instruct Apex runtime to send this fake response by calling this method 
                    Test.setMock(HttpCalloutMock.class, new TestCalloutMock());
                    Contact_Trigger_Handler.OnAfterInsertAsync(setconId);   
                    Test.stopTest();
            }catch(Exception ex){
            }

    }
    private static testMethod void callFutureMethodUpdate() {
        
         String OptOutYes ='Yes';
         String StoreRangeYes = 'Yes';
                    
            Contact con = new Contact(LastName = 'Test',AC_ID__c ='4');    
            Contact con1 = new Contact(LastName = 'Test',AC_ID__c =Null);           
            try
            {
               insert con;
            }
            catch(Exception e) {

               Boolean expectedExceptionThrown =  e.getMessage().contains('A Contact with this AC_ID') ? true : false;
               System.AssertEquals(expectedExceptionThrown, true);
               System.AssertEquals(expectedExceptionThrown, false);
                   
            }
            try
            {
               insert con1;
            }
            catch(Exception e){  
                  
               Boolean expectedExceptionThrown =  e.getMessage().contains('A Contact with this AC_ID') ? true : false;
               System.AssertEquals(expectedExceptionThrown, true);
               System.AssertEquals(expectedExceptionThrown, false);
                   
            }
            con.LastName = 'Test1';
            //con.Id = con.Id;
            con.AC_ID__c = '4';
            update con;

            List<Id> setconId = new List<ID>();
            setconId.add(con.id);
            try{
                    Test.startTest();
                    //instruct Apex runtime to send this fake response by calling this method 
                    Test.setMock(HttpCalloutMock.class, new TestCalloutMock());
                    Contact_Trigger_Handler.OnAfterUpdateAsync(setconId);    
                    Test.stopTest();
            }catch(Exception ex){

            }

    }
    private static testMethod void TestDuplicateInsertTrigger() {
                    
            Contact con = new Contact(LastName = 'Test',AC_ID__c ='5');
            insert con;    
            Contact con1 = new Contact(LastName = 'Test',AC_ID__c ='5');           
            try
            {
               insert con1;
            }
            catch(Exception e) {

               Boolean expectedExceptionThrown =  e.getMessage().contains('A Contact with this AC_ID') ? true : false;
               System.AssertEquals(expectedExceptionThrown, true);           
            }

    }
    private static testMethod void Test_CustomSetting(){
            //Covering Custom Setting
           
  
            String fields ='';
            List<String> fieldsetof = new List<String>();
            Contact__c setting = new Contact__c();
            setting.Name = 'Test Setting';
            setting.AC_Field_Name__c = 'Whatever';
            insert setting;
        
         for(Contact__c mappingTableRec  : Contact__c.getAll().values())
          {
              if (mappingTableRec.Name != null && mappingTableRec.AC_Field_Name__c != Null )
              {       fieldsetof.add(mappingTableRec.Name);
                      MapMappingTable.put(mappingTableRec.AC_Field_Name__c , mappingTableRec.Name);
                      //MapMappingTable.put(mappingTableRec.Name , mappingTableRec.AC_Field_Name__c);
                      fields += mappingTableRec.Name + ', ';
                 
              }
          }
        


    }
}