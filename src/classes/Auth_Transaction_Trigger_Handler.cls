public class Auth_Transaction_Trigger_Handler {
	List<NSCart__Authorize_net_Transaction__c> recordNewList = new List<NSCart__Authorize_net_Transaction__c>();
    List<NSCart__Authorize_net_Transaction__c> recordOldList = new List<NSCart__Authorize_net_Transaction__c>();
    Map<Id, NSCart__Authorize_net_Transaction__c> recordNewMap = new Map<Id, NSCart__Authorize_net_Transaction__c>();
    Map<Id, NSCart__Authorize_net_Transaction__c> recordOldMap = new Map<Id, NSCart__Authorize_net_Transaction__c>();
    Boolean isInsert, isUpdate, isDelete, isUndelete = false;
    public static boolean recursionController = false;
    
    public Auth_Transaction_Trigger_Handler(List<NSCart__Authorize_net_Transaction__c> newList, List<NSCart__Authorize_net_Transaction__c> oldList, Map<Id, NSCart__Authorize_net_Transaction__c> newMap, Map<Id, NSCart__Authorize_net_Transaction__c> oldMap, boolean isInsert, boolean isUpdate, Boolean isDelete, Boolean isUndelete) {
        this.recordNewList = newList;
        this.recordOldList = oldList;
        this.recordNewMap = newMap;
        this.recordOldMap = oldMap;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUndelete = isUndelete;
    }
    
    public void BeforeInsertEvent(){}
    
    public void BeforeUpdateEvent(){}
    
    public void BeforeDeleteEvent(){}
    
    public void AfterInsertEvent(){
        FieldHistory();
    }
    
    public void AfterUpdateEvent(){
        FieldHistory();
    }
    
    public void AfterDeleteEvent(){}
    
    public void AfterUndeleteEvent(){}
    
    public void FieldHistory(){
        List<Authorize_net_Transactions_History__c> AuthHistoryLst = new List<Authorize_net_Transactions_History__c>();
        Authorize_net_Transactions_History__c AuthHstry = new Authorize_net_Transactions_History__c();
        for(NSCart__Authorize_net_Transaction__c TransactionNewLst: recordNewList){            
            
            if(isInsert){
                
                AuthHstry.New_Amount__c = TransactionNewLst.NSCart__Amount__c;
                AuthHstry.New_Transaction_type__c = TransactionNewLst.NSCart__Transaction_type__c;
                AuthHstry.Old_Amount__c = null;
                AuthHstry.Old_Transaction_type__c = null;
                AuthHstry.Authorize_net_Transaction__c = TransactionNewLst.Id;

            }else{
                // CHECKING AMOUNT IS CHANGED OR NOT
                if(TransactionNewLst.NSCart__Amount__c != recordOldMap.get(TransactionNewLst.Id).NSCart__Amount__c){
                    AuthHstry.Old_Amount__c = recordOldMap.get(TransactionNewLst.Id).NSCart__Amount__c;
                    AuthHstry.New_Amount__c = TransactionNewLst.NSCart__Amount__c;
                }else{
                    AuthHstry.Old_Amount__c = recordOldMap.get(TransactionNewLst.Id).NSCart__Amount__c;
                    AuthHstry.New_Amount__c = recordOldMap.get(TransactionNewLst.Id).NSCart__Amount__c;
                }

                // CHECKING TRANSACTION_TYPE IS CHANGED OR NOT
                if(TransactionNewLst.NSCart__Transaction_type__c != recordOldMap.get(TransactionNewLst.Id).NSCart__Transaction_type__c){
                    AuthHstry.Old_Transaction_type__c = recordOldMap.get(TransactionNewLst.Id).NSCart__Transaction_type__c;
                    AuthHstry.New_Transaction_type__c = TransactionNewLst.NSCart__Transaction_type__c;
                }else{
                    AuthHstry.Old_Transaction_type__c = recordOldMap.get(TransactionNewLst.Id).NSCart__Transaction_type__c;
                    AuthHstry.New_Transaction_type__c = recordOldMap.get(TransactionNewLst.Id).NSCart__Transaction_type__c;
                }

                // ADD TRANSACTION ID FOR LOOKUP
                if(TransactionNewLst.NSCart__Amount__c != recordOldMap.get(TransactionNewLst.Id).NSCart__Amount__c || TransactionNewLst.NSCart__Transaction_type__c != recordOldMap.get(TransactionNewLst.Id).NSCart__Transaction_type__c){
                    AuthHstry.Authorize_net_Transaction__c = TransactionNewLst.Id;
                }                
            }

            // ADDING OBJECT RECORD TO LIST
            AuthHistoryLst.add(AuthHstry);
        }

        // CHECKING LIST IS EMPTY OR NOT
        if(AuthHistoryLst.size() != 0 || AuthHistoryLst.size() != null){                        
            insert AuthHistoryLst;
        }

    }
    
}