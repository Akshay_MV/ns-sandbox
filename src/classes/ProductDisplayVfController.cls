public class ProductDisplayVfController {
    //  @AuraEnabled
    public static String ProductId {get;set;}
    public static List<Product2> ProductList{get;set;}
    public static List<Product2> productLst{get;set;}
    public  static List<Product2> ProductList1 {get;set;}
    public static List<Product2> SuggestedPrductList {get;set;}
    public  String ProI {get;set;}
    public  static String ProductIds {get;set;}
    public  static String QuantityIds {get;set;}
    public  static boolean data{get;set;}
    public  static Map<String,Decimal> QuanMap {get;set;}
    public static Decimal SubTotal{get;set;}
    public  List<Site_User__c> UserList{get;set;}
    public Integer UserListSize{get;set;}
    public static Integer phone{get;set;}
    public List<String> upcell {get;set;}
    
    public void init(){

    }   

    // CONSTURCTOR
    public ProductDisplayVfController(){
        init();
        getUpcell();
        String data = '';
        Cookie counter = ApexPages.currentPage().getCookies().get('valueLogin');
        System.debug('counter'+counter);
        if(counter != null){
            data = apexpages.currentPage().getCookies().get('valueLogin').getValue();
        }
        String LoginId = data;
        UserListSize = 0;
         UserList = new List<Site_User__c>();
         UserList = [Select Id, UserName__c,Contact__r.FirstName,Contact__r.LastName,Contact__r.Phone, Contact__r.uid__c, Password__c from Site_User__c Where Id=: LoginId LIMIT 1];
         UserListSize =UserList.size();
         System.debug('UserList'+UserList);
         
        ProductList1 = new List<Product2>();
        String IdParam ='';
        IdParam = ApexPages.currentPage().getParameters().get('Id');

        if(IdParam != null){
            ProductByHomePage(IdParam);
        }
        else{
            getProductIds();
        }        
        
    }
    public PageReference getUserInformation(){
        //  List<Site_User__c> UserDataList = UserList;
        //  UserList = [Select Id, UserName__c,Contact__r.FirstName,Contact__r.LastName, Contact__r.uid__c, Password__c from Site_User__c LIMIT 1];
         System.debug('phone'+phone);
         return null;
        
    }

    public void getUpcell(){
        upcell = new List<String>{'Add A Healthy Treat','Add A Bone Or Chew...','Add Popular Health Supplements','Add A Fun Toy','Add Cat Products'};
    }


    public static void getData(){
        // List<Product2> productList = new List<Product2>();
        productLst = Database.query(util.queryData('Product2','',' LIMIT 4'));
        // return productList[0];
    }
    
    public static void getProducts(){
         List<String> QuanList = QuantityIds.split(',');
         QuanMap = new Map<String,Decimal>();
         System.debug(QuanList);
        for(String str : QuanList){
            System.debug('str'+str);
            QuanMap.put(str.split('::')[0],Decimal.valueOf(str.split('::')[1]));
        }
        
        List<String> PrList = ProductIds.split(',');
        Set<Id>prId = new Set<Id>();
        for(String str : PrList){
            prId.add(str);
        }
        System.debug(PrList);
        ProductList1 = new List<Product2>();
        ProductList1 = [SELECT Id, Name, NSCart__Full_size_image__c,(SELECT Id, UnitPrice FROM PricebookEntries), NSCart__Cart_Subcategory__c FROM Product2 WHERE Id =: prId   ORDER BY CreatedDate DESC];
        System.debug('ProductList'+ProductList1);
        data = true;
        SubTotal = 0.00;
        for(Product2 pro : ProductList1){
            if(pro.PricebookEntries[0].UnitPrice != null){
                SubTotal += pro.PricebookEntries[0].UnitPrice * QuanMap.get(pro.Id);
            }
        }
        System.debug('SubTotal'+SubTotal);

        // List<String> upcellref = new List<String>();

        // upcellref.add('Add Your Buiscuits');
        // upcellref.add('Add A Helathy Treat');
        // upcellref.add('Add A Bone Or Chew');
        // upcellref.add('Add Popular Health Supplements');
        // upcellref.add('Add A Fun Toy');

        // upcell = upcellref;
    }

    // @AuraEnabled
    public static void getProductIds(){
        // return [ SELECT Id FROM Product2 LIMIT 5 ];
        // List<Product2> productList = new List<Product2>();
        // productList = Database.query(Util.queryData('Product2','',' WHERE Id =: ProductId'));
        // List<Product2> ProductList 
        // String ProductId = '01t3t000004h62c';
        // ProductList = Database.query(util.queryData('Product2','',' LIMIT 4'));
        ProductList = [SELECT Id,Name,Sale_BestSeller_OurChoice__c,NSCart__Full_size_image__c,Description,(SELECT Id,UnitPrice FROM PricebookEntries),NSCart__Cart_Subcategory__c FROM Product2 LIMIT 4];
        SuggestedPrductList = new List<Product2>();
        SuggestedPrductList = [SELECT Id, Name, NSCart__Full_size_image__c,NSCart__Thumbnail_image__c,(SELECT Id, UnitPrice FROM PricebookEntries), NSCart__Cart_Subcategory__c FROM Product2 WHERE NSCart__Full_size_image__c != null  ORDER BY CreatedDate DESC LIMIT 4];
        // return Database.query(Util.queryData('Product2','',' WHERE Id =: ProductId'));
        // return ProductList;
        // return [ SELECT Id FROM Product2 WHERE Id = '01t3t000004h62c' ];
    }

   

    public void ProductByHomePage(String UrlId){
        List<Website_Filter_Data__c> WellnessProduct = new List<Website_Filter_Data__c>();
        WellnessProduct = [SELECT Id,Text_Data__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name =: UrlId AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate LIMIT 4];

        Set<Id> proIds = new Set<Id>{WellnessProduct[0].Text_Data__c,WellnessProduct[1].Text_Data__c,WellnessProduct[2].Text_Data__c,WellnessProduct[3].Text_Data__c};
        ProductList = [SELECT Id,Name,Sale_BestSeller_OurChoice__c,NSCart__Full_size_image__c,Description, (SELECT Id, UnitPrice FROM PricebookEntries),NSCart__Cart_Subcategory__c FROM Product2 WHERE Id =: proIds];
    }
}