@isTest
global class Test_MockCallout implements HttpCalloutMock{  
    global HttpResponse respond(HTTPRequest req){
            HttpResponse res = new HttpResponse();
            System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
            System.assertEquals('GET', req.getMethod());
            req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            res.setBody('{}');
            res.setStatus('OK');
            res.setStatusCode(200);
            return res;
    }
}