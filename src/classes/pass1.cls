public class pass1 {
    public static String Email {get;set;}
    public static Boolean success {get;set;}
    
    public String EmailSent {get;set;}

    //Page Messages
    public String PageMsg{get;set;}
    public String EmailSuccess{get;set;}

    public void Sent(){
        list<Site_User__c> con = new List<Site_User__c>();
        
        con = [SELECT Id,UserName__c,Contact__c FROM Site_User__c WHERE UserName__c =: Email];
        if(Email == null || Email == ''){
            PageMsg = 'Please Enter Your Email';
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter your email'));   
        }
        else if(con.isEmpty()){
            PageMsg = 'Can not Find Your Account';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Could not find your Account' ));
        }
        else{
            for(Site_User__c c : con){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {c.UserName__c}; 
                    mail.setToAddresses(toAddresses);
                //mail.setTargetObjectId(c.Contact__c);
                mail.setSenderDisplayName('Dharmik Shah');
                mail.setSubject('Reset Password');
                mail.setPlainTextBody('Hello, <br/><br/> Please reset your password from click on below url <br/><br/> Click here');
                mail.setHtmlBody('Hello, <br/><br/> Please reset your password from click on below url <br/><br/> <a href=https://mvclouds-nschicago.cs18.force.com/NaturesSelect/ForgotPassConfirm?id='+c.Id+'>click here.</a>');
                // Send the email you have created.
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                
                EmailSuccess = 'Email Sent Successfully';
               //apexpages.addMessage(new ApexPages.message(Apexpages.Severity.CONFIRM,'Email sent successfully'));
                success=true;
            }
        }
    }
    
}