public with sharing class couponEdit_Extension {
	
	/*public RecordType r {get;set;}

	public couponEdit_Extension (Apexpages.standardcontroller con){
		r = new RecordType();	
		if (apexpages.currentpage().getparameters().get('RecordType') != null) {
			r = [select id, name from RecordType where id = :apexpages.currentpage().getparameters().get('RecordType') AND sobjectType='Coupon__c'];	
		}
		contact customer;
		if (apexpages.currentpage().getparameters().get('customer')!= null) {
			customer = [select id,name,lastname, firstname from contact where id = :apexpages.currentpage().getparameters().get('customer')];
			con.getRecord().put('customer__c',customer.id);
		}
		
		if (apexpages.currentpage().getparameters().get('ctype')!= null) {
			string ctype = apexpages.currentpage().getparameters().get('ctype');
			con.getRecord().put('start_date__c',system.today());
			string rtype = (ctype=='i')?'Individual Coupon':(ctype=='c')?'Credit Coupon':(ctype=='r')?'Referral Reward':(ctype=='n')?'New Customer':(ctype=='s')?'Standard Coupon':'';
			if (rtype.trim().length()>0){
				r = [select id, name from RecordType where name=:rtype AND sobjectType='Coupon__c'];
				con.getRecord().put('recordtypeid',r.id);
			}
			//set individual coupon defaults
			if (ctype=='i'){
				con.getRecord().put('Max_Uses__c',1);
				con.getRecord().put('Min_Order_Amnt_for_Target__c',35);
			}
			//set standard coupon defaults
			if (ctype=='s'){
				con.getRecord().put('Min_Order_Amnt_for_Target__c',35);
			}
			//set credit coupon defaults
			if (ctype == 'c'){
				con.getRecord().put('coupon_type__c','Entire Order');
				con.getRecord().put('name','Credit'+customer.lastname+'{auto#}');
				con.getRecord().put('discount_type__c','Dollar Off');
				con.getRecord().put('Max_Uses__c',1);
				con.getRecord().put('Max_Individual_Uses__c',1);
				con.getRecord().put('expiration_date__c',system.today().adddays(365));
				con.getRecord().put('Min_Order_Amnt_for_Target__c',35);
			}
			//set New Customer coupon defaults
			if (ctype=='n'){
				con.getRecord().put('Max_Individual_Uses__c',1);
				con.getRecord().put('Min_Order_Amnt_for_Target__c',35);
			}
			//set referral coupon defaults
			if (ctype=='r') {
				con.getRecord().put('name','Thanks'+customer.firstname+'{auto#}');
				con.getRecord().put('expiration_date__c',system.today().adddays(365));
				con.getRecord().put('Max_Uses__c',1);
				con.getRecord().put('Max_Individual_Uses__c',1);
				con.getRecord().put('Min_Order_Amnt_for_Target__c',35);
			}
		}
		
		if (apexpages.currentpage().getparameters().get('amt')!= null) {
			con.getRecord().put('discount_amount__c',apexpages.currentpage().getparameters().get('amt'));
		}
		
		if (apexpages.currentpage().getparameters().get('maxiuse')!= null) {
			con.getRecord().put('',apexpages.currentpage().getparameters().get('maxiuse'));
		}
		
	} 
	
	@isTest (seealldata=true)
	static void couponEdit_test(){
		contact c = new contact(lastname='last', firstname='first');
		insert c;
		
		Pagereference p = page.coupon_Edit;
		p.getparameters().put('RecordType',[select id from recordtype where name = 'Individual Coupon' and sobjecttype ='Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		apexpages.standardcontroller scon = new apexpages.standardcontroller(new coupon__c());
		couponEdit_Extension ext = new couponEdit_Extension(scon);
	
		p.getparameters().put('RecordType',[select id from recordtype where name = 'Credit Coupon' and sobjecttype ='Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('RecordType',[select id from recordtype where name = 'Referral Reward' and sobjecttype ='Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('RecordType',[select id from recordtype where name = 'New Customer' and sobjecttype ='Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('ctype',[select id from recordtype where name = 'Credit Coupon' and sobjecttype ='Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('ctype',[select id from recordtype where name = 'Referral Reward' and sobjecttype ='Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('ctype',[select id from recordtype where name = 'New Customer' and sobjecttype ='Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('ctype',[select id from recordtype where name = 'Individual Coupon' and sobjecttype ='Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new coupon__c());
		ext = new couponEdit_Extension(scon);
		
	}*/
}