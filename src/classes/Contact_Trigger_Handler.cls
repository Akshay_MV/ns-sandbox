public class Contact_Trigger_Handler {


     public static Boolean isCallOutFire = false;
     public static Map<string, string> MapMappingTable = new map<string,string>();
     //public Static String qry = '';
     public Static String allfields ='';

     @future (callout=true)
     public static void OnAfterInsertAsync(List<ID> newContactIDs){
        //   List<sObject> ListOfCon = new List<Contact>();

        //   //*****************************************************
        //   List<String> fieldsetof = new List<String>();
        //   String fields ='';
        //   for(Contact__c mappingTableRec  : Contact__c.getAll().values())
        //   {
        //       if (mappingTableRec.Name != null && mappingTableRec.AC_Field_Name__c != Null )
        //       {       fieldsetof.add(mappingTableRec.Name);
        //               MapMappingTable.put(mappingTableRec.AC_Field_Name__c , mappingTableRec.Name);
        //               //MapMappingTable.put(mappingTableRec.Name , mappingTableRec.AC_Field_Name__c);
        //               fields += mappingTableRec.Name + ', ';
        //               //System.debug('@@@@@@'+fields);
        //               //System.debug('@@@@@@MapMappingTable::'+MapMappingTable);
        //       }
        //   }
        //   String qry2 = ' Select ' + String.join(fieldsetof, ',') + ' from Contact' + ' where Id =:newContactIDs'; 
        //   if(fields != '')
        //   {
        //       ListOfCon = Database.query(qry2);
        //       System.debug('@@@@@@@@@@@@ListOfCon to Post is' + ListOfCon);
        //   }

        // String ampersand = '';
        //   for(sObject con : ListOfCon){
        //      for(String mapfields : MapMappingTable.keySet()){
        //          allfields += ampersand + mapfields+'='+con.get(MapMappingTable.get(mapfields));
        //          ampersand = '&';
        //          System.debug('@@@@ allfields::::::'+allfields);
        //      }
        // }
        // List < Contact > ConList = [SELECT NSCart__Active__c, FirstName, LastName, ACLead__c, Email, HasOptedOutOfEmail, LeadSource, Days_Since_Last_Order_Placed__c, NSCart__Last_Date_Ordered__c,
        //                               NSCart__Total_Number_Of_Orders__c, NSCart__Total_Number_Of_Sales__c, Phone, Ship_to_ZIP__c, Ship_to_State__c,
        //                               Dog_1_Name__c, Dog_1_Breed__c, Dog_1_Life_Stage__c, NSCart__Frequency__c, NSCart__Issue_1__c,
        //                               MailingPostalCode,MailingCountry, MailingState, MailingCity, MailingStreet,   
        //                               Deactivated_Date__c,NSCart__INACTIVE_REASON__c,Ship_to_City__c,Ship_to_Street_Address__c,Store_Range__c
        //                               FROM Contact WHERE NSCart__Active__c = True AND Id =: newContactIDs];
        

        // for (Contact record: ConList) {
        //     try {

        //         //Credentials                                                                                              
        //         String username = String.valueOf(Label.Username);
        //         String password = String.valueOf(Label.Password);
        //         String api_key = String.valueOf(Label.AC_Api_Key);

        //         //Construct Endpoint
        //         String callout_url = String.valueOf(Label.Active_Campaign_Call_back_URL);


        //         //*********Map ActiveCampaign field to salesforce field******//
        
        //         String ACLead = record.ACLead__c;
        //         String tags;
        //         String NSCartLastDateOrdered = record.NSCart__Last_Date_Ordered__c == null ? '' : record.NSCart__Last_Date_Ordered__c.format();
        //         String DeactivatedDate = record.Deactivated_Date__c == null ? '' : record.Deactivated_Date__c.format();
        //         String OptOutYes;
        //         String StoreRangeYes;

        //         //*************************
        //         if(record.HasOptedOutOfEmail == true){
        //           System.debug('Has Opted');
        //           OptOutYes ='Yes';
        //         }else{
        //           OptOutYes ='No';
        //           System.debug('Has Opted');
        //         }
        //         //Store Range field
        //         if(record.Store_Range__c == true){
        //           StoreRangeYes ='True';
        //         }else{
        //           StoreRangeYes = '';
        //         }

        //         if(record.NSCart__Total_Number_Of_Orders__c > 0){
        //           tags = 'Customer';
        //         }else if(record.NSCart__Total_Number_Of_Orders__c <1){
        //           tags ='Lead';
        //         }else{
        //             tags ='Api';
        //         }
        //         //*************************
        //         //*********Map ActiveCampaign field to salesforce field******//


        //         //*******Construct HTTP request and response*****//
        //         Http http = new Http();
        //         HttpRequest req = new HttpRequest();
        //         HttpResponse res = new HttpResponse();
        //         //*******Construct HTTP request and response******//


        //         //*********Construct Authorization and Content header******//
        //         Blob headerValue = Blob.valueOf(username + ':' + password);
        //         String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        //         req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        //         req.setHeader('Authorization', authorizationHeader);
        //         //*********Construct Authorization and Content header******//

 
        //         //***********Set Method and Endpoint and Body***********//
        //         req.setMethod('POST');
        //         req.setEndpoint(callout_url);

        //         String body1= 'api_key=' + api_key +'&p[1]=' +1 + '&tags=' +tags + '&field[%LAST_ORDER_DATE%,0]=' +NSCartLastDateOrdered +
        //                       '&field[%O_OUT%,0]=' +OptOutYes+
        //                       '&field[%SHIP_TO_CITY%,0]=' +record.MailingCity+
        //                       '&field[%SHIP_TO_STATE%,0]=' +record.MailingState +
        //                       '&field[%SHIP_TO_ZIP%,0]=' +record.MailingPostalCode  +
        //                       '&field[%SHIP_TO_COUNTRY%,0]=' +record.MailingCountry  +
        //                       '&field[%SHIP_TO_STREET_ADDRESS%,0]=' +record.MailingStreet +
        //                       '&field[%CTIVATE_DATE%,0]=' +DeactivatedDate+
        //                       '&field[%RANGE%,0]=' +StoreRangeYes;
                                          
        //         //req.setBody(finalbody);
        //         //allfields.remove(null);
        //         req.setBody(body1+'&'+allfields);
        //         System.debug('Body Sent to insert Is***###--->' + req.getBody());
        //         //***********Set Method and Endpoint and Body***********//

        //         //******* Send endpoint request to ActiveCampiagn********//
        //         res = http.send(req);

        //         if (res.getStatusCode() == 201) {
        //             System.debug('Post Success' + res);
        //             System.debug(res.getBody());

        //         } else {
        //             System.debug('Callout SuccessFull: ' + res);
        //             System.debug(res.getBody());
        //         }
        //         //******** Send endpoint request to ActiveCampiagn *******//


        //         //*******Get Id of the inserted contact *****************//
        //         Dom.Document doc = res.getBodyDocument();    // retrieves the body of this request as a DOM document
        //         //Retrieve the root element for this document.
        //         Dom.XMLNode xmlResponse = doc.getRootElement();
        //         System.debug('Name of the root###' +xmlResponse.getName()); // prints out the name of the root
        //         Contact contacts = new Contact();
        //         for(Dom.XMLNode child : xmlResponse.getChildElements())
        //         {     
        //               System.debug('Child Name***' +child.getName());
        //               System.debug('Child Text**** or Contact Id:' +child.getText()); // will get the id of added contact


        //           //Update SF contact with AC Id ******
        //           if (child.getname() == 'subscriber_id') {
        //               contacts.Id=record.Id;
        //               contacts.AC_Id__c = child.getText();
        //               update contacts;
        //           }
                        
        //         }
            
        //         //*******Get Id of the inserted contact *****************//


        //         } catch (System.CalloutException e) {
        //             System.debug('Callout error: ' + e);
        //             System.debug('Error::' + e.getMessage());
        //     }
        // }       

     }

     @future (callout=true)
     public static void OnAfterUpdateAsync(List<ID> updatedContactIDs){

        
        //   List<sObject> ListOfCon = new List<Contact>();

        //   //*****************************************************
        //   List<String> fieldsetof = new List<String>();
        //   String fields ='';
        //   for(Contact__c mappingTableRec  : Contact__c.getAll().values())
        //   {
        //       if (mappingTableRec.Name != null && mappingTableRec.AC_Field_Name__c != Null )
        //       {       fieldsetof.add(mappingTableRec.Name);
        //               MapMappingTable.put(mappingTableRec.AC_Field_Name__c , mappingTableRec.Name);
        //               //MapMappingTable.put(mappingTableRec.Name , mappingTableRec.AC_Field_Name__c);
        //               fields += mappingTableRec.Name + ', ';
        //               //System.debug('@@@@@@'+fields);
        //               //System.debug('@@@@@@MapMappingTable::'+MapMappingTable);
        //       }
        //   }
        //   String qry2 = ' Select ' + String.join(fieldsetof, ',') + ' from Contact' + ' where Id =:updatedContactIDs'; 
        //   if(fields != '')
        //   {
        //       ListOfCon = Database.query(qry2);
        //       System.debug('@@@@@@@@@@@@ListOfCon to Post is' + ListOfCon);
        //   }
        //   String ampersand = '';
        //   for(sObject con : ListOfCon){
        //      for(String mapfields : MapMappingTable.keySet()){
        //          allfields += ampersand + mapfields+'='+con.get(MapMappingTable.get(mapfields));
        //          ampersand = '&';
        //          System.debug('@@@@ allfields::::::'+allfields);
        //      }
        //   }
        // List<Contact> conListUpdate = [SELECT NSCart__Active__c, FirstName, LastName, ACLead__c, AC_ID__c, Email, HasOptedOutOfEmail, LeadSource, Days_Since_Last_Order_Placed__c, NSCart__Last_Date_Ordered__c,
        //                                 NSCart__Total_Number_Of_Orders__c, NSCart__Total_Number_Of_Sales__c, Phone, Ship_to_ZIP__c, Ship_to_State__c,
        //                                 Dog_1_Name__c, Dog_1_Breed__c, Dog_1_Life_Stage__c, NSCart__Frequency__c, NSCart__Issue_1__c,
        //                                 MailingCity, MailingState, MailingPostalCode, MailingCountry,MailingStreet,
        //                                 Deactivated_Date__c,NSCart__INACTIVE_REASON__c,Ship_to_City__c,Ship_to_Street_Address__c,Store_Range__c
        //                                 FROM Contact WHERE NSCart__Active__c = True AND Id =: updatedContactIDs];

        
        // for (Contact record: conListUpdate) {
        //     try {

        //         //Credentials                                                                                              
        //         String username = String.valueOf(Label.Username);
        //         String password = String.valueOf(Label.Password);
        //         String api_key = String.valueOf(Label.AC_Api_Key);
        //         String ACLead = record.ACLead__c;
                
        //         String OptOutYes;
        //         String StoreRangeYes;
        //         String tags;

        //         if(record.HasOptedOutOfEmail == true){
        //           OptOutYes ='Yes';
        //           System.debug('Has Opted');
        //         }else{
        //           OptOutYes = 'No';
        //           System.debug('Has Opted');
        //         }
        //         //Store Range formula field

        //         if(record.Store_Range__c == true){
        //           StoreRangeYes ='True';
        //         }else{
        //           StoreRangeYes = '';
        //         }
        //         String NSCartLastDateOrdered = record.NSCart__Last_Date_Ordered__c == null ? '' : record.NSCart__Last_Date_Ordered__c.format();
        //         String DeactivatedDate = record.Deactivated_Date__c == null ? '' : record.Deactivated_Date__c.format();

        //         if(record.NSCart__Total_Number_Of_Orders__c > 0){
        //           tags = 'Customer';
        //         }else{
        //             tags ='Api';
        //         }
              

        //         //Construct Endpoint
        //         String callout_url = String.valueOf(Label.Active_Campaign_Call_back_URL_sync);

        //         //*******Construct HTTP request and response*****//
        //         Http http = new Http();
        //         HttpRequest req = new HttpRequest();
        //         HttpResponse res = new HttpResponse();
        //         //*******Construct HTTP request and response******//

        //         //*********Construct Authorization and Content header******//
        //         Blob headerValue = Blob.valueOf(username + ':' + password);
        //         String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        //         req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        //         req.setHeader('Authorization', authorizationHeader);
        //         //*********Construct Authorization and Content header******//

        //         req.setMethod('POST');
        //         req.setEndpoint(callout_url);
        //         String body1= 'api_key=' + api_key +'&p[1]=' +1 + '&tags=' +tags + '&field[%LAST_ORDER_DATE%,0]=' +NSCartLastDateOrdered +
        //                       '&field[%O_OUT%,0]=' +OptOutYes+
        //                       '&field[%SHIP_TO_CITY%,0]=' +record.MailingCity+
        //                       '&field[%SHIP_TO_STATE%,0]=' +record.MailingState +
        //                       '&field[%SHIP_TO_ZIP%,0]=' +record.MailingPostalCode  +
        //                       '&field[%SHIP_TO_COUNTRY%,0]=' +record.MailingCountry  +
        //                       '&field[%SHIP_TO_STREET_ADDRESS%,0]=' +record.MailingStreet +
        //                       '&field[%CTIVATE_DATE%,0]=' +DeactivatedDate+
        //                       '&field[%RANGE%,0]=' +StoreRangeYes;
                              
        //         //body1 = body1.replaceAll('\"[^\"]*\":null','');             
        //         //req.setBody(finalbody);
        //         req.setBody(body1+'&'+allfields);
              
        //         System.debug('Body Sent for update Is***###--->' + req.getBody());
        //         res = http.send(req);

        //         if (res.getStatusCode() == 201) {
        //             System.debug('Post Success' + res);
        //             System.debug(res.getBody());

        //         } else {
        //             System.debug('Callout SuccessFull: ' + res);
        //             System.debug(res.getBody());
        //         }

        //         }catch(System.CalloutException e){
        //     }
        // }

    }
   
}