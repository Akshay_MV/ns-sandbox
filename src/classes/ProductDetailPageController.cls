public class ProductDetailPageController {
    public Product2 Pro {get;set;}
    public String ProId {get;set;}
    public String url {get;set;}
    public Boolean CheckId {get;set;}
    public List<Product2> ProductListDetail {get;set;}
    public List<Product2> BuyAgainList {get;set;}
    public List<Website_Filter_Data__c> CategoriesList {get;set;}

    public List<Product2> gg {get;set;}
    public List<Product2> ProLt {get;set;}


    // public ProductDetailPageController(){
      
    public PageReference ErrorPage(){
        PageReference pr;
        
        url = ApexPages.currentPage().getUrl();
        CheckId = url.contains('?Id=');

        ProId = '';
        ProId = ApexPages.currentPage().getParameters().get('Id');

        if(CheckId || ProId == null || ProId == ''){
            pr = new PageReference('/NaturesSelect/ErrorPage');
            pr.setRedirect(true);
        }else{
            Pro = new Product2();
            Pro = [ SELECT Id, Name, Sale_BestSeller_OurChoice__c,Description, NSCart__Full_size_image__c FROM Product2 WHERE Id =: ProId LIMIT 1 ];
        }   
        return pr;
    }

    public ProductDetailPageController(){
        init();        
        BuyitAgainMethod();    
        ProLt = new List<Product2>(); 
    }

    public void ProductList(){
        ProductListDetail = new List<Product2>();
        ProductListDetail = [SELECT Id, Name, Slick__Full_size_image__c, (SELECT Id, UnitPrice FROM PricebookEntries) FROM Product2 ORDER BY CreatedDate DESC LIMIT 4];
    }

    public void init(){
        BuyAgainList = new List<Product2>();
        CategoriesList = new List<Website_Filter_Data__c>();
    }


    public void BuyitAgainMethod(){
        BuyAgainList = [SELECT Id, Name, NSCart__Full_size_image__c, NSCart__Thumbnail_image__c,ProductCode,Description,(SELECT Id, UnitPrice FROM PricebookEntries) FROM Product2 WHERE Name LIKE '%Buddy%' LIMIT 4];        

        
        // Integer flag = 0;
        // for(String s : util.getPickListValues('Product2','NSCart__Cart_Category__c')){
        //     //System.debug(s);
        //     if(s.length() < 8 && flag < 6){
        //         CategoriesList.add(s);
        //         flag +=1 ;
        //     }
        // }
        CategoriesList = [SELECT Id,Text_Data__c,Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Add Some Fun' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];

        gg = new List<Product2>();
        gg = [SELECT Id,Name,ProductCode FROM Product2 LIMIT 9];
        System.debug(gg);
    }

}