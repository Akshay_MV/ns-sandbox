public class ShopController {
    //INITIALIZE VARIABLES
    public List<String> ProList {get;set;}        
    public List<Product2> BuyAgainList {get;set;}
    public List<Website_Filter_Data__c> CategoriesList {get;set;}

    public List<Product2> ProductList {get;set;}
    public String SearchValue {get;set;}
    public String CategoriesValue {get;set;}   
    
    
    public Integer totalRecs {get;set;}
    public Integer count {get;set;}
    public Integer LimitSize {get;set;}
    
    public List<Product2> gg {get;set;}
    public List<Product2> ProLt {get;set;}
    public List<Website_Filter_Data__c> ShopWheelImages {get;set;}
    //CONSTUCTOR
    public ShopController(){
        init();        
        BuyitAgainMethod();        
        ProLt = new List<Product2>();
        totalRecs = [SELECT count() FROM Product2];
        shopwheel();
        
    }

     
    public void init(){
        BuyAgainList = new List<Product2>();
        CategoriesList = new List<Website_Filter_Data__c>();
        SearchValue = '';
        CategoriesValue = '';
        LimitSize = 12;
        totalRecs = 0;
        count = 0;
        ShopWheelImages = new List<Website_Filter_Data__c>();
    }

    public void shopwheel(){
        ShopWheelImages = [SELECT Id,Url__c,Text_Data__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Shop Wheel Images' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
    }

    public List<Product2> getProduct2(){
        System.debug('SearchValue   '+SearchValue);
        System.debug('CategoriesValue  '+CategoriesValue);
        // List<Product2> ld = [SELECT Id,Name,NSCart__Full_size_image__c FROM Product2 WHERE Name LIKE :SearchValue LIMIT:LimitSize OFFSET:count];
        List<Product2> ld = new List<Product2>();

        if(SearchValue != ''){
             ld = [SELECT Id,Name,NSCart__Full_size_image__c,ProductCode,Description,(SELECT Id, UnitPrice FROM PricebookEntries) FROM Product2 WHERE Name LIKE :SearchValue LIMIT:LimitSize OFFSET:count];
             System.debug('Search');
        }else{
            if(CategoriesValue != ''){
                ld = [SELECT Id,Name,NSCart__Full_size_image__c,ProductCode,Description,(SELECT Id, UnitPrice FROM PricebookEntries) FROM Product2 WHERE NSCart__Cart_Category__c = :CategoriesValue LIMIT:LimitSize OFFSET:count];
                System.debug('Categories');
            }else{
                ld = [SELECT Id,Name,NSCart__Full_size_image__c,ProductCode,Description,(SELECT Id, UnitPrice FROM PricebookEntries) FROM Product2 LIMIT:LimitSize OFFSET:count];
                System.debug('Main');
            }
        }
        System.debug('values are:'+ld);
        return ld;
    }

    public void updatePage(){
        ProLt.clear();        
        ProLt = [SELECT Id,Name,NSCart__Full_size_image__c,ProductCode,Description,(SELECT Id, UnitPrice FROM PricebookEntries) FROM Product2 WHERE Name LIKE :SearchValue LIMIT:LimitSize OFFSET:count];
    }

    public PageReference Firstbtn(){
        count=0;
        return null;
    }

    public PageReference prvbtn(){
        count= count-LimitSize;
        return null;
    }

    public PageReference Nxtbtn(){
        count=count+LimitSize;
        return null;
    }

    public PageReference lstbtn(){
        count=totalRecs - math.mod(totalRecs,LimitSize);
        return null;
    }

    public PageReference Searcc(){
        count = 0;
        SearchValue = '%'+ApexPages.currentPage().getParameters().get('SearchVal')+'%';
        // CategoriesValue = ApexPages.currentPage().getParameters().get('CateVal');
        return null;
    }
    public PageReference CatSearch(){
        count=0;
        SearchValue = '';
        CategoriesValue = ApexPages.currentPage().getParameters().get('CateVal');
        return null;
    }

    public Boolean getNxt(){
        if((count+LimitSize)>totalRecs){
            return true;
        }else{
            return false;
        }
    }

    public Boolean getPrv(){
        if(count==0){
            return true;
        }else{
            return false;
        }
    }


    // public void random(){
    //     Integer len = 3;
    //     String str = string.valueof(Math.abs(Crypto.getRandomLong()));
    //     String randomNumber = str.substring(0, len);
    //     system.debug('Random Number-' + randomNumber);
    // }

    public void BuyitAgainMethod(){
        BuyAgainList = [SELECT Id, Name, NSCart__Full_size_image__c, NSCart__Thumbnail_image__c,ProductCode,Description,(SELECT Id, UnitPrice FROM PricebookEntries) FROM Product2 WHERE Name LIKE '%Buddy%' LIMIT 4];        

        
        // Integer flag = 0;
        // for(String s : util.getPickListValues('Product2','NSCart__Cart_Category__c')){
        //     //System.debug(s);
        //     if(s.length() < 8 && flag < 6){
        //         CategoriesList.add(s);
        //         flag +=1 ;
        //     }
        // }
        CategoriesList = [SELECT Id,Text_Data__c,Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Add Some Fun' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];

        gg = new List<Product2>();
        gg = [SELECT Id,Name,ProductCode FROM Product2 LIMIT 9];
        System.debug(gg);
    }
    
}