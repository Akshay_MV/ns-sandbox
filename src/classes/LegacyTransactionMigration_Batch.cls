/*
Transfer data from Legacy_Transaction object(unmanaged) to NSCart__Legacy_transaction__c Object.
dynamic sobject to allow compile without package dependency.

DO NOT USE THIS FOR FRESH MIGRATIONS. Use the NSCartLegacyMigration_Batch for this.
*/
global class LegacyTransactionMigration_Batch{
	// implements Database.Batchable<sObject>, Database.Stateful
	/*
	String query='select '+ NSDataMigrationTool.queryAllFields('Legacy_Transaction__c')+' from Legacy_Transaction__c';
	global Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 

	
	global LegacyTransactionMigration_Batch() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		if(gd.containsKey('NSCart__Legacy_Transaction__c')){
   			Schema.SObjectType routeType = gd.get('NSCart__Legacy_Transaction__c');
   			list<sObject> newTransactions=new list<sObject>();
   			for(sObject rt:scope){
   				Legacy_Transaction__c oldTrans=(Legacy_Transaction__c)rt;
				sObject newTrans=routeType.newSObject();	
				newTrans.put('Name', oldTrans.name);		
				newTrans.put('NSCart__Amount__c', oldTrans.Amount__c);
				newTrans.put('NSCart__Authorization__c', oldTrans.Authorization__c);
				newTrans.put('NSCart__AVS_Response_Code__c', oldTrans.AVS_Response_Code__c);
				newTrans.put('NSCart__AVS_International__c', oldTrans.AVS_International__c);
				newTrans.put('NSCart__AVS_Zip__c', oldTrans.AVS_Zip__c);
				newTrans.put('NSCart__Bank_Account_Name__c', oldTrans.Bank_Account_Name__c);
				newTrans.put('NSCart__Bank_Account_Number__c', oldTrans.Bank_Account_Number__c);
				newTrans.put('NSCart__Bank_Account_Type__c', oldTrans.Bank_Account_Type__c);
				newTrans.put('NSCart__Bank_Name__c', oldTrans.Bank_Name__c);
				newTrans.put('NSCart__Bank_Routing_Number__c', oldTrans.Bank_Routing_Number__c);
				newTrans.put('NSCart__Billing_Address__c', oldTrans.Billing_Address__c);
				newTrans.put('NSCart__Billing_City__c', oldTrans.Billing_City__c);
				newTrans.put('NSCart__Billing_Company__c', oldTrans.Billing_Company__c);
				newTrans.put('NSCart__Billing_Country__c', oldTrans.Billing_Country__c);
				newTrans.put('NSCart__Billing_Email__c', oldTrans.Billing_Email__c);
				newTrans.put('NSCart__Billing_Fax__c', oldTrans.Billing_Fax__c);
				newTrans.put('NSCart__Billing_First__c', oldTrans.Billing_First__c);
				newTrans.put('NSCart__Billing_Last__c', oldTrans.Billing_Last__c);
				newTrans.put('NSCart__Billing_Phone__c', oldTrans.Billing_Phone__c);
				newTrans.put('NSCart__Billing_Postal_Code__c', oldTrans.Billing_Postal_Code__c);
				newTrans.put('NSCart__Billing_Province__c', oldTrans.Billing_Province__c);
				newTrans.put('NSCart__Billing_State__c', oldTrans.Billing_State__c);
				newTrans.put('NSCart__Credit_Card_Name__c', oldTrans.Credit_Card_Name__c);
				newTrans.put('NSCart__Credit_Card_Number__c', oldTrans.Credit_Card_Number__c);
				newTrans.put('NSCart__Credit_Card_Type__c', oldTrans.Credit_Card_Type__c);
				newTrans.put('NSCart__Currency__c', oldTrans.Currency__c);
				newTrans.put('NSCart__Description__c', oldTrans.Description__c);
				//newTrans.put('Details__c', oldTrans.Details__c);
				//newTrans.put('Details_Payflow__c', oldTrans.Details_Payflow__c);
				//newTrans.put('Gateway__c', oldTrans.Gateway__c);
				newTrans.put('NSCart__Gateway_Date__c', oldTrans.Gateway_Date__c);
				newTrans.put('NSCart__Gateway_ID__c', oldTrans.Gateway_ID__c);
				newTrans.put('NSCart__Opportunity__c', oldTrans.Opportunity__c);
				newTrans.put('NSCart__Payment_Method__c', oldTrans.Payment_Method__c);
				//newTrans.put('Payment_Number__c', oldTrans.Payment_Number__c);
				newTrans.put('NSCart__Reason_Code__c', oldTrans.Reason_Code__c); 
				newTrans.put('NSCart__Reason_Text__c', oldTrans.Reason_Text__c);
				newTrans.put('NSCart__Recurring__c', oldTrans.Recurring__c);
				newTrans.put('NSCart__Response__c', oldTrans.Response__c);
				newTrans.put('NSCart__Response_Code__c', oldTrans.Response_Code__c);
				newTrans.put('NSCart__Response_Message__c', oldTrans.Response_Message__c);
				newTrans.put('NSCart__Response_Status__c', oldTrans.Response_Status__c);
				newTrans.put('NSCart__Card_Code_Response__c', oldTrans.Card_Code_Response__c);
				newTrans.put('NSCart__Type__c', oldTrans.Type__c);
				newTransactions.add(newTrans);
			}
			insert newTransactions;
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	*/
}