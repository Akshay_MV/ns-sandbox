public with sharing class Email_Helper {
    
   /* public Id thisId {get;set;}
    
    public class emailWrapper{
        public String lastDeliveryDate {get;set;}
        public String routeDeliveryDay {get;set;}
        public String neverFeedSuggestion {get;set;}
        public Date nextAvailableDeliveryDate {get;set;}
        public List<String> lastOrderProducts {get;set;}
        public List<String> last12MonthProducts {get;set;}
        public List<String> suggested2Products {get;set;}
        public String Name {get;set;} 
        public boolean AutoReorder {get;set;}
        public Date NextOrderDate {get;set;}
    }
    public static emailwrapper queryEmailDetails (Contact cont){
        emailWrapper e = new emailWrapper();
            
            
            Date last12Months = system.today().addDays(-365);
            List<Opportunity> opptyQuery = [Select Id, Contact__c,Contact__r.Next_Order_Date__c, Contact__r.Auto_Reorder__c,Contact__r.Name, Contact__r.Route__r.Next_Delivery_Date__c, CloseDate, Delivery_Date__c, Delivery_Day_Of_Week__c,
                    (Select UnitPrice, TotalPrice, Tax__c, SortOrder, ServiceDate, Quantity, PricebookEntryId, OpportunityId, ListPrice, Ida_Total_Price__c, Ida_Product__c, Ida_Product_Price__c, Ida_Default_Catalog__c, Ida_Custom_Catalog__c, Id, Description, PriceBookEntry.Product2Id, PriceBookEntry.ProductCode, PricebookEntry.Name from OpportunityLineItems )
                 from Opportunity where Delivery_Date__c >= :last12Months and IsWon = true and Contact__c = :cont.Id order by CloseDate desc];
            Set<Id> mostRecentProducts = new Set<Id>();
            List<OpportunityLineItem> nonMostRecentProducts = new List<OpportunityLineItem>();
    
            Opportunity mostRecentOppty = new Opportunity();
            e.lastOrderProducts = new List<String>();
            e.last12MonthProducts = new List<String>();
            e.suggested2Products = new List<String>();
            
            for (Product2 p : [select Id, Name, Ida_Featured__c From Product2 where Ida_Featured__c = 'New Products!']){
                e.suggested2Products.add(p.Name);
            }
            
            for ( Integer i=0;i<opptyQuery.size();i++ ){
                if ( i==0){
                    mostRecentOppty = opptyQuery[i];
                    e.LastDeliveryDate = mostRecentOppty.Delivery_Date__c.format();
                    e.routeDeliveryDay = mostRecentOppty.Delivery_Day_Of_Week__c;
                    e.Name = mostRecentOppty.Contact__r.Name; 
                    e.AutoReorder = mostRecentOppty.Contact__r.Auto_Reorder__c;
                    e.NextOrderDate = mostRecentOppty.Contact__r.Next_Order_Date__c; 
                    if ( mostRecentOppty.Contact__r.Route__r.Next_Delivery_Date__c != null){
                        e.nextAvailableDeliveryDate = mostRecentOppty.Contact__r.Route__r.Next_Delivery_Date__c;    //need to update this
                    }
                    for ( OpportunityLineItem oli : opptyQuery[i].OpportunityLineItems ){
                        if(!oli.PricebookEntry.Name.startsWith('CS ')){
                        e.lastOrderProducts.add(oli.PricebookEntry.Name);
                        mostRecentProducts.add(oli.PriceBookEntry.Product2Id);
                        }
                    }
                } else {
                    for ( OpportunityLineItem oli : opptyQuery[i].OpportunityLineItems ){
                        if ( !mostRecentProducts.contains(oli.PriceBookEntry.Product2Id) && !oli.PricebookEntry.Name.startsWith('CS ')){
                            e.last12MonthProducts.add(oli.PriceBookEntry.Name);
                            mostRecentProducts.add(oli.PriceBookEntry.Product2Id);
                        }
                    }
                }
            }
    
            List<Web_Content__c> webContentQuery = [Select Id, Name, Value__c from Web_Content__c limit 2000];
            for ( Web_Content__c wc : webContentQuery ){
                if ( wc.Name == 'Never Feed Suggestion' ){
                    e.neverFeedSuggestion = wc.Value__c;
                }
            }
            
            return e;
    }
    
    public emailWrapper getEmailDetails(Contact cont){
        
            emailWrapper e = new emailWrapper();
            //if ( thisId == null ){
            //  thisId = apexPages.currentPage().getParameters().get('thisId');
            //}
            thisId = cont.id;
            
            Date last12Months = system.today().addDays(-365);
            List<Opportunity> opptyQuery = [Select Id, Contact__c,Contact__r.Next_Order_Date__c, Contact__r.Auto_Reorder__c,Contact__r.Name, Contact__r.Route__r.Next_Delivery_Date__c, CloseDate, Delivery_Date__c, Delivery_Day_Of_Week__c,
                    (Select UnitPrice, TotalPrice, Tax__c, SortOrder, ServiceDate, Quantity, PricebookEntryId, OpportunityId, ListPrice, Ida_Total_Price__c, Ida_Product__c, Ida_Product_Price__c, Ida_Default_Catalog__c, Ida_Custom_Catalog__c, Id, Description, PriceBookEntry.Product2Id, PriceBookEntry.ProductCode, PricebookEntry.Name from OpportunityLineItems )
                 from Opportunity where Delivery_Date__c >= :last12Months and IsWon = true and Contact__c = :thisId order by CloseDate desc];
            Set<Id> mostRecentProducts = new Set<Id>();
            List<OpportunityLineItem> nonMostRecentProducts = new List<OpportunityLineItem>();
    
            Opportunity mostRecentOppty = new Opportunity();
            e.lastOrderProducts = new List<String>();
            e.last12MonthProducts = new List<String>();
            e.suggested2Products = new List<String>();
            
            for (Product2 p : [select Id, Name, Ida_Featured__c From Product2 where Ida_Featured__c = 'New Products!']){
                e.suggested2Products.add(p.Name);
            }
            
            for ( Integer i=0;i<opptyQuery.size();i++ ){
                if ( i==0){
                    mostRecentOppty = opptyQuery[i];
                    e.LastDeliveryDate = mostRecentOppty.Delivery_Date__c.format();
                    e.routeDeliveryDay = mostRecentOppty.Delivery_Day_Of_Week__c;
                    e.Name = mostRecentOppty.Contact__r.Name; 
                    e.AutoReorder = mostRecentOppty.Contact__r.Auto_Reorder__c;
                    e.NextOrderDate = mostRecentOppty.Contact__r.Next_Order_Date__c; 
                    if ( mostRecentOppty.Contact__r.Route__r.Next_Delivery_Date__c != null){
                        e.nextAvailableDeliveryDate = mostRecentOppty.Contact__r.Route__r.Next_Delivery_Date__c;    //need to update this
                    }
                    for ( OpportunityLineItem oli : opptyQuery[i].OpportunityLineItems ){
                        if(!oli.PricebookEntry.Name.startsWith('CS ')){
                        e.lastOrderProducts.add(oli.PricebookEntry.Name);
                        mostRecentProducts.add(oli.PriceBookEntry.Product2Id);
                        }
                    }
                } else {
                    for ( OpportunityLineItem oli : opptyQuery[i].OpportunityLineItems ){
                        if ( !mostRecentProducts.contains(oli.PriceBookEntry.Product2Id) && !oli.PricebookEntry.Name.startsWith('CS ')){
                            e.last12MonthProducts.add(oli.PriceBookEntry.Name);
                            mostRecentProducts.add(oli.PriceBookEntry.Product2Id);
                        }
                    }
                }
            }
    
            List<Web_Content__c> webContentQuery = [Select Id, Name, Value__c from Web_Content__c limit 2000];
            for ( Web_Content__c wc : webContentQuery ){
                if ( wc.Name == 'Never Feed Suggestion' ){
                    e.neverFeedSuggestion = wc.Value__c;
                }
            }
            
            return e;
    }
    
    @isTest (seealldata=true)
	static void email_helper_test () {
		string contactid = [select id, contact__c from Opportunity where HasOpportunityLineItem = true limit 1].contact__c;
		contact c = [Select c.Use_Card__c, c.Total_Consumption_Rate__c, c.Title, c.SystemModstamp, c.Second_Email__c, c.Salutation, c.Route__c, c.Route_ID__c, c.ReportsToId, c.Repeat__c, c.Reorder_Rate__c, c.Reminder_Type__c, c.Refferal__c, c.Referring_Affiliate_Biz_Name__c, c.Referral_Name__c, c.Referral_Credited__c, c.ROP_Trigger_Field__c, c.ROP_Trigger_Field_5day__c, c.Primary__c, c.Preferred_Payment_Method__c, c.Phone, c.Pets_Name__c, c.Pet_Type__c, c.Pet4_Type__c, c.Pet4_Name__c, c.Pet3_Type__c, c.Pet3_Name__c, c.Pet2_Type__c, c.Pet2_Name__c, c.Password__c, c.OwnerId, c.OtherStreet, c.OtherState, c.OtherPostalCode, c.OtherPhone, c.OtherCountry, c.OtherCity, c.Old_Customer_ID__c, c.Next_Order_Date__c, c.Newsletter__c, c.Name_of_Biz_Partner__c, c.Name, c.MobilePhone, c.Method__c, c.MasterRecordId, c.MailingStreet, c.MailingState, c.MailingPostalCode, c.MailingCountry, c.MailingCity, c.LeadSource, c.Last_Order_Date__c, c.LastName, c.LastModifiedDate, c.LastModifiedById, c.LastCUUpdateDate, c.LastCURequestDate, c.LastActivityDate, c.KMV_d__c, c.JigsawContactId, c.Jigsaw, c.Issue_Other__c, c.Issue_2__c, c.Issue_2_4__c, c.Issue_2_3__c, c.Issue_2_2__c, c.Issue_1__c, c.Issue_1_4__c, c.Issue_1_3__c, c.Issue_1_2__c, c.IsDeleted, c.Internal_Payment_Notes__c, c.Internal_Customer_Notes__c, c.Id, c.HomePhone, c.Heard_Method__c, c.HasOptedOutOfEmail, c.Gender__c, c.Freq_date__c, c.Freq__c, c.First_Order_Date__c, c.FirstName, c.Fax, c.Email_2__c, c.EmailBouncedReason, c.EmailBouncedDate, c.Email, c.Duplicate_Group__c, c.Duplicate_Checker__c, c.Drop_Locations__c, c.Driver_Delivery_Instructions__c, c.DoNotCall, c.Description, c.Department, c.Days_till_Next_Food__c, c.CreatedDate, c.CreatedById, c.Consumption_Day__c, c.Consumption_Day4__c, c.Consumption_Day3__c, c.Consumption_Day2__c, c.Competitive_Food_Recipe__c, c.Competitive_Food_Brand__c, c.Card_2_Type__c, c.Card_2_Security_Code__c, c.Card_2_Number__c, c.Card_2_Name__c, c.Card_2_Expiration_Year__c, c.Card_2_Expiration_Month__c, c.Card_1_Type__c, c.Card_1_Security_Code__c, c.Card_1_Number__c, c.Card_1_Name__c, c.Card_1_Expiration_Year__c, c.Card_1_Expiration_Month__c, c.Call_Requested__c, c.C_Status__c, c.Breed__c, c.Breed4__c, c.Breed3__c, c.Breed2__c, c.Birthdate, c.Billing_Zip__c, c.Billing_Street__c, c.Billing_State__c, c.Billing_City__c, c.Bank_Routing_Number__c, c.Bank_Name__c, c.Bank_Account_Type__c, c.Bank_Account_Number__c, c.Bank_Account_Name__c, c.Auto_Reorder__c, c.AssistantPhone, c.AssistantName, c.Allow_Cron__c, c.Addtl_Last_Name__c, c.Addtl_First_Name__c, c.Active__c, c.AccountId From Contact c where id = :contactid];
		
		queryemaildetails(c);
	}*/ 
}