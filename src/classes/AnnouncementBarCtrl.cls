public class AnnouncementBarCtrl {
    public Website_Filter_Data__c ab {get;set;}

    public AnnouncementBarCtrl(){        
        ab = new Website_Filter_Data__c();
        ab = [ SELECT Id, Text_Data__c, Background_Color__c, Font_Color__c, Height__c, Width__c, Font_Size__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Announcement Bar' AND Website_Filter__r.is_Active__c = true LIMIT 1];
        
    }
}