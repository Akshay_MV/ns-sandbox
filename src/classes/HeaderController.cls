public class HeaderController {
public boolean isLogin{get;set;}
    public HeaderController(){
        String data = '';
        Cookie counter = ApexPages.currentPage().getCookies().get('valueLogin');
        System.debug('counter'+counter);
        if(counter != null){
            data = apexpages.currentPage().getCookies().get('valueLogin').getValue();
            isLogin = true;
        }else{
            isLogin = false;
        }
        // String LoginId = data;
    }
    public Pagereference doLogout(){
        Pagereference pr = new Pagereference('/NaturesSelect/LoginPage');
        Cookie cook = new Cookie('valueLogin', 'true', null, 0, false);
        pr.setCookies(new Cookie[] {cook});
        // authenticated = false;
        pr.setRedirect(true);
        return pr;
    }
}