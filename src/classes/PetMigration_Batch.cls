global class PetMigration_Batch  {
	/*
	String query='select '+ NSDataMigrationTool.queryAllFields('Pet__c')+' from Pet__c';
	global Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
	global PetMigration_Batch() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		if(gd.containsKey('NSCart__Pet__c')){
			list<sObject> newPets=new list<sObject>();
			for(sObject sOb:Scope){
	   			Pet__c petOld = (Pet__c)sOb;
				Schema.SObjectType petType = gd.get('NSCart__Pet__c');
				sObject pet=petType.newsObject();
				pet.put('name',petOld.name);
				pet.put('NSCart__Active_Pet__c',petOld.Active_Pet__c);
				pet.put('NSCart__Breed__c',petOld.Breed__c);
				pet.put('NSCart__Cat_Concerns_Issues_1__c',petOld.Cat_Concerns_Issues_1__c);
				pet.put('NSCart__Cat_Concerns_Issues_2__c',petOld.Cat_Concerns_Issues_2__c);
				pet.put('NSCart__Consumption_Day_by_Cups__c',petOld.Consumption_Day_by_Cups__c);
				pet.put('NSCart__Contact__c',petOld.Contact__c);
				pet.put('NSCart__Date_of_Birth__c',petOld.Date_of_Birth__c);
				pet.put('NSCart__Deceased__c',petOld.Deceased__c);
				pet.put('NSCart__Deceased_Notification__c',petOld.Deceased_Notification__c);
				pet.put('NSCart__DOB__c',petOld.DOB__c);
				pet.put('NSCart__Dog_Issues_1__c',petOld.Dog_Issues_1__c);
				pet.put('NSCart__Dog_Issues_2__c',petOld.Dog_Issues_2__c);
				pet.put('NSCart__Dog_Issues_3__c',petOld.Dog_Issues_3__c);
				pet.put('NSCart__Issue_1__c',petOld.Issue_1__c);
				pet.put('NSCart__Issue_2__c',petOld.Issue_2__c);
				pet.put('NSCart__Mixed_Breed_or_Pure_Bred__c',petOld.Mixed_Breed_or_Pure_Bred__c);
				pet.put('NSCart__New_Cat_Breed__c',petOld.New_Cat_Breed__c);
				pet.put('NSCart__New_Dog_Breed__c',petOld.New_Dog_Breed__c);
				pet.put('NSCart__New_Dog_Breed__c',petOld.New_Dog_Breed__c);
				pet.put('NSCart__Old_Pet_Id__c',petOld.Old_Pet_Id__c);
				//pet.put('NSCart__Special_Note__c',petOld.Special_Note__c);
				pet.put('NSCart__Type__c',petOld.Type__c);
				pet.put('NSCart__Weight_Lbs__c',petOld.Weight__c);
				newPets.add(pet);
			}		
			insert newPets;
		}
	}
	*/
	global void finish(Database.BatchableContext BC) {
		
	}
	
}