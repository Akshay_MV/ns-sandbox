public without sharing class TestingController {
    
    //INTIALIZE VARIABLES
    public Website_Filter_Data__c ProfilePageTextImg {set;get;}
    // public Website_Filter_Data__c ProfilePageImage {set;get;}
    public List<String> TabsCount {get;set;}
    public Integer hey {get;set;}
    public String eh {get;set;}
    public String petName {get;set;}
    public List<Site_User__c> UserList{get;set;}
    public UserWrapper UserWrapperdata{get;set;}
    public List<PetWrapper> PetWrapperList {get;set;}       
    public String UserName {get;set;}        
    public Integer PetSize {get;set;}
    public List<Product2> OldProduct {get;set;}
    public List<String> dateString {get;set;}

   
    //CONSTRUCTOR OF THIS CLASS
    public TestingController(){
        ProfileMainContent();
        List<string> CC = new List<String>{'Pet1'};
        TabsCount = CC;
        hey = TabsCount.size()+1;
        getLoginDetails();
        getOldProduct();    
    }    

   
    // METHOD OF FETCHING TEXT AND IMAGES FROM WEBSITE FILTER DATA
    public void ProfileMainContent(){
        ProfilePageTextImg = new Website_Filter_Data__c();
        ProfilePageTextImg = [SELECT Id,Text_Data__c,Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Profile Page Text and Image' AND Website_Filter__r.is_active__c = true];

        // ProfilePageImage = new Website_Filter_Data__c();
        // ProfilePageImage = [SELECT Id, Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Profile Page Image' AND Website_Filter__r.is_active__c = true];
    }

    
    public void getOldProduct(){
        OldProduct = new List<Product2>();
        OldProduct = [SELECT Id,Name,NSCart__Full_size_image__c,NSCart__Product_Sort__c ,Description,ProductCode,(SELECT Id, UnitPrice FROM PricebookEntries) FROM Product2 WHERE NSCart__Full_size_image__c != null ORDER BY CreatedDate DESC LIMIT 4];            

        dateString = new List<String>{'13/07/2001','13/07/2002','13/07/2003','13/07/2004'};
    }
    

    public void getLoginDetails(){
        String data = '';
        Cookie counter = ApexPages.currentPage().getCookies().get('valueLogin');
        System.debug('counter'+counter);
        if(counter != null){
            data = apexpages.currentPage().getCookies().get('valueLogin').getValue();
        }
        String LoginId = data;
        System.debug(LoginId);
        // UserListSize = 0;
         UserList = new List<Site_User__c>();
         UserList = [SELECT Id, UserName__c,Contact__r.FirstName,Contact__r.LastName,Contact__r.Phone, Contact__r.uid__c,Contact__r.CreatedDate, Password__c FROM Site_User__c WHERE Id=: LoginId LIMIT 1];
        // UserListSize =UserList.size();
        Contact con = [SELECT Id,Name,FirstName,LastName,Email,Phone,Billing_City__c,Billing_State__c,Billing_Zip__c,Billing_Street__c FROM Contact WHERE Id =: UserList[0].Contact__c ];

        List<NSCart__Credit_Card__c> creditcard = new List<NSCart__Credit_Card__c>();
        creditcard = [SELECT Id,NSCart__Card_Number__c,NSCart__Name_on_Card__c,NSCart__Expiration_Month__c,NSCart__Expiration_Year__c,NSCart__CVV__c,NSCart__Contact__c FROM NSCart__Credit_Card__c WHERE NSCart__Contact__c =: con.Id];

        List<Pet__c> petList = new List<Pet__c>();
        petList = [SELECT Id,Name,Type__c,Breed__c,Date_of_Birth__c,Active_Pet__c,Weight__c,Issue_1__c,Contact__c,Age__c,Your_Pet_Favorites__c,Part_Of_Family__c,Nickname__c FROM Pet__c WHERE Contact__c =: con.Id];
        PetSize = petList.size();
        
        
        


        System.debug('Credit-----'+creditcard);
        System.debug('Pet-------'+petList);
                
        UserWrapperdata = new UserWrapper();
        UserWrapperdata.ContactId = con.Id;
        UserWrapperdata.Username = con.FirstName;
        UserWrapperdata.Phone = con.Phone;
        UserWrapperdata.Email = con.Email;
        UserWrapperdata.AdditionalEmail ='';
        UserWrapperdata.Address = con.Billing_Street__c;
        UserWrapperdata.PostalCode= con.Billing_Zip__c;
        UserWrapperdata.State = con.Billing_State__c;
        UserWrapperdata.City = con.Billing_City__c;
        UserWrapperdata.userImage = getImage(con.Id);
        
        if(creditcard.size() > 0){
            UserWrapperdata.CardNumber = creditcard[0].NSCart__Card_Number__c;
            UserWrapperdata.CardHolderName = creditcard[0].NSCart__Name_on_Card__c;
            UserWrapperdata.ExpiryMonth = creditcard[0].NSCart__Expiration_Month__c;
            UserWrapperdata.ExpiryYear = creditcard[0].NSCart__Expiration_Year__c;
            UserWrapperdata.CVV = creditcard[0].NSCart__CVV__c;
            UserWrapperdata.CreditId = creditcard[0].Id;
        }else{
            UserWrapperdata.CardNumber = '';
            UserWrapperdata.CardHolderName ='';
            UserWrapperdata.ExpiryMonth = '';
            UserWrapperdata.ExpiryYear = '';
            UserWrapperdata.CVV = '';
            UserWrapperdata.CreditId = null;
        }
        
        PetWrapperList =  new List<PetWrapper>();
        for(Pet__c peet : petList){
            PetWrapper pt = new PetWrapper();
            pt.PetId = peet.Id;
            pt.PetUserName = peet.Name;
            pt.PetNickname = peet.Nickname__c;
            pt.PetType = peet.Type__c;
            pt.PetBreed = peet.Breed__c;
            pt.PetBirthday = peet.Date_of_Birth__c;
            pt.PetActivityLevel = peet.Active_Pet__c;
            pt.PetFavourites = peet.Your_Pet_Favorites__c;
            pt.PetFamily = peet.Part_Of_Family__c;
            pt.PetWeight = peet.Weight__c;
            pt.PetAge = peet.Age__c;
            pt.PetIssue = peet.Issue_1__c;
            PetWrapperList.add(pt);
        }
        if(petList.size() == 0){
            PetWrapper pt = new PetWrapper();
            pt.PetId = null;
            pt.PetUserName = '';
            pt.PetNickname = '';
            pt.PetType = '';
            pt.PetBreed = '';
            pt.PetBirthday = System.today();
            pt.PetActivityLevel = '';
            pt.PetFavourites = '';
            pt.PetFamily = '';
            pt.PetWeight = '';
            pt.PetAge = 1;
            pt.PetIssue = '';
            PetWrapperList.add(pt);
        }
        UserWrapperdata.PetWrapperList =PetWrapperList;
        // TabsCount.add('Pet'+petList.size());
        // System.debug(TabsCount.size());
        // hey = TabsCount.size()+1;
    } 

    
    
    public void add(){
        // eh = 'Pet'+hey;
        // TabsCount.add(eh);
        // hey = hey + 1;
        // PetWrapperList = new List<PetWrapper>();
        Integer s = UserWrapperdata.PetWrapperList.size();
        String sname = 'Pet '+s;

        PetWrapper pt = new PetWrapper();
        pt.PetUserName = sname;
        pt.PetNickname = '';
        pt.PetType = '';
        pt.PetBreed = '';
        pt.PetBirthday = System.today();
        pt.PetActivityLevel = '';
        pt.PetFavourites = '';
        pt.PetFamily = '';
        pt.PetWeight = '1';
        pt.PetAge = 1;
        pt.PetIssue = '';
        // pt.petImage = new Attachment();
        PetWrapperList.add(pt);
        UserWrapperdata.PetWrapperList = PetWrapperList;
    }


    // METHOD TO SAVE DATA THAT USER CHANGES FROM FRONTEND
    public PageReference SaveUserPetData(){ 
    //   UserWrapper up = UserWrapperdata;
      System.debug('UserWrapperdata'+UserWrapperdata);
      
      Contact con = new Contact();
      
      if(UserWrapperdata.ContactId != null)con.Id  = UserWrapperdata.ContactId;
      con.LastName              = UserWrapperdata.Username;
      con.Email                 = UserWrapperdata.Email; 
      con.Phone                 = UserWrapperdata.phone;
      con.Billing_City__c       = UserWrapperdata.city;
      con.Billing_State__c      = UserWrapperdata.State;
      con.Billing_Zip__c        = UserWrapperdata.PostalCode;
      con.Billing_Street__c     = UserWrapperdata.Address;

      upsert con;
      
      NSCart__Credit_Card__c credit         = new NSCart__Credit_Card__c();

      if(UserWrapperdata.CreditId != null)credit.Id = UserWrapperdata.CreditId;
      credit.NSCart__Card_Number__c         = UserWrapperdata.CardNumber;
      credit.NSCart__Name_on_Card__c        = UserWrapperdata.CardHolderName;
      credit.NSCart__Expiration_Month__c    = UserWrapperdata.ExpiryMonth;
      credit.NSCart__Expiration_Year__c     = UserWrapperdata.ExpiryYear;
      credit.NSCart__CVV__c                 = UserWrapperdata.CVV;
      credit.NSCart__Contact__c             = con.Id;

      upsert credit;
      
      List<Attachment> attachList = new List<Attachment>();
      List<Pet__c> PetList = new List<Pet__c>();

      for(PetWrapper ptwrap : UserWrapperdata.PetWrapperList){
          Pet__c pt = new Pet__c();
          if(ptwrap.PetId != null) pt.Id = ptwrap.PetId;
          pt.Name = ptwrap.PetUserName;
          pt.Nickname__c = ptwrap.PetNickname;
          pt.Type__c = ptwrap.PetType;
          pt.Breed__c = ptwrap.PetBreed;
          pt.Date_of_Birth__c = ptwrap.PetBirthday;
          pt.Active_Pet__c = ptwrap.PetActivityLevel;
          pt.Weight__c = ptwrap.PetWeight;
          pt.Issue_1__c = ptwrap.PetIssue;
          pt.Age__c = ptwrap.PetAge;
          pt.Your_Pet_Favorites__c = ptwrap.PetFavourites;
          pt.Part_Of_Family__c = ptwrap.PetFamily;
          if(ptwrap.PetId == null)pt.Contact__c = con.Id;
          
        //   Attachment atc = new Attachment();
        //   atc.Name = ptwrap.petImage.Name;
        //   atc.body = ptwrap.petImage.body;
        //   atc.ParentId = ptwrap.PetId;
        //   attachList.add(atc);
          PetList.add(pt);
      }
      upsert PetList;
      insert attachList;
      return null;
    }

    // METHOD TO LOG OUT FROM YOUR ACCOUNT
    public Pagereference doLogout(){
        Pagereference pr = new Pagereference('/NaturesSelect/LoginPage');
        Cookie cook = new Cookie('valueLogin', 'true', null, 0, false);
        pr.setCookies(new Cookie[] {cook});
        // authenticated = false;
        pr.setRedirect(true);
        return pr;
    }
    
    // Login Validation
    public Pagereference loginValidation(){
        String data = '';
        Cookie counter = ApexPages.currentPage().getCookies().get('valueLogin');
        System.debug('counter'+counter);
        if(counter != null){
            data = apexpages.currentPage().getCookies().get('valueLogin').getValue();
        }
        
        if(data == null){
            Pagereference pr = new Pagereference('/NaturesSelect/LoginPage');
            pr.setRedirect(true);
            return pr;
        }
        return null;
    }

    public class UserWrapper{
         public Id ContactId{get;set;}
         public String Username{get;set;}
         public String Phone {get;set;}
         public String Email {get;set;}
         public String AdditionalEmail{get;set;}
         public String Address{get;set;}
         public String PostalCode{get;set;}
         public String State{get;set;}
         public String City{get;set;}
         public String CardNumber{get;set;}
         public String CardHolderName{get;set;}
         public String ExpiryMonth{get;set;}
         public String ExpiryYear{get;set;}
         public String CVV{get;set;}
         public Id CreditId{get;set;}
         public List<PetWrapper> PetWrapperList {get;set;}
         public String userImage {get;set;}
         
    }
    public class PetWrapper{
        public Id PetId{get;set;}
        public String PetUserName {get;set;}
        public String PetNickname {get;set;}
        public String PetType {get;set;}
        public String PetBreed {get;set;}
        public Date PetBirthday {get;set;}
        public String PetActivityLevel {get;set;}
        public String PetFavourites {get;set;}
        public String PetFamily {get;set;}
        public String PetWeight {get;set;}
        public Decimal PetAge {get;set;}
        public String PetIssue {get;set;}
        // public Attachment petImage{get;set;}
    }

    // To insert the image Of user
    @RemoteAction
    public static void doUploadAttachment(String Id, String attachmentName, String attachmentBody){
        System.debug('apex method::');
        System.debug(attachmentBody);
        System.debug(attachmentName);
        System.debug('Listing Id :: '+Id);
        Attachment att = new Attachment();
        att.parentId = Id;
        att.name = attachmentName;
        att.body = EncodingUtil.base64Decode(attachmentBody);
        insert att;
    }

    public string getImage(string reid){
        List<Attachment> attach = new List<Attachment>();
        string pdfvalue;
        attach = [SELECT Id,Name,Body,ContentType FROM Attachment WHERE ParentId =: reid ORDER BY CreatedDate DESC LIMIT 1];
        String s = '';            
        
        if(attach.size() > 0){
            String contype = attach[0].ContentType;
            Blob attachbody = attach[0].Body;
            pdfvalue= '';
            pdfvalue= EncodingUtil.base64Encode(attachbody);            
            s = 'data:' + contype + ';base64,' + pdfvalue;    
        }else{
            s = 'https://mvclouds-nschicago.cs18.force.com/resource/1610110501000/MissingImage';
        }
        // s = '{!$Resource.Missingimage}';
        return s;            
    }
}