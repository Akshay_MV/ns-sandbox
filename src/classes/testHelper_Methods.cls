public class testHelper_Methods {
/*
	public static Account account_insertTest(){
		Account testRecord = new Account();
		testRecord.name = string.valueOf(system.now());
		insert testRecord;
		return testRecord;
	}

	public static Contact contact_insertTest(Account testAccount){
		Contact testRecord = new Contact();
		testRecord.AccountId = testAccount.Id;
		testRecord.FirstName = 'Test';
		testRecord.LastName = string.valueOF(system.now());
		testRecord.Use_Card__c = 
		testRecord.Bank_Account_Name__c = 'Test Account';
		testRecord.Bank_Account_Number__c = '0000000000';
		testRecord.Bank_Account_Type__c = 'Checking';
		testRecord.Bank_Name__c = 'Test Bank';
		testRecord.Bank_Routing_Number__c = '000000000';
		testRecord.Card_1_Expiration_Month__c = '01';
		testRecord.Card_1_Expiration_Year__c = '2022';
		testRecord.Card_1_Name__c = 'Test Name';
		testRecord.Card_1_Number__c = '0000000000000000';
		testRecord.Card_1_Security_Code__c = '000';
		testRecord.Card_1_Type__c = 'Visa';
		testRecord.Card_2_Expiration_Month__c = '01';
		testRecord.Card_2_Expiration_Year__c = '2022';
		testRecord.Card_2_Name__c = 'Test Name';
		testRecord.Card_2_Number__c = '0000000000000000';
		testRecord.Card_2_Security_Code__c = '000';
		testRecord.Card_2_Type__c = 'Visa';
		testRecord.MailingCity = 'Test City';
		testRecord.MailingCountry = 'USA';
		testRecord.MailingPostalCode = '90000';
		testRecord.MailingState = 'ST';
		testRecord.MailingStreet = '123 My Street';
		testRecord.OtherCity = 'Test City';
		testRecord.OtherCountry = 'USA';
		testRecord.OtherPostalCode = '90000';
		testRecord.OtherState = 'ST';
		testRecord.OtherStreet = '123 My Street';

		insert testRecord;
		return testRecord;		
	}


	public static Ida_Coupon__c	idaCoupon_insertTest(Product2 testProduct){
		Ida_Coupon__c testRecord = new Ida_Coupon__c();
		testRecord.Name = 'test';
		testRecord.Ida_Coupon_Amount__c = 1;
		testRecord.Ida_Product__c = testProduct.Id;
		testRecord.Ida_Activation_Date__c = system.today().addDays(-365);
		testRecord.Ida_Expiration_Date__c = system.today().addDays(365);
		insert testRecord;
		return testRecord;
	}

	public static Ida_Tax__c idaTax_insertTest(){
		Ida_Tax__c testRecord = new Ida_Tax__c();
		testRecord.Ida_Tax_Percentage__c = 0.2;
		testRecord.Zip__c = '90000,99999';
		testRecord.Ida_Tax_State__c = 'CA';
		insert testRecord;
		return testRecord;
	}

	public static Opportunity opportunity_insertTest(Account testAccount, Contact testContact){
		List<Opportunity> opptys = [Select id, AccountId, AMount, CLoseDate, Contact__c, Delivery_Date__c, Discount__c, Ida_Applied_Coupon__c, Name, StageName from Opportunity limit 1];
		if ( opptys.size() > 0 ){
			return opptys[0];
		} else {
			Opportunity testRecord = new Opportunity();
			testRecord.AccountId = testAccount.Id;
			testRecord.Amount = 0;
			testRecord.CloseDate = system.today();
			testRecord.Contact__c = testContact.Id;
			testRecord.Delivery_Date__c = system.today();
			testRecord.Discount__c = '1%';
			testRecord.Ida_Applied_Coupon__c = 'test';
			testRecord.Name = string.valueOf(system.now());
			testRecord.StageName = 'Prospecting';
			insert testRecord;
			return testRecord;
		}		
	}

	public static OpportunityLineItem opportunityLineItem_InsertTest(Opportunity testOpportunity, PricebookEntry testPricebookEntry){
		OpportunityLineItem testRecord = new OpportunityLineItem();
		testRecord.OpportunityId = testOpportunity.Id;
		if ( testPricebookEntry != null && testPricebookEntry.Id != null ){
			testRecord.PricebookEntryId = testPricebookEntry.Id;
		}
		testRecord.Description = 'Test Line Item';
		testRecord.Quantity = 1;
		testRecord.UnitPrice = 1;
		return testRecord;		
	}

	public static Pricebook2 pricebook_InsertTest(){
		Pricebook2 testRecord = new Pricebook2();
		testRecord.Name = 'test';
		testRecord.IsActive = true;
		insert testRecord;
		return testRecord;
	}

	public static PricebookEntry pricebookEntry_InsertTest(Pricebook2 testPricebook, Product2 testProduct){
		List<Pricebook2> standardPB = [Select Id from Pricebook2 where IsStandard = true limit 1];
		
		PricebookEntry standardEntryRecord = new PricebookEntry();
		standardEntryRecord.Pricebook2Id = standardPB[0].Id;
		standardEntryRecord.IsActive = true;
		standardEntryRecord.Product2Id = testProduct.Id;
		standardEntryRecord.UnitPrice = 1;
		standardEntryRecord.UseStandardPrice = false;
		insert standardEntryRecord;

		PricebookEntry testRecord = new PricebookEntry();
		testRecord.Pricebook2Id = testPricebook.Id;
		testRecord.IsActive = true;
		testRecord.Product2Id = testProduct.Id;
		testRecord.UnitPrice = 1;
		testRecord.UseStandardPrice = false;
		insert testRecord;
		return testRecord;
	}

	public static Product2 product_InsertTest(){
		Product2 testRecord = new Product2();
		testRecord.Name = 'Test Product';
		testRecord.IsActive = true;
		testRecord.Family = 'Test';
		insert testRecord;
		return testRecord;
	}

	public static Route__c route_InsertTest(){
		Route__c testRecord = new Route__c();
		testRecord.Zip__c = '90000,00000';
		testRecord.Area__c = 'Test';
		testRecord.Day__c = 'Monday';
		testRecord.Time__c = 'AM';
		insert testRecord;
		return testRecord;
	}
*/
}