/*
Transfer  IDA Catalog Custom  to NSCart_Product_Tab__c
dynamic sobject to allow compile without package dependency.

*/

global class Product_TabMigration_Batch  {
/*	
	//String query='select '+ NSDataMigrationTool.queryAllFields('Product2')+' from Product2';
	String query='select id,name, NSCart__Product_Tab_Set__c from Product2';
	global Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
	global Product_TabMigration_Batch() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		//For every product. Find all ida_Catalog_Custom__c. related to it
   		//create a product Tab Grouping, Then create product tab items for each Ida catalog for this oppty.
   		//link the product tab grouping to this product.
   		//batch size 1.
   		if(gd.containsKey('NSCart__Product_Tab__c')){
   			Schema.SObjectType productTabType = gd.get('NSCart__Product_Tab__c');
   			map<String,recordType> recordTypemap=new map<String,recordType>();
   			map<id,list<ida_Catalog_custom__c>> productToIdaCatalogMap=new map<id,list<ida_catalog_custom__c>>();

   			for(RecordType rts:[select id ,name from recordType where sobjectType='NSCart__Product_Tab__c'])
   				recordTypeMap.put(rts.name,rts);
   			
   			set<id> productIds=new set<id>();
   			for(sObject rt:scope){
   				Product2 prod=(Product2)rt;
   				productIds.add(prod.id);
   			}

   			for(ida_Catalog_custom__c idacatalog:[select id, ida_Text_rich__c,ida_title__c, product__c,Ida_catalog_Custom__c from Ida_Catalog_Custom__c
   			  where (Ida_catalog_custom__c='Tab 1' or Ida_catalog_custom__c='Tab 2' or Ida_catalog_custom__c='Tab 3' or Ida_catalog_custom__c='Tab 4') and product__c in:productIds])
			{
				if(productToIdaCatalogMap.containskey(idacatalog.product__c))
					productToIdaCatalogMap.get(idacatalog.product__c).add(idacatalog);			
				else
					productToIdaCatalogmap.put(idacatalog.product__c,new list<ida_catalog_custom__c>{idacatalog});			
			}
			
			list<sObject> productTabParentInserts=new list<sObject>();
			list<TabGroupWrapper> tabGroupWrappers=new list<tabGroupWrapper>();
   			for(sObject rt:scope){
   				Product2 prod=(Product2)rt;
				if(productToIdaCatalogMap.containskey(prod.id)){
   					sObject newproductTabParent=productTabType.newSObject();
   					newproductTabParent.put('RecordTypeId',recordTypeMap.get('Tab Group').id);
   					//trim name
   					String productName=prod.name;
   					if(productName.length()>80){
   						productName=productName.subString(0,80);
   					}
   					newproductTabParent.put('name',productName);
   					productTabParentInserts.add(newProductTabParent);
   					TabGroupWrapper tgwrapper=new tabGroupWrapper();
   					tgwrapper.productId=prod.id;
   					tgWrapper.tabGroup=newproductTabParent;
					for(ida_Catalog_Custom__c idaCat:productToIdaCatalogMap.get(prod.id)){
						sObject newproductTab=productTabType.newSObject();
						newProductTab.put('Name',idaCat.Ida_Title__c);
						//newProductTab.put('NSCart__Product_tab_set__c',);
						newProductTab.put('NSCart__Tab_Content__c',idaCat.ida_text_rich__c);
						//substring of Ida_catalog_custom__c to get order should be in Tab # format
						String tabnoStr=idaCat.Ida_catalog_custom__c.subString(4,5);
						integer tabno=integer.valueof(tabnoStr);
						newProductTab.put('NSCart__Order__c',tabNo);
						newProductTab.put('RecordTypeId',recordTypeMap.get('Product Tab').id);
						tgWrapper.productTabs.add(newProductTab);

					}
					tabGroupWrappers.add(tgWrapper);
				}			
			}
			insert productTabParentInserts;
			list<sObject> productTabInserts= new list<sObject>();
			list<Product2> productUpdates=new list<Product2>();
			//have parent ids now
			for(tabGroupWrapper tgWrapper:tabGroupWrappers){
				for(sObject productTab: tgWrapper.productTabs){
					//set parent id
					productTab.put('NSCart__Product_tab_set__c',tgWrapper.tabGroup.id);
					productTabInserts.add(productTab);
				}
				product2 prodUpdate=new product2(id=tgWrapper.productId);
				prodUpdate.put('NSCart__Product_Tab_Set__c',tgWrapper.tabGroup.id);
				productUpdates.add(prodUpdate);
			}
			insert productTabInserts;
			update productUpdates;	
		}	
	}

	global class TabGroupWrapper{
		public id productId{get;set;}
		public sObject tabGroup{get;set;}
		public list<sObject> productTabs{get;set;}
		public TabGroupWrapper(){
			this.productTabs=new list<sObject>();
		}
	}
	*/
	global void finish(Database.BatchableContext BC) {
		
	}
	
}