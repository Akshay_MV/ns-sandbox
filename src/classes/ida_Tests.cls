@isTest(SeeAllData=true) 
private class ida_Tests {
/*
	static Pricebook2 testPricebook = testHelper_Methods.pricebook_InsertTest();
	static Product2 testProduct = testHelper_Methods.product_InsertTest();
	static PricebookEntry testPricebookEntry = testHelper_Methods.pricebookEntry_InsertTest(testPricebook, testProduct);
	static Ida_Coupon__c testCoupon = testHelper_Methods.idaCoupon_insertTest(testProduct);
	static Ida_Tax__c testTax = testHelper_Methods.idaTax_insertTest();
	
    static testMethod void ida_UnitTest() {
		Ida_Shipping_Master__c testShippingMaster = new Ida_Shipping_Master__c();
		testShippingMaster.Ida_Shipping_Policy_Description__c = 'test';
		insert testShippingMaster;

		Ida_Shipping_Policy__c testShippingPolicy = new Ida_Shipping_Policy__c();
		testShippingPolicy.Ida_Shipping_Country__c = 'US';
		testShippingPolicy.Ida_Shipping_State__c = 'CA';
		testShippingPolicy.Ida_Shipping_Master__c = testShippingMaster.Id;
		testShippingPolicy.Ida_Flat_Fee__c = 1;
		testShippingPolicy.Ida_Shipping_Unit_Cost1__c = 1;
		testShippingPolicy.Ida_Shipping_Unit_Cost2__c = 1;
		testShippingPolicy.Ida_Shipping_Unit_Cost3__c = 1;
		testShippingPolicy.Ida_Shipping_Unit_Cost4__c = 1;
		insert testShippingPolicy;

		testProduct.Ida_Coupon__c = testCoupon.Id;
		testProduct.Ida_Shipping_Master__c = testShippingMaster.Id;
		update testProduct;
    	
		List<IdaShoppingCartHandler.ShoppingItem> shoppingItems = new List<IdaShoppingCartHandler.ShoppingItem>();
		IdaShoppingCartHandler.ShoppingItem si = new IdaShoppingCartHandler.ShoppingItem(testPricebookEntry.Product2Id, 'Test', testPricebookEntry.Id, null, 1, 1, false);
		shoppingItems.add(si);

		String shippingCountry = 'US';
		String shippingState = 'CA';
		Set<String> shippingMasterIds = new Set<String>();	
		shippingMasterIds.add(testShippingMaster.Id);
		Decimal quantity = 1;
		
		List<Ida_Shipping_Policy__c> shippingDetails = new List<Ida_Shipping_Policy__c>();
		shippingDetails.add(testShippingPolicy);
		String couponApplied = 'Test';
		String couponCode = 'Test';
		
		List<Opportunity> opptyQuery = [Select Id from Opportunity where IsWon = true and HasOpportunityLineItem = true and Contact__c != null limit 1];

    	test.startTest();
			Decimal d = IdaShippingCostCalculator.calculateShippingCost(shoppingItems,shippingCountry,shippingState);
	
			d = IdaCouponCalculator.calculateCoupon(testCoupon.Name, shoppingItems);
			String coup = IdaCouponCalculator.checkCouponCode(couponCode, testCoupon.Name, shoppingItems);

			PageReference p = page.IdaPaymentStep1;
			if ( opptyQuery.size() == 1){
				p.getParameters().put('reorderId',opptyQuery[0].Id);
			}
			test.setCurrentPage(p);
			IdaPaymentController con = new IdaPaymentController();
			con.getDeliveryDateOptions();
			con.getShoppingCart();
			con.getPaymentMethods();

			IdaOrderHandler han = new IdaOrderHandler(opptyQuery[0].Id,false);
			han.loadExistingOrder();
			han.copyShippingInfo();
			han.getCopiedOrder();

			IdaTaxCalculator.calculateTax(100, 'CA', '99999');
			IdaTaxCalculator.calculateTax(100, 'CA');

			//Ida_Chargent_Validate_Controller val = new Ida_Chargent_Validate_Controller();
			//val.getShoppingCartPage();

		test.stopTest();



    }*/
}