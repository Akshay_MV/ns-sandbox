public without sharing class opportunity_Methods {
/*
	public static Boolean hasRunBeforeUpdate = false;
	public static Boolean hasRunAfterUpdate = false;
	public static string test; 
	/*public static void beforeInsert(List<Opportunity> newList){
		copyContactInfo(newlist);
		copyCardInfo(newlist);
		list<Opportunity> noContact = new list<Opportunity>();
		for (Integer i=0;i<newList.size();i++){
			if( newlist[i].Contact__c == null){
				noContact.add(newlist[i]);
			}
		}
		if(noContact.size() > 0){
			attachContact(noContact);
		}
		
	}*/

	/*public static void beforeupdate(List<Opportunity> newList, list<opportunity> oldlist){
		if ( !hasRunBeforeUpdate ){
			list<opportunity> toProcess = new list<opportunity>();
			list<opportunity> cardProcess = new list<opportunity>();
			for (Integer i=0;i<newList.size();i++){
				if (newlist[i].use_information_on_file__c) toProcess.add(newlist[i]);
			}
			copyContactInfo(toProcess);
			copyCardInfo(newlist);
			hasRunBeforeUpdate = true;
		}
	}*/
	
	/*public static void afterInsert(List<Opportunity> newList){
		Map<Id,Date> contactMap = new Map<Id,Date>();
		Map<Id,Date> contactUpdate = new Map<Id,Date>();
		List<Opportunity> processlist = new List<Opportunity>();
		
		for (Integer i=0;i<newList.size();i++){
			if ( newList[i].isWon && newList[i].Contact__c != null && newList[i].Delivery_Date__c != null ){
				if (!( contactMap.containsKey(newList[i].Contact__c) && contactMap.get(newList[i].Contact__c) > newList[i].CloseDate )){
					contactMap.put(newList[i].Contact__c,newList[i].Delivery_Date__c);
				}			
			}
			if( newList[i].Contact__c != null ){
				contactUpdate.put(newList[i].Contact__c,newList[i].CloseDate);
			}
			
			updateLastOrderDate(contactMap);
			FirstOrderDate(contactMap);
		} 
	}*/
	/*
	public static void afterUpdate(List<Opportunity> newList, List<Opportunity> oldList){
		if ( !hasRunAfterUpdate ){
			Map<Id,Date> contactMap = new Map<Id,Date>();
			for (Integer i=0;i<newList.size();i++){
				if ( newList[i].isWon && oldList[i].isWon != true && newList[i].Contact__c != null && newList[i].Delivery_Date__c != null){
					if ( !contactMap.containsKey(newList[i].Contact__c) || !( contactMap.containsKey(newList[i].Contact__c) && contactMap.get(newList[i].Contact__c) > newList[i].CloseDate )){
						contactMap.put(newList[i].Contact__c,newList[i].Delivery_Date__c);
					}			
				}
			}
			
			updateLastOrderDate(contactMap);
			hasRunAfterUpdate = true;
		}
	}*/
/*
	public static void updateLastOrderDate(Map<Id,Date> contactMap){
		List<Contact> contactUpdates = new List<Contact>();
		
		for ( Contact c : [Select Id, Last_Order_Date__c, Next_Order_Date__c, Freq__c from Contact where Id in :contactMap.keySet() ]){
			if ( ((c.Last_Order_Date__c != null && contactMap.get(c.Id) > c.Last_Order_Date__c)) || c.Last_Order_Date__c == null ){
				c.Last_Order_Date__c = contactMap.get(c.Id);
				if ( c.Freq__c == null ){
					c.Freq__c = 5;
				}
				c.Next_Order_Date__c = c.Last_Order_Date__c.addDays((7*c.Freq__c).intValue());
				contactUpdates.add(c);
			}
		}			
		update contactUpdates;
		
		//remove pending tasks regarding notification for reorder || REPLACED BY BATCH
//		list<Task> obsoleteTasks = new list<Task>();
//		for (Task t : [select id from Task where whoid IN :contactMap.KeySet() AND subject = 'ROP F/Up' AND isclosed = false AND isarchived = false AND isdeleted = false AND status!= 'Ordered Food']) {
//			t.status = 'Ordered Food';
//			obsoleteTasks.add(t);
//		}
//		if (obsoleteTasks.size() > 0) {
//			update obsoleteTasks;
//		}
//		
	}
	*//*
	public static void FirstOrderDate(Map<Id,Date> ContactUpdate){
		List<Contact> ContactFirstOrder = new List<Contact>();
		for (Contact c : [Select Id, First_Order_Date__c from Contact where Id in :contactUpdate.keySet() ]){
			if(c.First_Order_Date__c == null){
				c.First_Order_Date__c = contactUpdate.get(c.Id);
				ContactFirstOrder.add(c);
			}
		}
		
		update ContactFirstOrder;
	}*/
	/*
	public static void copyContactInfo(List<Opportunity> newList){
		Set<id> contactIds = new Set<Id>();
		for (Integer i=0;i<newList.size();i++){
			if ( newList[i].Payment_Info_On_File__c == 'Yes' && newList[i].Use_Information_On_File__c && newList[i].isWon && newList[i].Contact__c != null && newList[i].Delivery_Date__c != null ){
				contactIds.add(newList[i].Contact__c);
			}
		}	
		Map<Id,Contact> contactMap = new Map<Id,Contact>([select id,use_card__c, Bank_Account_Name__c, Bank_Account_Number__c,Bank_Account_Type__c,Bank_Name__c,Bank_Routing_Number__c,Card_1_Expiration_Month__c,Card_1_Expiration_Year__c, Card_1_Name__c, Card_1_Number__c, Card_1_Security_Code__c, Card_1_Type__c, Card_2_Expiration_Month__c,Card_2_Expiration_Year__c, Card_2_Name__c, Card_2_Number__c, Card_2_Security_Code__c, Card_2_Type__c from contact where id IN :contactIds]);
		for (Integer i=0;i<newList.size();i++){
			if ( newList[i].Payment_Info_On_File__c == 'Yes' && newList[i].Use_Information_On_File__c && newList[i].isWon && newList[i].Contact__c != null && newList[i].Delivery_Date__c != null && contactMap.containsKey(newList[i].Contact__c)){
				if(newList[i].ChargentSFA__Payment_Method__c == 'E-Check'){
					newList[i].ChargentSFA__Bank_Account_Name__c = checkNullString(contactmap.get(newList[i].contact__c).Bank_Account_Name__c);
					newList[i].ChargentSFA__Bank_Account_Number__c = checkNullString(contactmap.get(newList[i].contact__c).Bank_Account_Number__c);
					newList[i].ChargentSFA__Bank_Account_Type__c = checkNullString(contactmap.get(newList[i].contact__c).Bank_Account_Type__c);
					newList[i].ChargentSFA__Bank_Name__c = checkNullString(contactmap.get(newList[i].contact__c).Bank_Name__c);
					newList[i].ChargentSFA__Bank_Routing_Number__c = checkNullString(contactmap.get(newList[i].contact__c).Bank_Routing_Number__c);
				}
				else if(newList[i].ChargentSFA__Payment_Method__c == 'Credit Card' && contactmap.get(newList[i].contact__c).Use_Card__c == 'Card 1'){	
					newList[i].ChargentSFA__Card_Month__c = checkNullString(contactmap.get(newList[i].contact__c).Card_1_Expiration_Month__c);
					newList[i].ChargentSFA__Card_Year__c = checkNullString(contactmap.get(newList[i].contact__c).Card_1_Expiration_Year__c);
					newList[i].ChargentSFA__Card_Name__c = checkNullString(contactmap.get(newList[i].contact__c).Card_1_Name__c);
					newList[i].ChargentSFA__Card_Number__c = checkNullString(contactmap.get(newList[i].contact__c).Card_1_Number__c);
					newList[i].ChargentSFA__Card_Security__c = checkNullString(contactmap.get(newList[i].contact__c).Card_1_Security_Code__c);
					newList[i].ChargentSFA__Card_Type__c = checkNullString(contactmap.get(newList[i].contact__c).Card_1_Type__c);
				}
				else if(newList[i].ChargentSFA__Payment_Method__c == 'Credit Card' && contactmap.get(newList[i].contact__c).Use_Card__c == 'Card 2'){
					newList[i].ChargentSFA__Card_Month__c = checkNullString(contactmap.get(newList[i].contact__c).Card_2_Expiration_Month__c);
					newList[i].ChargentSFA__Card_Year__c = checkNullString(contactmap.get(newList[i].contact__c).Card_2_Expiration_Year__c);
					newList[i].ChargentSFA__Card_Name__c = checkNullString(contactmap.get(newList[i].contact__c).Card_2_Name__c);
					newList[i].ChargentSFA__Card_Number__c = checkNullString(contactmap.get(newList[i].contact__c).Card_2_Number__c);
					newList[i].ChargentSFA__Card_Security__c = checkNullString(contactmap.get(newList[i].contact__c).Card_2_Security_Code__c);
					newList[i].ChargentSFA__Card_Type__c = checkNullString(contactmap.get(newList[i].contact__c).Card_2_Type__c);
				}
			}
		}
	}
	*//*
	public static void copyCardInfo(List<Opportunity> newList){
		Set<id> contactIds = new Set<Id>();
		for (Integer i=0;i<newList.size();i++){
			if ( ((newList[i].ChargentSFA__Payment_Method__c == 'Credit Card' && newList[i].ChargentSFA__Card_Number__c != null) || (newList[i].ChargentSFA__Payment_Method__c == 'E-Check' && newList[i].ChargentSFA__Bank_Account_Number__c != null) ) && newlist[i].Contact__r.Card_1_Number__c == null && newList[i].isWon && newList[i].Contact__c != null && newList[i].Delivery_Date__c != null ){
				contactIds.add(newList[i].Contact__c);
			}
		}	
		Map<Id,Contact> contactMap = new Map<Id,Contact>([select id,use_card__c, Bank_Account_Name__c, Bank_Account_Number__c,Bank_Account_Type__c,Bank_Name__c,Bank_Routing_Number__c,Card_1_Expiration_Month__c,Card_1_Expiration_Year__c, Card_1_Name__c, Card_1_Number__c, Card_1_Security_Code__c, Card_1_Type__c, Card_2_Expiration_Month__c,Card_2_Expiration_Year__c, Card_2_Name__c, Card_2_Number__c, Card_2_Security_Code__c, Card_2_Type__c from contact where id IN :contactIds]);
		List<Contact> updateContact = new List<Contact>();
		for (Integer i=0;i<newList.size();i++){
			if ( newList[i].isWon && newList[i].Contact__c != null && newList[i].Delivery_Date__c != null && contactMap.containsKey(newList[i].Contact__c)){
				if(newList[i].ChargentSFA__Payment_Method__c == 'E-Check' && contactmap.get(newList[i].contact__c).Bank_Account_Number__c == null){
					contactmap.get(newList[i].contact__c).Bank_Account_Name__c = checkNullString(newList[i].ChargentSFA__Bank_Account_Name__c) ;
					contactmap.get(newList[i].contact__c).Bank_Account_Number__c = checkNullString(newList[i].ChargentSFA__Bank_Account_Number__c)  ;
					contactmap.get(newList[i].contact__c).Bank_Account_Type__c = checkNullString(newList[i].ChargentSFA__Bank_Account_Type__c) ;
					contactmap.get(newList[i].contact__c).Bank_Name__c = checkNullString(newList[i].ChargentSFA__Bank_Name__c)  ;
					contactmap.get(newList[i].contact__c).Bank_Routing_Number__c = checkNullString(newList[i].ChargentSFA__Bank_Routing_Number__c) ;
					updatecontact.add(contactmap.get(newList[i].contact__c));
				}
				else if(newList[i].ChargentSFA__Payment_Method__c == 'Credit Card' && contactmap.get(newList[i].contact__c).Card_1_Number__c == null){	
					contactmap.get(newList[i].contact__c).Card_1_Expiration_Month__c = checkNullString(newList[i].ChargentSFA__Card_Month__c);
					contactmap.get(newList[i].contact__c).Card_1_Expiration_Year__c = checkNullString(newList[i].ChargentSFA__Card_Year__c) ;
					contactmap.get(newList[i].contact__c).Card_1_Name__c = checkNullString(newList[i].ChargentSFA__Card_Name__c) ;
					contactmap.get(newList[i].contact__c).Card_1_Number__c = checkNullString(newList[i].ChargentSFA__Card_Number__c) ;
					contactmap.get(newList[i].contact__c).Card_1_Security_Code__c = checkNullString(newList[i].ChargentSFA__Card_Security__c) ;
					contactmap.get(newList[i].contact__c).Card_1_Type__c = checkNullString(newList[i].ChargentSFA__Card_Type__c) ;
					updatecontact.add(contactmap.get(newList[i].contact__c));
				}
			}
		}
		if(updateContact.size() > 0){
		update updatecontact; 
		}
	}*/
	/*
	public static string checkNullString (String s){
		if (s == null) {s='';return s;}
		else return s;
	}
	/*
	public static void attachContact(list<Opportunity> newlist) {
				
		set<string> contactEmails = new set<string>();
		for (integer i=0; i< newlist.size(); i++) {
			contactEmails.add(newlist[i].Ida_Shipping_Email__c); 
		}
		
		map<string,Contact> matchingContacts = new map<string,Contact>();
		
		for (Contact c : [Select id, email, email_2__c, Accountid from Contact where email IN :contactEmails OR email_2__c IN :contactEmails]){
            matchingContacts.put(c.email,c);
            matchingContacts.put(c.email_2__c,c);
        }	
		
		list<Contact> newContacts = new list<Contact>();
		map<string,Contact> newContactMap = new map<string,Contact>();
		
		for (integer i=0; i<newlist.size(); i++) {
			if (matchingContacts.containskey(newlist[i].Ida_Shipping_Email__c)){ // if the contact exists, attach it to order, otherwise create new contact
				newlist[i].contact__c = matchingContacts.get(newlist[i].Ida_Shipping_Email__c).id;
				/*if(newContactMap.get(newlist[i].Ida_Shipping_Email__c).Accountid != null){
					newlist[i].Accountid =newContactMap.get(newlist[i].Ida_Shipping_Email__c).Accountid;
				}*/
		/*	}
			else { // create new contact from Order info
				List<Account> a = [Select Id, Default_Account__c from Account where Default_Account__c = True limit 1];
				 
				Contact c = new Contact();
				c.lastname = newlist[i].Ida_Shipping_Last_Name__c;
				c.firstname = newlist[i].Ida_Shipping_First_Name__c;
				c.mailingstreet = newlist[i].Ida_Shipping_Address_Line1__c;
				c.mailingcity = newlist[i].Ida_Shipping_City__c;
				c.mailingstate = newlist[i].Ida_Shipping_State__c;
				c.mailingpostalcode = newlist[i].Ida_Shipping_ZipCode__c;
				c.phone = newlist[i].Ida_Shipping_Phone__c;
				c.email = newlist[i].Ida_Shipping_Email__c;	
				
				if(a.size() > 0){
					c.accountid = a[0].id;
				}
				
				newContacts.add(c);	
									
			}
		}
		
		system.debug('contacts to add' + newContacts);
		insert newContacts; // commit new contacts
		
		for(Contact c : newContacts){
			newContactMap.put(c.email,c);
		}	



		for (integer i=0; i<newlist.size();i++) { //attach the new contacts to order
			if (newContactMap.containskey(newlist[i].Ida_Shipping_Email__c)){
				newlist[i].contact__c = newContactMap.get(newlist[i].Ida_Shipping_Email__c).id;
				newlist[i].Accountid =newContactMap.get(newlist[i].Ida_Shipping_Email__c).Accountid;
			}
		}
	}*/
}