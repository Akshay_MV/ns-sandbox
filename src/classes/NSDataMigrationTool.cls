public class NSDataMigrationTool {
	public NSDataMigrationTool() {
		//Routes @done Run batch
			//Database.executeBatch(new routeMigration_Batch());
		//Tax @done Run Batch
		 	//Database.executeBatch(new salestaxmigration_Batch());

		
		//Zip @done Run Script to match routes and taxes into zip records. Run AFTER routes and tax finishes.
			//NSDataMigrationTool.updateRoutesTax();

		//Contact @ done
			//database.executebatch(new ContactMigration_Batch()); 

		//opportunity @done
		 	//database.executebatch(new OpportunityMigration_Batch()); 	

		  //transaction @done
		  	//database.executebatch(new TransactionMigration_Batch());
		  //creditcard  @done  
		  //database.executebatch(new creditcardMigration_Batch(),1);
		 

		//product @done
			//database.executebatch(new ProductMigration_Batch()); 

		//coupon @done
			//coupon user @done
			//database.executebatch(new CouponMigration_Batch()); 	

	} 

	public static void updateRoutesTax(){
		map<id, NSCart__Route__c> routeMap = new map<id, NSCart__Route__c>([select id, NSCart__zip__c from NSCart__Route__c]);
		map<id, NSCart__Sales_Tax__c> taxMap = new map<id, NSCart__Sales_Tax__c>([select id, NSCart__zip__c from NSCart__Sales_tax__c where NSCart__zip__c != null]);

		map<string, NSCart__Zip_Code__c> zipMap = new map<string, NSCart__zip_code__c>();
		for (NSCart__Zip_Code__c z : [select id, name, NSCart__route__c, NSCart__sales_tax__c from NSCart__Zip_Code__c]){
			zipmap.put(z.name,z);
		}

		for (NSCart__Route__c r : routeMap.values()){
			if (string.isnotblank(r.NSCart__zip__c)){
				for (string s : r.NSCart__zip__c.split(',')){
					s=s.trim();
					NSCart__Zip_Code__c z = new NSCart__Zip_Code__c();
					if (zipMap.containskey(s)) z = zipMap.get(s);
					z.name = s;
					z.NSCart__route__c = r.id;
					zipMap.put(z.name, z);
				}
			}
		}

		for (NSCart__Sales_Tax__c st : taxmap.values()){
			if (string.isnotblank(st.NSCart__zip__c)){
				for (string s : st.NSCart__zip__c.split(',')){
					s=s.trim();
					NSCart__Zip_Code__c z = new NSCart__Zip_Code__c();
					if (zipMap.containskey(s)) z = zipMap.get(s);
					z.name = s;
					z.NSCart__Sales_Tax__c = st.id;
					zipMap.put(z.name, z);
				}
			}
		}
		NSCart.batch_DML job = new NSCart.batch_DML(zipmap.values(),false);
		database.executeBatch(job);
		//NSCart.batch_DML.startBatch(zipmap.values(),false); //true if deleting	
	}

    public static String queryAllFields(String ObjectName){
		//return only fields
		String queryString='';
		map<String, Schema.SObjectField> M = Schema.getGlobalDescribe().get(objectname.tolowercase()).getDescribe().fields.getMap();
		for (string s : m.keySet()) if (m.get(s).getDescribe().getType() != Schema.Displaytype.LOCATION) queryString += s+', ';
		queryString = queryString.trim();
		queryString = queryString.substring(0,queryString.length()-1);
		return queryString;
	}
}