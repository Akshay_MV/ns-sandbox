public class RecipeController {
    // INITIALIZE ALL VARIABLES
    public List<Website_Filter_Data__c> OurRecipeUpDownText {get;set;}
    public List<Website_Filter_Data__c> RecipeTabs {get;set;}
    public List<Website_Filter__c> RecipeMainTab {get;set;}
    public Map<String,list<Website_Filter_Data__c>> TabDataList {get;set;}
    public List<Website_Filter_Data__c> TabList {get;set;}
    public List<Website_Filter_Data__c> NeedAdviceText {get;set;}//Need Advice TExt
    public List<Website_Filter_Data__c> NeedAdviceImg {get;set;}//Need Advice Images

    // CONSTRUCTION
    public RecipeController() {
        init();
        OurRecipeMethod();
        RecipeTabData();
        NeedAdvice();
    }

    public void init(){
        OurRecipeUpDownText = new List<Website_Filter_Data__c>();
        RecipeTabs = new List<Website_Filter_Data__c>();
        RecipeMainTab = new List<Website_Filter__c>();
        TabDataList = new Map<String,List<Website_Filter_Data__c>>();
        TabList = new List<Website_Filter_Data__c>();
    }

    public void OurRecipeMethod(){
        OurRecipeUpDownText = [SELECT Id, Name, is_Active__c, Height__c, Width__c, Url__c, Text_Data__c, Background_Color__c, Font_Color__c, Font_Size__c, Website_Filter__c,Sub_Text_Data__c,Sub_Font_Color__c,Sub_Font_Size__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Our Recipes Text upper and lower' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
    }

    public void NeedAdvice(){
        NeedAdviceText = new List<Website_Filter_Data__c>();
        NeedAdviceText = [SELECT Id,Text_Data__c,Sub_Text_Data__c,Font_Color__c,Font_Size__c,Sub_Font_Color__c,Sub_Font_Size__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Need Advice Text' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];

        NeedAdviceImg = new List<Website_Filter_Data__c>();
        NeedAdviceImg = [SELECT Id,Url__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Need Advice Images' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
    }

    public void RecipeTabData(){
        // RecipeTabs = [SELECT Id, Name, is_Active__c, Height__c, Width__c, Url__c, Text_Data__c, Background_Color__c, Font_Color__c, Font_Size__c, Website_Filter__c,Sub_Text_Data__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name LIKE '%Recipe Tab%' AND Website_Filter__r.is_Active__c = true ORDER BY Website_Filter__r.Name];
        RecipeMainTab = [SELECT Id,Name,(SELECT Id, Name, is_Active__c, Height__c, Width__c, Url__c, Text_Data__c, Background_Color__c,Background_Color_2__c, Font_Color__c, Font_Size__c, Sub_Text_Data__c,Recipe_RecordType__c,Product_Id__c,Sub_Font_Color__c,Sub_Font_Size__c FROM Website_Filter_Data__r ORDER BY Sequence__c) FROM Website_Filter__c WHERE Name LIKE '%Recipe Tab%' AND is_Active__c = true ORDER BY Name ];        
        // TabList = [SELECT Id,Name,is_Active__c FROM Website_Filter__c WHERE Name LIKE '%Recipe Tab%' AND is_Active__c = true ORDER BY Name];
        TabList = [SELECT Id,Name,Text_Data__c,Recipe_RecordType__c,Product_Id__c,Font_Size__c,Font_Color__c FROM Website_Filter_Data__c WHERE Recipe_RecordType__c = 'Tab' AND Website_Filter__r.Name LIKE '%Recipe Tab%' ORDER BY Website_Filter__r.Name];
        
        for(Website_Filter__c wf: RecipeMainTab){            
            List<Website_Filter_Data__c> wfdList = new List<Website_Filter_Data__c>();
            for(Website_Filter_Data__c wfd: wf.Website_Filter_Data__r){
                if(!TabDataList.containsKey(wf.Name)){
                    List<Website_Filter_Data__c> wfdList1 = new List<Website_Filter_Data__c>();
                    wfdList1.add(wfd);
                    TabDataList.put(wf.Name, wfdList1);
                }else{
                    List<Website_Filter_Data__c> wfdList1 = TabDataList.get(wf.Name);
                    wfdList1.add(wfd);
                    TabDataList.put(wf.name, wfdList1);
                }
                // wfdList.add(wfd);
            }
            // TabDataList.put(wf.Id,wfdList);
        }        

        for(String str: TabDataList.keyset()){
            for(Website_Filter_Data__c wfbmap: TabDataList.get(str)){
                System.debug('Map keyset---- '+str);
                System.debug('list value---- '+wfbmap);
            }
        }
        
    }
}