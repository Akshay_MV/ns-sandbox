public without sharing class coupon_Methods {
/*
	public static map<id, string> couponRecordTypes = getCouponRecordTypes();
	public static string customerEmail {get;set;}
	public static Contact customer {get;set;}

	public static void totaldiscount (string couponcode, IdaShoppingCartHandler.ShoppingCart shoppingCart, IdaOrderHandler handler, string email) {
		
		customerEmail = email;
		
		list<opportunity_Extension.productWrapper> convProdList = new list<opportunity_Extension.productWrapper>();
		
		set<id> shoppingCartProductSet = new set<id>();
		for (idashoppingcarthandler.ShoppingItem sci : shoppingcart.shoppingitems) {
			shoppingCartProductSet.add(sci.productid);
   		}         
		
		map<id,Product2> shoppingCartProducts = new map<id,Product2>([Select p.Weight__c, p.Tax_Class__c, p.SystemModstamp, p.Products_Ordered__c, p.Product_Category__c, p.ProductCode, p.Old_Product_ID__c, p.Name, p.Long_ID__c, p.LastModifiedDate, p.LastModifiedById, p.IsDeleted, p.IsActive, p.Ida_Shipping_Master__c, p.Ida_Sample_Data__c, p.Ida_Product_Position__c, p.Ida_Product_Name__c, p.Ida_Product_Image_Thumbnail__c, p.Ida_Product_Image_Full_Size__c, p.Ida_Place_Order__c, p.Ida_Menu_Item__c, p.Ida_Inventory__c, p.Ida_Include_Default_Upgrade__c, p.Ida_Include_Default_Tab__c, p.Ida_Filter__c, p.Ida_Featured__c, p.Ida_Coupon__c, p.Ida_Category__c, p.Ida_Allow_Back_Order__c, p.Ida_Active__c, p.Id, p.Group__c, p.Family__c, p.Family_Code__c, p.Family_Code2__c, p.Family, p.Description, p.CreatedDate, p.CreatedById From Product2 p where id IN :shoppingCartProductSet]);			
		map<id,PricebookEntry> pbeMap = new map<id,PricebookEntry>([Select p.UseStandardPrice, p.UnitPrice, p.SystemModstamp, p.ProductCode, p.Product2Id, p.Pricebook2Id, p.Name, p.LastModifiedDate, p.LastModifiedById, p.IsDeleted, p.IsActive, p.Id, p.CreatedDate, p.CreatedById From PricebookEntry p where product2id IN :shoppingCartProductSet]);
		
		for (idashoppingcarthandler.ShoppingItem sci : shoppingcart.shoppingitems) {
			opportunity_Extension.productWrapper pw = new opportunity_Extension.productWrapper();
			
			pw.product = shoppingCartProducts.get(sci.productid);		
			pw.pricebookentry = pbeMap.get(sci.PricebookEntryId);
			pw.qty = sci.Quantity;
			pw.lineTotal = pw.priceBookEntry.UnitPrice * pw.qty;
			
			convProdList.add(pw);
		}
		
		decimal tempdiscount = total_discount(couponcode, convProdList, customerEmail); 
		
		if (tempDiscount != 0) {
			try {
				shoppingcart.Coupon = tempDiscount;
				handler.updateCoupon(couponcode, shoppingCart);
			}
			catch (exception e) {
				apexpages.addmessage(new apexpages.message(apexpages.severity.error,'TRACE: ERROR APPLYING COUPON '+couponcode+' - '+e.getmessage()));
			}
		}
	}

	public static decimal total_discount (string couponcode, list<opportunity_Extension.productWrapper> products, string email) {
		Coupon__c coupon;				
		decimal totaldiscount = 0;
		customeremail = email;
		totaldiscount = autoapply(products);
			system.debug('couponcode'+couponcode);
			if (couponcode != null && couponcode.trim().length()>0 && couponcode != 'autoprocess'){
				list<Coupon__c> couponMatch = queryAllFields('Coupon__c','name = \''+couponcode+'\' limit 1');
				if (couponMatch.size()==0){
					apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'Invalid Coupon Code'));
					return totaldiscount;
				}
				else {
					coupon = couponmatch[0];
				}
				system.debug('couponcode: '+couponcode);
				system.debug('totaldiscount: '+totaldiscount);
				system.debug('coupon: '+coupon);
				system.debug('notuseable: '+coupon.not_useable_with_auto_discount__c);
				if (couponcode != 'autoProcess' && (couponMatch == null || couponMatch.size() == 0)) {
					//error
					//apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'Invalid Coupon Code'));
				}
				else if (couponcode != 'autoProcess' && totaldiscount > 0 && coupon.not_useable_with_auto_discount__c==true) {
					apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'This coupon cannot be used with the auto applied discount given.'));
				}
				else if (couponcode != 'autoProcess') {
					coupon = couponMatch[0];   
					boolean couponApplied = false;
					
					// verify coupon applies	
					if (couponApplicable(coupon, products)) {
						totaldiscount += calculateDiscount(coupon, products);
			    		couponApplied = true;
					}
					else {
			    		couponApplied = false;
			    	}
			    	
					if (couponApplied == false) {
						apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'Coupon cannot be applied to this order.'));
					}	    		
				}
			}
		return totaldiscount;
	}
	
	public static boolean couponValid (Coupon__c Coupon, list<opportunity_Extension.productWrapper> products) {
		boolean couponValid = true;
		decimal cartsubtotal = 0;
		for (opportunity_Extension.productWrapper pw : products){
			cartsubtotal += pw.linetotal;
		}
		system.debug('coupon Validation email: '+customeremail);
		if ((couponRecordTypes.get(coupon.recordtypeid) == 'Individual Coupon' || couponRecordTypes.get(coupon.recordtypeid) == 'Credit Coupon' || couponRecordTypes.get(coupon.recordtypeid) == 'Referral Reward') && customerEmail != null && customerEmail.trim().length()>0){
			list<Contact> clist = [select id, email, Email_2__c from Contact where email = :customerEmail OR email_2__c = :customerEmail limit 1]; 
			if (clist.size() > 0) {
				customer = clist[0];
				if (coupon.Customer__c != customer.id) {
					couponValid = false;
					apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'Customer does not match specified customer.'));
				}
			}
			else{
				couponValid = false;
				apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'Customer does not match specified customer.'));
			} 
		}
		if (couponRecordTypes.get(coupon.recordtypeid) == 'New Customer' && customerEmail != null && customerEmail.trim().length()>0 && customerEmail.trim().toLowerCase().startsWith('none@ns')==false) {
			list<Contact> clist = [select id, email, Email_2__c, (select id, Times_Used__c from coupon_users__r where coupon__c = :coupon.id) from Contact where email = :customerEmail OR email_2__c = :customerEmail limit 1]; 
			list<Opportunity> colist = [select id, contact__r.email, contact__r.Email_2__c, (select id from ChargentSFA__Transactions__r) from Opportunity where contact__r.email = :customerEmail OR contact__r.email_2__c = :customerEmail limit 1];
			if (clist.size()>0)	customer = clist[0];
			if (colist.size() > 0 && colist[0].ChargentSFA__Transactions__r.size()>0) {
				
			
			//	decimal timesused = 0;
			//	for (coupon_user__c cu : customer.coupon_users__r){
			//		timesused += (cu.times_used__c==null)?0:cu.times_used__c;
			//	}
			//	if (timesused > coupon.Max_Individual_Uses__c){
					couponValid = false;
			//		apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'You have exceeded your maximum allowed use for this coupon.'));
					apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'This coupon can only be used by new customers.'));
			//	}
			}
		} 
		
		//not available yet
		if (coupon.Start_Date__c != null && system.today() < coupon.start_date__c) couponValid = false; 
		
		//expired
	    if (coupon.expiration_date__c != null && system.today() > coupon.expiration_date__c) couponValid = false;
	    
	    //max uses exceeded
	    if ((coupon.Max_Uses__c != null && coupon.Max_Uses__c > 0 ) && (coupon.Times_Used__c != null  && coupon.Times_Used__c >= coupon.Max_Uses__c)) couponValid = false;

		//minimum order amount not met
		if ((coupon.Min_Order_Amnt_for_Target__c != null && coupon.Min_Order_Amnt_for_Target__c > 0) && coupon.Min_Order_Amnt_for_Target__c > cartsubtotal) couponValid = false;

		return couponValid;
	}
	
	public static decimal calculateDiscount(Coupon__c coupon, list<opportunity_Extension.productWrapper> products){
		decimal couponDiscount = 0;
		decimal cartsubtotal = 0;
		for (opportunity_Extension.productWrapper pw : products){
			cartsubtotal += pw.lineTotal;
		}
		
		set<string> groupSet = new set<string>();
		set<string> familySet = new set<string>();
		if (coupon.Product_Group__c != null){
			for (string s : string.valueof(coupon.get('product_group__c')).split(';')) {
				groupSet.add(s);
			}
		}
		if (coupon.Product_Family__c != null) {
			for (string s : string.valueof(coupon.get('Product_Family__c')).split(';')) {
				familySet.add(s);
			}
		}
		
		if (coupon.discount_type__c == 'Dollar Off') {
			couponDiscount = coupon.Discount_Amount__c;
		}
		else if (coupon.discount_type__c == 'Percent Off') {
			if (coupon.Apply_to_Entire_Order__c == true) couponDiscount = (cartsubtotal*(coupon.Discount_Amount__c/100));
			else {
				for (opportunity_Extension.productWrapper pw : products) {
					if (groupSet.contains(pw.product.group__c) || familySet.contains(pw.product.family)){
						couponDiscount += pw.linetotal*(coupon.Discount_Amount__c/100);
					}
				}
			}
		}
		else if (coupon.discount_type__c == 'BOGO') {
			for (opportunity_Extension.productWrapper pw : products) {
    			if (coupon.Free_Product__c != null && pw.product.id == coupon.Free_Product__c) {
					couponDiscount = pw.pricebookentry.unitPrice;//sci.unitprice;    				
    			}
    		}
		}
		else {
			apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'Coupon has no discount, please contact Nature\'s Select'));
			return 0;
		}
		try {
			coupon.Times_Used__c = coupon.Times_Used__c+1;
			update coupon;
			
			if (couponRecordTypes.get(coupon.recordtypeid) == 'New Customer') {
				list<Coupon_User__c> cuList = [select id, times_used__c from Coupon_User__c where coupon__c = :coupon.id AND customer__c = :customer.id limit 1];
				Coupon_User__c cu;
				if (cuList.size()==0){ 
					cu = new Coupon_User__c(); 	
					cu.customer__c = customer.id;
					cu.coupon__c = coupon.id;
				}
				else {
					cu = culist[0];
				}
				cu.times_used__c = (cu.times_used__c!=null)?cu.times_used__c+1:1;
				upsert cu;
			}
			return couponDiscount;
		}
		catch(exception e) {
			apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'There was an error applying this coupon.'));
			system.debug('TRACE: ERROR ON UPDATE COUPON TIMES USED: '+coupon.name+' - '+e.getmessage());
			return 0;
		}
	}
	
	public static boolean couponApplicable(Coupon__c coupon, list<opportunity_Extension.productWrapper> products) {
		set<string> groupSet = new set<string>();
		set<string> familySet = new set<string>();
		if (coupon.Product_Group__c != null){
			for (string s : string.valueof(coupon.get('product_group__c')).split(';')) {
				groupSet.add(s);
			}
		}
		if (coupon.Product_Family__c != null) {
			for (string s : string.valueof(coupon.get('product_family__c')).split(';')) {
				familySet.add(s);
			}
		}
	
		//specific product
		if (coupon.coupon_type__c == 'Specific Product') {
    		boolean productmatch = false;
    		integer quantityCount = 0;
    		
    		for (opportunity_Extension.productWrapper pw : products) {
    			if (coupon.Product__c != null && pw.Product.Id == coupon.Product__c) {
    				productmatch = true;
    				quantityCount += pw.qty; 
    			}
    		}
    		
    		if (coupon.Quantity_Required__c != null && coupon.Quantity_Required__c >1 && quantityCount >= coupon.Quantity_Required__c && productMatch && couponValid(coupon, products) ) {
    			//specific quantity
    			return true;
    		}
    		else if ((coupon.Quantity_Required__c == null || coupon.Quantity_Required__c <=1) && productMatch && couponValid(coupon, products)) {
    			//any quantity
    			return true;
    		}
		}
		
		//if coupon_Type == 'Product group'	
		else if (coupon.coupon_type__c == 'Product Group') {
			boolean groupMatch = false;
			integer quantityCount = 0;
			//product group logic
			
			for (opportunity_Extension.productWrapper pw : products) {
    			if (groupSet.contains(pw.product.group__c)) {
    				groupMatch = true; 
    				quantityCount+= pw.qty; 
    			}
    		}
			
			if (coupon.Quantity_Required__c != null && coupon.Quantity_Required__c >1 && quantityCount >= coupon.Quantity_Required__c && groupMatch && couponValid(coupon, products)){
				return true;
			}
			else if ((coupon.Quantity_Required__c == null || coupon.Quantity_Required__c <=1) && groupMatch && couponValid(coupon, products)) {
    			return true;
    		}
		}	
		
		
		//if coupon_Type == 'Product family'	
    	else if (coupon.coupon_type__c == 'Product Family') {
			boolean familyMatch = false;
			integer quantityCount = 0;
			//product family logic
			for (opportunity_Extension.productWrapper pw : products) {
    			if (familySet.contains(pw.product.family)) {
    				familyMatch = true;
    				quantityCount+= pw.qty; 
    			}
    		}
			
			if (coupon.Quantity_Required__c != null && coupon.Quantity_Required__c >1 && quantityCount >= coupon.Quantity_Required__c && familyMatch && couponValid(coupon, products)) {
				return true;
			}
			else if ((coupon.Quantity_Required__c == null || coupon.Quantity_Required__c <=1) && familyMatch && couponValid(coupon, products)) {
    			return true;
    		}
		}
		
		
		//if coupon_Type == 'Entire order'	
		else if (coupon.coupon_type__c == 'Entire Order') {
			//logic = minimum met?
			//entire order logic
			if (couponValid(coupon, products)) {
    			return true;
    		}
		}
		
		//if coupon_Type == 'BOGO'	
		else if (coupon.coupon_type__c == 'BOGO') {
			//if both items in cart; and quantities agree, else find max paired count to apply discount
			//bogo logic
			boolean getOneMatch = false;
			boolean buyOneMatch = false;
			
			for (opportunity_Extension.productWrapper pw : products) {
    			if (coupon.Free_Product__c != null && pw.Product.Id == coupon.Free_Product__c) {
    				getOneMatch = true; 
    			}
    			if (coupon.Product__c != null && pw.product.id == coupon.product__c) {
    				buyOneMatch = true;
    			}
    		}
			
			if (buyOneMatch && getOneMatch && couponValid(coupon, products)) {
    			return true;
    		}
		}
		
		//not applicable
		return false;
	}
	
	public static decimal autoApply (list<opportunity_Extension.productWrapper> products){
		decimal autodiscount = 0;
		
		list<Coupon__c> autoCoupons = queryAllFields('Coupon__c','apply_Automatically__c = true order by discount_type__c, discount_amount__c desc');
		
		for (Coupon__c c : autoCoupons) {
			decimal tempdiscount = 0;
			if (couponApplicable (c, products)) {
				tempdiscount = calculateDiscount(c, products);
				
				if (tempdiscount > autodiscount){
					autodiscount = tempdiscount;
				}
			}
		}
		
		return autodiscount;
	}
	
	@isTest (seealldata = true)
	static void coupon_Tests () {
		Pricebook2 PB = [select id from Pricebook2 where isStandard = true];
		Product2 p = new Product2();
		p.Name = 'testprod';
		p.Family = 'f';
		p.Group__c = 'g';
		insert p;
		PricebookEntry pbe = new PricebookEntry();
		pbe.UnitPrice = 5;
		pbe.Pricebook2Id = pb.id;
		pbe.Product2Id = p.id;
		insert pbe;
		
		Coupon__c coup = new Coupon__c();
		coup.Start_Date__c = system.today().adddays(-1);
		coup.Expiration_Date__c = system.today().adddays(20);
		coup.Coupon_Type__c = 'BOGO';
		coup.Discount_Amount__c = 5;
		coup.Discount_Type__c = 'BOGO';
		coup.Free_Product__c = p.id;
		coup.Name = 'testcoupon';
		coup.Product__c = p.id;
		coup.Product_Family__c = 'f';
		coup.Product_Group__c = 'g';
		
		insert coup;
				
		list<opportunity_Extension.productWrapper> testProducts = new list<opportunity_Extension.productWrapper>();
		opportunity_Extension.productWrapper pw = new opportunity_Extension.productWrapper();
		pw.priceBookEntry = pbe;
		pw.product = p;
		pw.qty = 3;
		pw.lineTotal = pw.qty*pw.priceBookEntry.unitprice;
		
		testProducts.add(pw);
		string mail;
		decimal testdiscount = total_discount('testcoupon', testproducts, mail);
		
		coup.Coupon_Type__c = 'Specific Product';
		coup.Discount_Type__c = 'Dollar Off';
		update coup;
		
		coup.Coupon_Type__c = 'Entire Order';
		update coup;
		testdiscount = total_discount('testcoupon', testproducts, mail);
		
		coup.Coupon_Type__c = 'Product Family';
		update coup;
		testdiscount = total_discount('testcoupon', testproducts, mail);
		
		coup.Coupon_Type__c = 'Product Group';
		update coup;
		testdiscount = total_discount('testcoupon', testproducts, mail);
		
		coup.Coupon_Type__c = 'Specific Product';
		coup.Quantity_Required__c = 2;
		update coup;
		testdiscount = total_discount('testcoupon', testproducts, mail);
		
		coup.Coupon_Type__c = 'Entire Order';
		update coup;
		testdiscount = total_discount('testcoupon', testproducts, mail);
		
		coup.Coupon_Type__c = 'Product Family';
		coup.Discount_Type__c = 'Percent Off';
		update coup;
		testdiscount = total_discount('testcoupon', testproducts, mail);
		
		coup.Coupon_Type__c = 'Product Group';
		update coup;
		testdiscount = total_discount('testcoupon', testproducts, mail);
	}
	
	public static void beforeUpsert (list<Coupon__c> newlist, list<Coupon__c> oldlist) {
		for (integer i=0; i<newlist.size(); i++) {
			if (newlist[i].Coupon_Type__c == 'BOGO') {
				newlist[i].Discount_Type__c = 'BOGO';
			}
		}
	}
	
	public static void afterInsert (list<Coupon__c> newlist){
		list<Coupon__c> updateList = new list<Coupon__c>();
		for (integer i=0; i < newlist.size(); i++){
			if (couponRecordTypes.get(newlist[i].recordtypeid) == 'Credit Coupon' || couponRecordTypes.get(newlist[i].recordtypeid) == 'Referral Reward'){
				Coupon__c coup = new Coupon__c(id=newlist[i].id);
				coup.name = newlist[i].name.replace('{auto#}','')+newlist[i].Auto_Number__c;
				updatelist.add(coup);
			}
		}
		if (updatelist.size() > 0) update updatelist;
	}
	
	public static map<id, string> getCouponRecordTypes(){
		map<id, string> output = new map<id, string>();
		for (RecordType r : [select id, name from RecordType where sobjectType ='Coupon__c']){
			output.put(r.id,r.name);
		}
		return output;
	}
	
	public static list<Coupon__c> queryAllFields (string objectName, string filtersLimits) {
		string queryString = 'Select ';
		for (string s : schema.getGlobalDescribe().get(objectname.toLowerCase()).getDescribe().fields.getMap().keySet()){
			queryString += s+', ';
		}
		queryString = queryString.trim();
		queryString = queryString.substring(0,queryString.length()-1);
		queryString+= ' FROM '+objectname;
		if (filtersLimits != null) queryString+= ' where '+filtersLimits;
		
		return database.query(queryString);
	}*/
}