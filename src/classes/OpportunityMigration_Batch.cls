/*
Transfer Opportunity IDA , chargent and legacy fields to NSCart_Fields
dynamic sobject to allow compile without package dependency.
disable workflows and triggers before executing.
*/

global class OpportunityMigration_Batch {
	/*
	//String query='select '+ NSDataMigrationTool.queryAllFields('Opportunity')+' from opportunity';

   String query='select '+ NSDataMigrationTool.queryAllFields('Opportunity')+' from opportunity where createddate<2014-11-21T00:00:00Z';
	
	global Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 

	global OpportunityMigration_Batch() {  
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		list<Opportunity> oppupdates= new list<Opportunity>();
   		for(sObject sOb:Scope){
   			Opportunity opp = (Opportunity)sOb;
            migrateOpportunity(opp);
   			oppUpdates.add(opp);
   		} 
   		update oppUpdates;		
	}
	*/
	global void finish(Database.BatchableContext BC) {
		
	}
/*
   global static opportunity migrateOpportunity(Opportunity opp){
      
      //All fields
      //IDA and chargent fields
      opp.put('NSCart__Balance_Due__c',opp.ChargentSFA__Balance_Due__c);
      opp.put('NSCart__Card_Expiration_Month__c',opp.ChargentSFA__Card_Month__c);
      opp.put('NSCart__Card_Expiration_Year__c',opp.ChargentSFA__Card_Year__c);
      opp.put('NSCart__Card_Name__c',opp.ChargentSFA__Card_Name__c);
      opp.put('NSCart__Card_Number__c',opp.ChargentSFA__Card_Number__c);
      opp.put('NSCart__Card_Security__c',opp.ChargentSFA__Card_Security__c);

      opp.put('NSCart__Card_Type__c',opp.ChargentSFA__Card_Type__c);
      opp.put('NSCart__Charge_Amount__c',opp.ChargentSFA__Charge_Amount__c);
      //opp.put('NSCart__Order_Number__c',opp.ChargentSFA__OrderNumber__c);
      
      opp.put('NSCart__Order_number__c',opp.Order_number__c);
      //ORDER_NUMBER__c????? or ChargentSFA__OrderNumber__c

      opp.put('NSCart__Applied_Coupon_Codes__c',opp.Ida_Applied_Coupon__c);

      if(String.isnotblank(opp.Ida_Billing_Address_Line1__c))
         opp.put('NSCart__Billing_Street_1__c',opp.Ida_Billing_Address_Line1__c);
      else
         opp.put('NSCart__Billing_Street_1__c',opp.ChargentSFA__Billing_Address__c);
   
      opp.put('NSCart__Billing_Street_2__c',opp.Ida_Billing_Address_Line2__c);
      

      if(String.isnotblank(opp.Ida_Billing_City__c))
         opp.put('NSCart__Billing_City__c',opp.Ida_Billing_City__c);
      else
         opp.put('NSCart__Billing_City__c',opp.ChargentSFA__Billing_City__c);


      if(String.isnotblank(opp.Ida_Billing_Country__c))
         opp.put('NSCart__Billing_Country__c',opp.Ida_Billing_Country__c);
      else
         opp.put('NSCart__Billing_Country__c',opp.ChargentSFA__Billing_Country__c);

      if(String.isnotblank(opp.Ida_Billing_Email__c))
            opp.put('NSCart__Billing_Email__c',opp.Ida_Billing_Email__c);
      else
            opp.put('NSCart__Billing_Email__c',opp.ChargentSFA__Billing_Email__c);

      if(String.isnotblank(opp.Ida_Billing_First_Name__c))
            opp.put('NSCart__Billing_First_Name__c',opp.Ida_Billing_First_Name__c);
      else
            opp.put('NSCart__Billing_First_Name__c',opp.ChargentSFA__Billing_First__c);

      if(String.isnotblank(opp.Ida_Billing_Last_Name__c))
            opp.put('NSCart__Billing_Last_Name__c',opp.Ida_Billing_Last_Name__c);
      else
            opp.put('NSCart__Billing_Last_Name__c',opp.ChargentSFA__Billing_Last__c);

      if(String.isnotblank(opp.Ida_Billing_Phone__c))     
            opp.put('NSCart__Billing_Phone__c',opp.Ida_Billing_Phone__c);
      else
            opp.put('NSCart__Billing_Phone__c',opp.ChargentSFA__Billing_Phone__c);


      if(String.isnotblank(opp.Ida_Billing_State__c))
         opp.put('NSCart__Billing_State__c',opp.Ida_Billing_State__c);
      else
         opp.put('NSCart__Billing_State__c',opp.ChargentSFA__Billing_State__c);

      if(String.isnotblank(opp.Ida_Billing_ZipCode__c))
         opp.put('NSCart__Billing_Zipcode__c',opp.Ida_Billing_ZipCode__c);
      else
         opp.put('NSCart__Billing_Zipcode__c',opp.ChargentSFA__Billing_Zip__c);
      if(String.isnotblank(opp.Ida_Business_Name__c))
         opp.put('NSCart__Billing_Company_Name__c',opp.Ida_Business_Name__c);
      else
         opp.put('NSCart__Billing_Company_Name__c',opp.ChargentSFA__Billing_Company__c);


      opp.put('NSCart__Coupon_Amount__c',opp.Ida_Coupon__c);

      opp.put('NSCart__Delivery_Location_Type__c',opp.Ida_Delivery_Location_Type__c);

      opp.put('NSCart__Order_Notes__c',opp.Ida_Message__c);

      if(String.isnotblank(opp.Ida_Shipping_Address_Line1__c))
         opp.put('NSCart__Shipping_Address_Line_1__c',opp.Ida_Shipping_Address_Line1__c);
      else
         opp.put('NSCart__Shipping_Address_Line_1__c',opp.ChargentSFA__Shipping_Address__c);

      opp.put('NSCart__Shipping_Address_Line_2__c',opp.Ida_Shipping_Address_Line2__c);

      if(String.isnotblank(opp.Ida_Shipping_City__c))
         opp.put('NSCart__Shipping_City__c',opp.Ida_Shipping_City__c);
      else
         opp.put('NSCart__Shipping_City__c',opp.ChargentSFA__Shipping_City__c);

    
      opp.put('NSCart__Shipping_Email__c',opp.Ida_Shipping_Email__c);
      

      if(String.isnotblank(opp.Ida_Shipping_First_Name__c))
         opp.put('NSCart__Shipping_First_Name__c',opp.Ida_Shipping_First_Name__c);
      else
         opp.put('NSCart__Shipping_First_Name__c',opp.ChargentSFA__Shipping_First__c);

      if(String.isnotblank(opp.Ida_Shipping_Last_Name__c))
         opp.put('NSCart__Shipping_Last_Name__c',opp.Ida_Shipping_Last_Name__c);
      else
         opp.put('NSCart__Shipping_Last_Name__c',opp.ChargentSFA__Shipping_Last__c);

     
      opp.put('NSCart__Shipping_Phone__c',opp.Ida_Shipping_Phone__c);
     


      if(String.isnotblank(opp.Ida_Shipping_State__c))
         opp.put('NSCart__Shipping_State__c',opp.Ida_Shipping_State__c);
      else
         opp.put('NSCart__Shipping_State__c',opp.ChargentSFA__Shipping_State__c);

      if(String.isnotblank(opp.Ida_Shipping_ZipCode__c))
         opp.put('NSCart__Shipping_Zipcode__c',opp.Ida_Shipping_ZipCode__c);
      else
         opp.put('NSCart__Shipping_Zipcode__c',opp.ChargentSFA__Shipping_Zip__c);

      if(String.isnotblank(opp.Ida_Shipping_Country__c))
         opp.put('NSCart__Shipping_Country__c',opp.Ida_Shipping_Country__c);
      else
         opp.put('NSCart__Shipping_Country__c',opp.ChargentSFA__Shipping_Country__c);



      opp.put('NSCart__Tax__c',opp.Ida_Tax__c);
      //opp.put('NSCart__TotalDiscountTaxOther__c',opp.Ida_Total_Amount__c);
      opp.put('NSCart__Payment_Method__c',opp.ChargentSFA__Payment_Method__c);
      opp.put('NSCart__Payment_Status__c',opp.ChargentSFA__Payment_Status__c);
      //??opp.put('NSCart__',opp.ChargentSFA__Shipping_Amount__c);

      //Custom fields         
      opp.put('NSCart__Alternate_Delivery_Date__c',opp.Scheduled_Order_Date__c);
      opp.put('NSCart__Contact__c',opp.Contact__c);
      opp.put('NSCart__Delivery_Date__c',opp.Delivery_Date__c);
      opp.put('NSCart__Discount__c',opp.Discount__c);
      //opp.put('NSCart__DiscountCurrency__c',opp.Discount_Currency__c);
      //discount_percent
      opp.put('NSCart__Driver_Notes__c',opp.Driver_Notes__c);
      //first_delivery_date_from_Contact__c
      opp.put('NSCart__First_order_for_this_Contact__c',opp.First_Order_This_Contact__c);
      
      //fulfillment_status_2__c
      opp.put('NSCart__Fulfillment_Status__c',opp.Fulfilment_Status__c);

      opp.put('NSCart__Historically_Closed__c',opp.Historically_Closed__c);

      //Last_Delivery_Date_from_contact__c
      //Last_Order_Created_Date__c
      //Net_Sale__c
      //Non_Tax_Other_Charges__c
      //OCN_Order__c
      //Order_Date__c (now a formula on orderplaced)

      //NSCart__Order_PLaced???
      opp.put('NSCart__Order_Placed_Method__c',opp.Order_Placed_Method__c);
      opp.put('NSCart__Other_Charges__c',opp.Other_Charges__c);
      opp.put('NSCart__Payment_Hold__c',opp.Payment_Hold__c);
      
      //Sales_Tax_Currency__c
      
      opp.put('NSCart__Other_Charges__c',opp.Other_Charges__c);
      opp.put('NSCart__Pickup__c',opp.Pickup__c);
      opp.put('NSCart__Stop_Number__c',opp.Stop_Number__c);
      opp.put('NSCart__Subtotal__c',opp.SubTotal__c);
      opp.put('NSCart__Subtotal_with_Tax__c',opp.SubTotal_with_Tax__c);
      opp.put('NSCart__Tax_Percentage__c',opp.Sales_Tax_Percent__c);
      opp.put('NSCart__Use_Information_On_File__c',opp.Use_Information_On_File__c);
      opp.put('NSCart__Payment_Status__c',opp.ChargentSFA__Payment_Status__c);

      
      //New fields 12/2014
      opp.put('NSCart__Check_Number__c',opp.Check_Number__c);
      opp.put('NSCart__Check_Amount__c',opp.Check_Amount__c);
      opp.put('NSCart__Historical_Coupon__c',opp.Historical_Coupon__c);
      //doesnt exist in chicago opp.put('NSCart__Historical_Coupon_Code__c',opp.Historical_Coupon_Code__c);
      opp.put('NSCart__Historical_Delivery__c',opp.Historical_Delivery__c);
      opp.put('NSCart__Historical_Discount__c',opp.Historical_Discount__c);
      opp.put('NSCart__Historical_Other_Charges__c',opp.Historical_Other_Charges__c);
      opp.put('NSCart__Historical_SubTotal__c',opp.Historical_SubTotal__c);
      opp.put('NSCart__Historical_Tax__c',opp.Historical_Tax__c);
      opp.put('NSCart__Historical_Total__c',opp.Historical_Total__c);
      //doesnt exist in chicago opp.put('NSCart__Kibble_Specialist__c',opp.Kibble_Specialist__c);
      //doesnt exist in chicago  opp.put('NSCart__Sales_Tax_Region__c',opp.Sales_Tax_Region__c);
      opp.put('NSCart__Store_Order_Duplicate__c',opp.Store_Order_Duplicate__c);
      //doesnt exist in chicago  opp.put('NSCart__Truck_Route__c',opp.Truck_Route__c);

      

      return opp;
   }

*/
	
}