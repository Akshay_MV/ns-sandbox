public class NSTransGood_Ctrl {
    
    public String ResponseCode {get;set;}
    public String Response {get;set;}
    public String ContactId {get;set;}
    public String AdultOrPuppy {get;set;}
    public Boolean shouldRedirect {public get; private set;}
    public String redirectUrl {public get; private set;}

    public NSTransGood_Ctrl() {
        System.debug('Inside constructor CALLED');
        ResponseCode = ApexPages.currentPage().getParameters().get('ResponseCode');
        ContactId = ApexPages.currentPage().getParameters().get('ContactId');
        Response = ApexPages.currentPage().getParameters().get('Response');
        AdultOrPuppy = ApexPages.currentPage().getParameters().get('AdultOrPuppy');
        shouldRedirect = false;
    }
    public void InitPage() {
        shouldRedirect = true;
        System.debug('Init method called');
        if(Response!=null && Response == 'Update'){
           Contact con = new Contact();
           con.Id=ContactId;
           con.GetM20Multi__c = System.now();
           try{
                update con;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }
        }
        if(ResponseCode!=null && ResponseCode == 'Smooth'){
           //List<Contact> conList = [Select Id, Name,Email from Contact where Id=:ContactId];
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.NCTrans_Good__c=ResponseCode;
           feedback_Obj.Content_Reference__c ='Survey Email';
           feedback_Obj.Contact__c = ContactId;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
                insert feedback_Obj;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }
        }else
        if(ResponseCode!=null && ResponseCode == 'ContactMe'){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.NCTrans_CallASAP__c =ResponseCode;
           feedback_Obj.Content_Reference__c ='Survey Email';
           feedback_Obj.Contact__c = ContactId;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
                insert feedback_Obj;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }     
        }else
        if(ResponseCode!=null && ResponseCode == 'Issues'){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.Content_Reference__c ='Survey Email';
           feedback_Obj.NCTrans_Issues__c =ResponseCode;
           feedback_Obj.Contact__c = ContactId;
           feedback_Obj.Time_Stamp__c =System.now();
           try{
                insert feedback_Obj;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }     
        }
        else
        if(AdultOrPuppy!=null && AdultOrPuppy == 'Adult'){
           System.debug('Inside adult condition');
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.Content_Reference__c = 'Survey Email2CloneB';
           feedback_Obj.MultiPro__c ='MultiPro';
           feedback_Obj.FFS2Multi__c = System.now();
           feedback_Obj.Time_Stamp__c =System.now();
           feedback_Obj.Contact__c = ContactId;
           try{
                insert feedback_Obj;
                redirectUrl = System.Label.AdultRedirectUrl;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }     
        }
        else
        if(AdultOrPuppy!=null && AdultOrPuppy == 'Puppy'){
           Feedback__c feedback_Obj = new Feedback__c();
           feedback_Obj.FFS2High__c  = System.now();
           feedback_Obj.Content_Reference__c = 'Survey Email2CloneB';
           feedback_Obj.HighPro__c ='HighPro';
           feedback_Obj.Time_Stamp__c =System.now();
           feedback_Obj.Contact__c = ContactId;
           try{
                insert feedback_Obj;
                redirectUrl = System.Label.PuppyRedirectUrl;
           }catch(Exception e){
                System.debug('Exception Caught:'+e.getMessage());
           }     
        }
    }
}