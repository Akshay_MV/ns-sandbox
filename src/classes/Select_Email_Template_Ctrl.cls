public class Select_Email_Template_Ctrl{

    public String selectedTemplateId { public get; public set; }
    public String ConId {get;set;}

    public List<SelectOption> getMyPersonalTemplateOptions() {
        List<SelectOption> options = new List<SelectOption>();
        for (EmailTemplate t : [select Id,Name from EmailTemplate where Folder.Name ='Surveys']) {
             options.add(new SelectOption(t.Id,t.Name));
        }
        return options;
    }
    public pageReference SendMail(){
        System.debug('selectedTemplateId====>'+selectedTemplateId);
        ConId=ApexPages.currentPage().getParameters().get('id');
        System.debug('ConId====>'+ConId);
        
        //EmailTemplate templateId = [Select id from EmailTemplate where name = 'Your template Name'];
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateID(selectedTemplateId); 
        mail.setTargetObjectId(ConId);
        mail.setSaveAsActivity(true);
        allmsg.add(mail);
        Messaging.sendEmail(allmsg,false);
        System.debug('Email Sent Successfully!');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Email Sent Successfully!'));
        return null;
   }
   public pageReference Cancel(){

        return new PageReference('javascript:window.close()');
   }  
}