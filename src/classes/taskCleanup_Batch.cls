/**
 * taskCleanup_Batch Class
 * @author: M. Wicherski, MK Partners 11/2012
 * @Description: Clears out open tasks by setting status to food ordered
 */

/*
	To Run:
			taskCleanup_Batch job = new taskCleanup_Batch();
			Id processId = Database.executeBatch(job);
	
	To Check Status:
			AsyncApexJob j = [select ApexClassId, CompletedDate, CreatedById, CreatedDate, 
								Id, JobItemsProcessed, JobType, MethodName, NumberOfErrors, Status, 
								TotalJobItems from AsyncApexJob where Id =:processId];
	
	Info about this Class
			ApexClass ac = [select ApiVersion, Body, BodyCrc, CreatedById, CreatedDate, Id, 
								IsValid, LastModifiedById, LastModifiedDate, LengthWithoutComments, Name, 
								NamespacePrefix, Status, SystemModstamp from ApexClass where Id = :j.ApexClassId];
*/
global with sharing class taskCleanup_Batch  {

	/*public static date runDate = system.today();

	// Queries to be used
	static datetime past2hours = system.now().addhours(-2);

	public static string getQueryString(){
		String x = 'Select ';
		x += 'c.Use_Card__c, ';
		//x += 'c.Total_Consumption_Rate__c, ';
		x += 'c.Title, ';
		x += 'SystemModstamp, ';
		x += 'Second_Email__c, ';
		x += 'Salutation, ';
		x += 'Route__c, ';
		x += 'Route_ID__c, ';
		x += 'ReportsToId, ';
		x += 'Repeat__c, ';
		x += 'Reorder_Rate__c, ';
		x += 'Reminder_Type__c, ';
		x += 'Refferal__c, ';
		x += 'Referring_Affiliate_Biz_Name__c, ';
		x += 'Referral_Name__c, ';
		x += 'Referral_Credited__c, ';
		x += 'ROP_Trigger_Field__c, ';
		x += 'ROP_Trigger_Field_5day__c, ';
		x += 'Primary__c, ';
		x += 'Preferred_Payment_Method__c, ';
		x += 'Phone, ';
		x += 'Pets_Name__c, ';
		x += 'Pet_Type__c, ';
		x += 'Pet4_Type__c, ';
		x += 'Pet4_Name__c, ';
		x += 'Pet3_Type__c, ';
		x += 'Pet3_Name__c, ';
		x += 'Pet2_Type__c, ';
		x += 'Pet2_Name__c, ';
		x += 'Password__c, ';
		x += 'OwnerId, ';
		x += 'OtherStreet, ';
		x += 'OtherState, ';
		x += 'OtherPostalCode, ';
		x += 'OtherPhone, ';
		x += 'OtherCountry, ';
		x += 'OtherCity, ';
		x += 'Old_Customer_ID__c, ';
		x += 'Next_Order_Date__c, ';
		x += 'Newsletter__c, ';
		x += 'Name_of_Biz_Partner__c, ';
		x += 'Name, ';
		x += 'MobilePhone, ';
		x += 'Method__c, ';
		x += 'MasterRecordId, ';
		x += 'MailingStreet, ';
		x += 'MailingState, ';
		x += 'MailingPostalCode, ';
		x += 'MailingCountry, ';
		x += 'MailingCity, ';
		x += 'LeadSource, ';
		x += 'Last_Order_Date__c, ';
		x += 'LastName, ';
		x += 'LastModifiedDate, ';
		x += 'LastModifiedById, ';
		x += 'LastCUUpdateDate, ';
		x += 'LastCURequestDate, ';
		x += 'LastActivityDate, ';
		x += 'KMV_d__c, ';
		x += 'JigsawContactId, ';
		x += 'Jigsaw, ';
		x += 'Issue_Other__c, ';
		x += 'Issue_2__c, ';
		x += 'Issue_2_4__c, ';
		x += 'Issue_2_3__c, ';
		x += 'Issue_2_2__c, ';
		x += 'Issue_1__c, ';
		x += 'Issue_1_4__c, ';
		x += 'Issue_1_3__c, ';
		x += 'Issue_1_2__c, ';
		x += 'IsDeleted, ';
		x += 'Internal_Payment_Notes__c, ';
		x += 'Internal_Customer_Notes__c, ';
		x += 'Id, ';
		x += 'HomePhone, ';
		x += 'Heard_Method__c, ';
		x += 'HasOptedOutOfEmail, ';
		x += 'Gender__c, ';
		x += 'Freq_date__c, ';
		x += 'Freq__c, ';
		x += 'First_Order_Date__c, ';
		x += 'FirstName, ';
		x += 'Fax, ';
		x += 'Email_2__c, ';
		x += 'EmailBouncedReason, ';
		x += 'EmailBouncedDate, ';
		x += 'Email, ';
		x += 'Duplicate_Group__c, ';
		x += 'Duplicate_Checker__c, ';
		x += 'Drop_Locations__c, ';
		x += 'Driver_Delivery_Instructions__c, ';
		x += 'DoNotCall, ';
		x += 'Description, ';
		x += 'Department, ';
		x += 'Days_till_Next_Food__c, ';
		x += 'CreatedDate, ';
		x += 'CreatedById, ';
		x += 'Consumption_Day__c, ';
		x += 'Consumption_Day4__c, ';
		x += 'Consumption_Day3__c, ';
		x += 'Consumption_Day2__c, ';
		x += 'Competitive_Food_Recipe__c, ';
		x += 'Competitive_Food_Brand__c, ';
		x += 'Card_2_Type__c, ';
		x += 'Card_2_Security_Code__c, ';
		x += 'Card_2_Number__c, ';
		x += 'Card_2_Name__c, ';
		x += 'Card_2_Expiration_Year__c, ';
		x += 'Card_2_Expiration_Month__c, ';
		x += 'Card_1_Type__c, ';
		x += 'Card_1_Security_Code__c, ';
		x += 'Card_1_Number__c, ';
		x += 'Card_1_Name__c, ';
		x += 'Card_1_Expiration_Year__c, ';
		x += 'Card_1_Expiration_Month__c, ';
		x += 'Call_Requested__c, ';
		x += 'C_Status__c, ';
		x += 'Breed__c, ';
		x += 'Breed4__c, ';
		x += 'Breed3__c, ';
		x += 'Breed2__c, ';
		x += 'Birthdate, ';
		x += 'Billing_Zip__c, ';
		x += 'Billing_Street__c, ';
		x += 'Billing_State__c, ';
		x += 'Billing_City__c, ';
		x += 'Bank_Routing_Number__c, ';
		x += 'Bank_Name__c, ';
		x += 'Bank_Account_Type__c, ';
		x += 'Bank_Account_Number__c, ';
		x += 'Bank_Account_Name__c, ';
		x += 'Auto_Reorder__c, ';
		x += 'AssistantPhone, ';
		x += 'AssistantName, ';
		x += 'Allow_Cron__c, ';
		x += 'Addtl_Last_Name__c, ';
		x += 'Addtl_First_Name__c, ';
		x += 'Active__c, ';
		x += 'AccountId ';
		x += 'From contact c';
		
		x+= ' where Last_Order_Date__c = TODAY OR Last_Order_Date__c = YESTERDAY';
		
		system.debug('QUERY STRING IS: ' +x);
		return x;
	}

	public static final String DATA_SET_QUERY = getQueryString();

	// Query to fetch the data required by the job
	private String jquery; 

	global taskCleanup_Batch() {
		this(DATA_SET_QUERY);
	}

	public class BatchApexJobException extends Exception {}
	public class InvalidObjectInstanceException extends Exception {}

	global taskCleanup_Batch(String query) {
		try {
	    	// If no query set, throw exception
	    	if (query == null) {
	    		throw new BatchApexJobException('Query string is null.');
	    	}
	    	jquery = query;
	    	system.debug('QUERY RESULT: '+Database.query(jquery));
	    	
	    	//Check if the job is running already
	        ApexClass ac = [select Name from ApexClass where Name = 'taskCleanup_Batch'];
	        List<AsyncApexJob> aajs = [select Status from AsyncApexJob where ApexClassId = :ac.Id and 
	        							JobType = 'batchApex' and Status in ('Queued', 'Processing')];
	        if (!aajs.isEmpty()) {
	        	throw new BatchApexJobException(aajs.size() + ' taskCleanup_Batch job(s) already running or processing.');
			}
    	}
    	catch (Exception e) {
            throw e;
        }
	}

    //Method required by the framework to start the job execution.
    global Database.QueryLocator start(Database.BatchableContext ctx) {
        Database.QueryLocator qLocator = null;
		try {
            qLocator = Database.getQueryLocator(jquery);
        }
        catch (Exception e) {
            throw e;
        }
        return qLocator;
    }

	public static void exec (list<Contact> customers) {
		try {
			set<id> contactIds = new set<id>();
			for (Contact c : customers){
				contactIds.add(c.id);
			}
			
			list<Task> openTasks = [select id from Task where whoid IN :contactIds AND  subject = 'ROP F/Up' AND status != 'Ordered Food']; 
			for (Task t: openTasks){
				t.status = 'Ordered Food';
			}
			
			if (openTasks.size() > 0){
				database.update(openTasks);
			}
		}
		catch (Exception e) {
			throw e; 
		}
	}

    //Method required by the framework for the actual job execution. 
	global void execute(Database.BatchableContext ctx, List<SObject> sobjects) {
		list<Contact> thisList = toCollection(sobjects);
		exec(thislist);
	}

	//Method required and invoked by the framework when a batch job is done.
	global void finish(Database.BatchableContext ctx) {

	}
	
	//Method to convert list of SObjects to a list of opportunities
	public List<Contact> toCollection(SObject[] sobjects) {
		List<Contact> thisList = new List<Contact>();
		for (SObject obj : sobjects) {
			if (obj instanceof Contact) {
				thisList.add((Contact) obj);
			} else {
				throw new InvalidObjectInstanceException('Invalid object instance in cursor.');
			}
		}
		return thisList;
    }*/
}