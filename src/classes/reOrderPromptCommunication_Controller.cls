public with sharing class reOrderPromptCommunication_Controller {

	/*public Id thisId {get;set;}

	public class emailWrapper{
		public String lastDeliveryDate {get;set;}
		public String routeDeliveryDay {get;set;}
		public String neverFeedSuggestion {get;set;}
		public Date nextAvailableDeliveryDate {get;set;}
		public List<String> lastOrderProducts {get;set;}
		public List<String> last12MonthProducts {get;set;}
		public List<String> suggested2Products {get;set;}
		public String Name {get;set;} 
		public boolean AutoReorder {get;set;}
		public Date NextOrderDate {get;set;}
	}

	public emailWrapper getEmailDetails(){
		emailWrapper e = new emailWrapper();
		if ( thisId == null ){
			thisId = apexPages.currentPage().getParameters().get('thisId');
		}
		Date last12Months = system.today().addDays(-365);
		List<Opportunity> opptyQuery = [Select Id, Contact__c,Contact__r.Next_Order_Date__c, Contact__r.Auto_Reorder__c,Contact__r.Name, Contact__r.Route__r.Next_Delivery_Date__c, CloseDate, Delivery_Date__c, Delivery_Day_Of_Week__c,
				(Select UnitPrice, TotalPrice, Tax__c, SortOrder, ServiceDate, Quantity, PricebookEntryId, OpportunityId, ListPrice, Ida_Total_Price__c, Ida_Product__c, Ida_Product_Price__c, Ida_Default_Catalog__c, Ida_Custom_Catalog__c, Id, Description, PriceBookEntry.Product2Id, PriceBookEntry.ProductCode, PricebookEntry.Name from OpportunityLineItems )
			 from Opportunity where Delivery_Date__c >= :last12Months and IsWon = true and Contact__c = :thisId order by CloseDate desc];
		Set<Id> mostRecentProducts = new Set<Id>();
		List<OpportunityLineItem> nonMostRecentProducts = new List<OpportunityLineItem>();

		Opportunity mostRecentOppty = new Opportunity();
		e.lastOrderProducts = new List<String>();
		e.last12MonthProducts = new List<String>();
		e.suggested2Products = new List<String>();
		
		for (Product2 p : [select Id, Name, Ida_Featured__c From Product2 where Ida_Featured__c = 'New Products!']){
			e.suggested2Products.add(p.Name);
		}
		
		for ( Integer i=0;i<opptyQuery.size();i++ ){
			if ( i==0){
				mostRecentOppty = opptyQuery[i];
				e.LastDeliveryDate = mostRecentOppty.Delivery_Date__c.format();
				e.routeDeliveryDay = mostRecentOppty.Delivery_Day_Of_Week__c;
				e.Name = mostRecentOppty.Contact__r.Name; 
				e.AutoReorder = mostRecentOppty.Contact__r.Auto_Reorder__c;
				e.NextOrderDate = mostRecentOppty.Contact__r.Next_Order_Date__c; 
				if ( mostRecentOppty.Contact__r.Route__r.Next_Delivery_Date__c != null){
					e.nextAvailableDeliveryDate = mostRecentOppty.Contact__r.Route__r.Next_Delivery_Date__c;	//need to update this
				}
				for ( OpportunityLineItem oli : opptyQuery[i].OpportunityLineItems ){
					if(!oli.PricebookEntry.Name.startsWith('CS ')){
					e.lastOrderProducts.add(oli.PricebookEntry.Name);
					mostRecentProducts.add(oli.PriceBookEntry.Product2Id);
					}
				}
			} else {
				for ( OpportunityLineItem oli : opptyQuery[i].OpportunityLineItems ){
					if ( !mostRecentProducts.contains(oli.PriceBookEntry.Product2Id) && !oli.PricebookEntry.Name.startsWith('CS ')){
						e.last12MonthProducts.add(oli.PriceBookEntry.Name);
						mostRecentProducts.add(oli.PriceBookEntry.Product2Id);
					}
				}
			}
		}

		List<Web_Content__c> webContentQuery = [Select Id, Name, Value__c from Web_Content__c limit 2000];
		for ( Web_Content__c wc : webContentQuery ){
			if ( wc.Name == 'Never Feed Suggestion' ){
				e.neverFeedSuggestion = wc.Value__c;
			}
		}
		
		return e;
	}*/


}