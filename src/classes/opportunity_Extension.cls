public with sharing class opportunity_Extension {
/*
	public Opportunity oppty {get;set;}
	public Contact contact {get;set;}
	public String shipToAddress {get;set;}
	public String billToAddress {get;set;}
	public String searchTerm {get;set;}
	public List<SelectOption> addressOptions {get;set;}
	public List<SelectOption> deliveryDateOptions {get;set;}
	public List<productWrapper> opptyProducts {get;set;}
	public List<productWrapper> searchResults {get;set;}
	public List<productWrapper> lastOrderItems {get;set;}
	public List<productWrapper> past12MonthsItems {get;set;}
	public Date last12Months = system.today().addDays(-365);
	public Set<Id> past12productset {get;set;}
	public string taxper {get;set;} 
	 
	public class productWrapper {
		public OpportunityLineItem oli {get;set;}
		public Product2 product {get;set;}
		public PricebookEntry priceBookEntry {get;set;}
		public Decimal lineTotal {get;set;}
		public Integer qty {get;set;}
	}


	public opportunity_Extension(ApexPages.StandardController controller){
		this.oppty = (Opportunity)controller.getRecord();
		if ( oppty.CloseDate == null ){
			oppty.CloseDate = system.today();
		}
		if (apexpages.currentpage().getparameters().get('cid')!=null) oppty.contact__c = apexpages.currentpage().getparameters().get('cid');
		queryContact(oppty.Contact__c);
		
		Setting__c orderByTimeDay = Setting__c.getInstance('Order by Time Day');
		Setting__c orderByTimeNext = Setting__c.getInstance('Order by Time Next');
		
		//system.debug('DEBUG: ORDER BY TIME DAY: '+orderByTimeDay.Value__c);
		//system.debug('DEBUG: ORDER BY TIME NEXT: '+orderByTimeNext.Value__c);
		
		deliveryDateOptions = new List<SelectOption>();
        /*Date d = system.today().addDays(1);
        if ( orderByTime != null && orderByTime.Value__c != null && system.now().hour() > integer.valueOf(orderByTime.Value__c) ) {
            d = d.addDays(1);
        }
        for ( Integer i=0;i<14;i++ ){
            deliveryDateOptions.add(new SelectOption(d.format(),d.format()));
            d = d.addDays(1);
        }
        
        if ( contact.MailingPostalCode != null){
        	oppty.Ida_shipping_ZipCode__c = contact.MailingPostalCode;
        }
		
		
        if ( oppty.Ida_Shipping_ZipCode__c != null && oppty.Ida_Shipping_ZipCode__c.trim().length() >= 5 ){
			deliveryDateOptions = idaPaymentController.compileDeliveryDateOptions(oppty.Ida_Shipping_ZipCode__c);


			Map<String,Route__c> zipRouteMap = new Map<String,Route__c>();
			for ( Route__c r : [Select Area__c, Day__c, Id, Next_Delivery_Date__c, Time__c, Zip__c from Route__c where Next_Delivery_Date__c != null]){
				if ( r.Zip__c != null ){
					for ( String z : r.Zip__c.split(',') ){
						zipRouteMap.put(z.trim(),r);
					}
				} 
			}
			system.debug ('oppty zipcode:' +oppty.Ida_Shipping_ZipCode__c);
			String thisZip = '9999901';
			if (oppty.Ida_Shipping_ZipCode__c.trim().length() > 5){
				thisZip = oppty.Ida_Shipping_ZipCode__c.trim().subString(0,7);
			} else {
				thisZip = oppty.Ida_Shipping_ZipCode__c.trim().subString(0,5);
			}
			//String thisZip = oppty.Ida_Shipping_ZipCode__c.trim().subString(0,7);
			Date d;
			d = system.today();
			//d = d.addDays(2);
			date myDate = date.newInstance(1900, 1, 7);
			if ( zipRouteMap.containsKey(thisZip) ){
				//if ( orderByTime != null && orderByTime.Value__c != null && system.now().hourGmt()-8 > integer.valueOf(orderByTime.Value__c) ) {
					//d = d.addDays(1);
				//}
				if (zipRouteMap.get(thisZip).Day__c == 'Next Business Day'){

					//d = d.addDays(1);
					for ( Integer i=0;i<16;i++ ){
						d = zipRouteMap.get(thisZip).Next_Delivery_Date__c.addDays(i*1);
						//system.debug('MATH.MOD IS RETURNING: '+ Math.MOD(d.daysbetween(DATE.valueof(mydate)),7)+' for '+d);
						if ( orderByTimeNext != null && orderByTimeNext.Value__c != null && system.now().hourGmt()-8 > integer.valueOf(orderByTimeNext.Value__c)&& (Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != 0 && Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != -6) ) {
							d = d.addDays(1);
							deliveryDateOptions.add(new SelectOption(d.format(),d.format()));
						} else {
				            if (Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != 0 && Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != -6){
				            	deliveryDateOptions.add(new SelectOption(d.format(),d.format()));
							}
						}
					}
				} else {
					for ( Integer i=0;i<9;i++ ){
						d = zipRouteMap.get(thisZip).Next_Delivery_Date__c.addDays(i*7);
						if ( orderByTimeDay != null && orderByTimeDay.Value__c != null && system.now().hourGmt()-8 > integer.valueOf(orderByTimeDay.Value__c)&& (Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != 0 && Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != -6) ) {
						    deliveryDateOptions.add(new SelectOption(d.format(),d.format()));
						} else {
							if (Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != 0 && Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != -6){
				            	deliveryDateOptions.add(new SelectOption(d.format(),d.format()));
							}
						}
					}
					//if (system.now().hourGmt()-8 < 15) {
						//deliveryDateOptions.add(new SelectOption(system.today().adddays(1).format(),'Tomorrow: ' +system.today().adddays(1).month()+'/'+ system.today().adddays(1).day()+'/'+system.today().adddays(1).year()));
					//}
					//else if (system.now().hourGmt()-8 > 15) {
						//deliveryDateOptions.add(new SelectOption(system.today().adddays(1).format(), 'Next Day: ' +system.today().adddays(2).month()+'/'+system.today().adddays(2).day()+'/'+system.today().adddays(2).year()));
					//} 
				}
			} else {
				d = system.today();
				for ( Integer i=0;i<16;i++ ){
					d = system.today().addDays(i*1);
					//system.debug('MATH.MOD IS RETURNING: '+ Math.MOD(d.daysbetween(DATE.valueof(mydate)),7)+' for '+d);
					if ( orderByTimeNext != null && orderByTimeNext.Value__c != null && system.now().hourGmt()-8 > integer.valueOf(orderByTimeNext.Value__c)&& (Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != 0 && Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != -6) ) {
						d = d.addDays(1);
						deliveryDateOptions.add(new SelectOption(d.format(),d.format()));
			            	
					} else {
			            if (Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != 0 && Math.MOD(d.daysbetween(DATE.valueof(mydate)),7) != -6){
			            	deliveryDateOptions.add(new SelectOption(d.format(),d.format()));
			            	
						}
					}
				}
			}
*/
/*
		} else {
			deliveryDateOptions.add(new SelectOption('','Enter Zip'));
		} 

		opptyProducts = new List<productWrapper>();
		searchResults = new List<productWrapper>();
		lastOrderItems = new List<productWrapper>();
		past12MonthsItems = new List<productWrapper>();
		past12productset = new Set<Id>();
		
		List<Opportunity> opptyQuery = [
			Select Id, Contact__c, Contact__r.Name, Contact__r.Route__r.Next_Delivery_Date__c, CloseDate, Delivery_Date__c, Delivery_Day_Of_Week__c,
				(Select UnitPrice, Tax__c, SortOrder, ServiceDate, Quantity, PricebookEntryId, OpportunityId, ListPrice, Ida_Total_Price__c, Ida_Product__c, Ida_Product_Price__c, Ida_Default_Catalog__c, Ida_Custom_Catalog__c, Id, Description, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Description, PriceBookEntry.Product2.Family, PriceBookEntry.Product2.Group__c, PriceBookEntry.Product2.Name, PriceBookEntry.ProductCode, PricebookEntry.Name, PricebookEntry.IsActive, PriceBookEntry.UnitPrice from OpportunityLineItems )
			from Opportunity where Delivery_Date__c >= :last12Months and IsWon = true and Contact__c = :contact.Id order by CloseDate desc
		];
		if ( opptyQuery.size() > 0 ){
			for ( Integer i=0;i<opptyQuery.size();i++ ){
				for ( OpportunityLineItem oli : opptyQuery[i].OpportunityLineItems ){
					productWrapper pw = create_Wrapper_from_OpportunityLineItem(oli);
					if ( i == 0 ){
						lastOrderItems.add(pw);
					} else {
						//past12MonthsItems = new List<productWrapper>();
						if(!past12productset.contains(pw.product.Id)){
							past12MonthsItems.add(pw);
							past12productset.add(pw.product.Id); 
						}
					}
				}
			}
		}
		searchTerm = '';
		shipToAddress = '';
		billToAddress = '';

	}

	public void queryContact(Id cId){
		List<Contact> contactQuery = [Select Id, AccountId, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, OtherStreet, OtherCity, OtherState, OtherPostalCode, OtherCountry, Route__c, Route__r.Next_Delivery_Date__c, email from Contact where Id =:cId limit 1];
		if ( contactQuery.size() > 0 ){
			contact = contactQuery[0];
		} else {
			contact = new Contact();
		}
		addressOptions = new List<SelectOption>();
		//addressOptions.add(new SelectOption('','--Select One--'));
		if ( contact != null && contact.AccountId != null ){
			oppty.AccountId = contact.AccountId;
		}
		if ( contact != null && contact.MailingStreet != null ){
			addressOptions.add(new SelectOption('mailing',contact.MailingStreet));
			oppty.ChargentSFA__Shipping_Address__c = contact.MailingStreet;
			oppty.ChargentSFA__Shipping_City__c = contact.MailingCity;
			oppty.ChargentSFA__Shipping_State__c = contact.MailingState;
			oppty.ChargentSFA__Shipping_Zip__c = contact.MailingPostalCode;
			
			oppty.ChargentSFA__Billing_Address__c = contact.MailingStreet;
			oppty.ChargentSFA__Billing_City__c = contact.MailingCity;
			oppty.ChargentSFA__Billing_State__c = contact.MailingState;
			oppty.ChargentSFA__Billing_Zip__c = contact.MailingPostalCode;
		}
		if ( contact != null && contact.OtherStreet != null ){
			addressOptions.add(new SelectOption('other',contact.OtherStreet));
		}
	}

	public PageReference updateShipToAddress(){
		if ( shipToAddress == 'mailing' ){
			oppty.ChargentSFA__Shipping_Address__c = contact.MailingStreet;
			oppty.ChargentSFA__Shipping_City__c = contact.MailingCity;
			oppty.ChargentSFA__Shipping_State__c = contact.MailingState;
			oppty.ChargentSFA__Shipping_Zip__c = contact.MailingPostalCode;

		} else
		if ( shipToAddress == 'other' ){
			oppty.ChargentSFA__Shipping_Address__c = contact.OtherStreet;
			oppty.ChargentSFA__Shipping_City__c = contact.OtherCity;
			oppty.ChargentSFA__Shipping_State__c = contact.OtherState;
			oppty.ChargentSFA__Shipping_Zip__c = contact.OtherPostalCode;
		}
		return null;
	}


	public PageReference updateBillToAddress(){
		if ( billToAddress == 'mailing' ){
			oppty.ChargentSFA__Billing_Address__c = contact.MailingStreet;
			oppty.ChargentSFA__Billing_City__c = contact.MailingCity;
			oppty.ChargentSFA__Billing_State__c = contact.MailingState;
			oppty.ChargentSFA__Billing_Zip__c = contact.MailingPostalCode;
		} else
		if ( billToAddress == 'other' ){
			oppty.ChargentSFA__Billing_Address__c = contact.OtherStreet;
			oppty.ChargentSFA__Billing_City__c = contact.OtherCity;
			oppty.ChargentSFA__Billing_State__c = contact.OtherState;
			oppty.ChargentSFA__Billing_Zip__c = contact.OtherPostalCode;
		}
		return null;
	}


	public PageReference searchProducts(){
		searchResults.clear();
		if ( searchTerm == null || searchTerm.trim().length() == 0 ){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter a Search Term'));
		} else {
			String term = '%'+searchTerm+'%';
			for ( Product2 p : [Select Id, Name, isActive, Description, Family, Family_Code__c, Family_Code2__c, Group__c, (Select Id, Name, Pricebook2Id, Product2Id, UnitPrice, IsActive, UseStandardPrice, ProductCode From PricebookEntries where IsActive = true) from Product2 where isActive = true AND ( Name like :term or Family_Code2__c like :term or Family like :term or Group__c like :term or ProductCode like :term or Vendor__c like :term ) order by Name limit 2000]){
				productWrapper pw = new productWrapper();
				pw.product = p;
				for ( PricebookEntry pbe : p.PricebookEntries ){
					pw.oli = new OpportunityLineItem();
					pw.oli.PricebookEntry = pbe;
					pw.priceBookEntry = pbe;
					
				}
				searchResults.add(pw);
			}
		}
		return null;
	}

	public PageReference updateTotals(){
		oppty.Amount = 0;
		List<productWrapper> tempProducts = new List<productWrapper>();
		for ( productWrapper pw : opptyProducts ){
			if ( pw.qty != null && pw.qty > 0 && pw.oli != null && pw.pricebookEntry != null ){
				pw.oli.Quantity = pw.qty;
				pw.lineTotal = pw.oli.Quantity * pw.pricebookEntry.unitPrice; 
				tempProducts.add(pw);
				oppty.Amount += pw.lineTotal;
			}
		}
		opptyProducts = tempProducts;
 
 		oppty.Ida_Coupon__c = 0;
 		oppty.Ida_Coupon__c = coupon_Methods.total_discount('autoProcess', opptyproducts, contact.email);
 		if (oppty.Ida_Applied_Coupon__c != null && oppty.Ida_Applied_Coupon__c.trim().length() > 0) {
			oppty.Ida_Coupon__c = coupon_Methods.total_discount(oppty.Ida_Applied_Coupon__c, opptyproducts, contact.email);
 		}
		oppty.amount -= oppty.Ida_Coupon__c; 
		
		string discounttemp;
		decimal discount; 
		
		if (oppty.Discount__c != null ){
			
			if (oppty.discount__c.contains('%')) {
				discounttemp = oppty.discount__c.substring(0,oppty.discount__c.indexof('%',0));
				discount = decimal.valueof(discounttemp)/100;
				oppty.amount -= oppty.amount*discount;
			}
			else {
				oppty.Amount -= decimal.valueof(oppty.Discount__c);
			}
		}
		//if(Contact.Non_Taxable__c == false){
			oppty.Ida_Tax__c = IdaTaxCalculator.calculateTax(oppty.Amount, oppty.ChargentSFA__Shipping_State__c, oppty.ChargentSFA__Shipping_Zip__c);
		//}else{oppty.Ida_Tax__c = 0;}
		 
		oppty.SubTotal__c = oppty.Amount + oppty.Ida_Tax__c;
		if(oppty.Other_Charges__c != null){
			oppty.ChargentSFA__Charge_Amount__c = oppty.Amount + oppty.Ida_Tax__c + oppty.Other_Charges__c;
		} else {
			oppty.ChargentSFA__Charge_Amount__c = oppty.Amount + oppty.Ida_Tax__c;
		}
		return null;
	}

	public static ProductWrapper cloneProductWraper(ProductWrapper pw){
		ProductWrapper newPW = new ProductWrapper();
		newPW.oli = pw.oli.clone(false,true);
		newPW.qty = pw.qty;
		newPW.priceBookEntry = pw.oli.PriceBookEntry;
		newPW.product = pw.product;
		newPW.lineTotal = pw.oli.Quantity * pw.pricebookEntry.unitPrice;
		return newPW;
	}


	public static ProductWrapper create_Wrapper_from_OpportunityLineItem(OpportunityLineItem oli){
		productWrapper pw = new productWrapper();
		pw.oli = oli.clone(false,true);
		pw.priceBookEntry = oli.PriceBookEntry;
		pw.product = oli.PriceBookEntry.Product2;
		return pw;
	}

	public PageReference addProductsToOrder(){
		for ( ProductWrapper pw : searchResults ){
			if ( pw.Qty != null && pw.Qty > 0){
				pw.oli.Quantity = pw.qty;
			}
			if ( pw.oli.Quantity != null && pw.oli.Quantity > 0 ){
				opptyProducts.add(pw);
			}
		}
		searchResults = new List<productWrapper>();

		for ( ProductWrapper pw : lastOrderItems ){
			if ( pw.Qty != null && pw.Qty > 0){
				pw.oli.Quantity = pw.qty;
			}
			if ( pw.oli.Quantity != null && pw.oli.Quantity > 0 ){
				opptyProducts.add(cloneProductWraper(pw));
			}
			pw.Qty = 0;
		}
		for ( ProductWrapper pw : past12MonthsItems ){
			if ( pw.Qty != null && pw.Qty > 0){
				pw.oli.Quantity = pw.qty;
			}
			if ( pw.oli.Quantity != null && pw.oli.Quantity > 0 ){
				opptyProducts.add(cloneProductWraper(pw));
			}
			pw.Qty = 0;
		}

		updateTotals();
		
		// get tax %
		for ( Ida_Tax__c t : [select Ida_Tax_Percentage__c, Zip__c from Ida_Tax__c where ( State_Abbreviation__c =:oppty.ChargentSFA__Shipping_State__c or Ida_Tax_State__c = :oppty.ChargentSFA__Shipping_State__c) and Ida_Tax_Percentage__c != null]){
    		if (t != null && t.zip__c !=null){
	    		for ( String z : t.Zip__c.split(',') ){
	    			if ( z.trim() == oppty.ChargentSFA__Shipping_Zip__c ){
						taxPer=  '(' + string.valueof( t.Ida_Tax_Percentage__c)+'%)';
	    			}
	    		} 
    		}
		}
		
		return null;
	}
 
	public List<productWrapper> getItemsOnSale(){
		List<productWrapper> results = new List<productWrapper>();
		
		return results;
	}


	public PageReference submitOrder(){
		updateTotals();
		oppty.Name = 'NewOrder'; 
		oppty.StageName = 'Closed Won'; 
		oppty.ChargentSFA__Manual_Charge__c = true; 
		upsert oppty;
		List<OpportunityLineItem> lineItemInserts = new List<OpportunityLineItem>();
		for ( productWrapper pw : opptyProducts ){
			system.debug(pw.oli.unitPrice);
			system.debug(pw.oli.totalPrice);
//			if ( pw.oli.UnitPrice == null ){
				pw.oli.UnitPrice = pw.pricebookEntry.unitPrice;
//			}
			pw.oli.PricebookEntryId = pw.PricebookEntry.Id;
			pw.oli.OpportunityId = oppty.Id;
			lineItemInserts.add(pw.oli);
		}
		insert lineItemInserts;

		return (new ApexPages.StandardController(oppty)).view();
	}

	public PageReference opportunity_New_Onload(){
		Boolean hasContact = false;
		Map<String,String> paramMap = apexPages.currentPage().getParameters();
		for ( String s : paramMap.Values() ){
			if ( s != null && s.length() >= 15 && s.subString(0,3) == '003' ){
				hasContact = true;
			} 
		}
		if ( hasContact ){
			return null;
		} else {
			PageReference p = (new ApexPages.StandardController(oppty)).edit();
			p.getParameters().put('nooverride','1');
			return p;
		}
	}
*/
}