public class ProductListController {

    public List<Product2> ProductList {get;set;}
    
    public List<Website_Filter_Data__c> WellnessSolutionImg {get;set;}//WellnessSolutionImages

    public ProductListController(){
        ProductListMethod();
        WellnessSolution();
    }

    public void ProductListMethod(){
        ProductList = new List<Product2>();
        ProductList = [SELECT Id, Name,Description, Slick__Full_size_image__c, (SELECT Id, UnitPrice FROM PricebookEntries),NSCart__Cart_Subcategory__c FROM Product2 ORDER BY CreatedDate DESC LIMIT 4];
    }
    public void WellnessSolution(){
        WellnessSolutionImg = new List<Website_Filter_Data__c>();
        WellnessSolutionImg = [SELECT Id,Url__c,Text_Data__c FROM Website_Filter_Data__c WHERE Website_Filter__r.Name = 'Wellness Solutions Images' AND Website_Filter__r.is_Active__c = true ORDER BY CreatedDate];
    }
}