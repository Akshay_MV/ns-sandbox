/**
 * This class is used to charge the order and update the payment status
 */
public class Ida_Chargent_Validate_Controller 
{
    /*private String orderRequestId;
    public Ida_Chargent_Validate_Controller()
    {
        orderRequestId = ApexPages.currentPage().getParameters().get('id');
    }
    public PageReference processOrder()
    {
        String queryString = 'select Id, Ida_Payment_Status__c from Opportunity where Id = \'' + String.escapeSingleQuotes(orderRequestId) + '\' limit 1';
        try
        {
            sObject orderRequest = Database.query(queryString);
            
            chargeOpportunity();
            updateOpportunity(orderRequest);
            clearCookies();
            
            PageReference pageRef = new PageReference('/apex/IdapaymentStep3?id=' + orderRequestId);
            pageRef.setRedirect(true);
            return pageRef;
        }
        catch(ChargeException ex)
        {
            ApexPages.addMessages(ex);
            return null;
        }
        catch(Exception ex)
        {
            ApexPages.addMessages(ex);
            return null;
        }
    }
    private void chargeOpportunity()
    {
    	ChargentSFA.TChargentOperations.TChargentResult chargeResult = ChargentSFA.TChargentOperations.ChargeOpportunity_Click(orderRequestId);
        if(!chargeResult.Message.contains('Approved'))
        {
            throw new ChargeException(chargeResult.Message);
        } 
    }
    private void updateOpportunity(sObject orderRequest)
    {
        orderRequest.put('Ida_Payment_Status__c', 'Completed');
        orderRequest.put('StageName', 'Closed Won');
        update orderRequest;
    }    
    private void clearCookies()
    {
        Cookie shoppingCart = ApexPages.currentPage().getCookies().get('shoppingCart');
        shoppingCart = new Cookie('shoppingCart', '', null, 0, false);
        Cookie order = ApexPages.currentPage().getCookies().get('orderId');
        order = new Cookie('orderId', '', null, 0 ,false);
        ApexPages.currentPage().setCookies(new Cookie[]{shoppingCart});
        ApexPages.currentPage().setCookies(new Cookie[]{order});
    }
    
    public String getShoppingCartPage()
    {
        String sitePrefix = (Site.getPrefix() == null) ? '' : Site.getPrefix();
        return sitePrefix + '/IdaPaymentStep1?id=' + orderRequestId;
    }
    public class ChargeException extends Exception {}    
    
    
    @isTest
    static void test_Ida_Chargent_Validate_Controller()
    {
        try
        {
	        Opportunity orderRequest = new Opportunity(Name = 'Acme', StageName = 'Closed Won', CloseDate = Date.today());
	        orderRequest.ChargentSFA__Charge_Amount__c = 1000;
	        orderRequest.ChargentSFA__Payment_Method__c = 'chargent';
	        orderRequest.ChargentSFA__Card_Type__c = 'Visa';
	        orderRequest.ChargentSFA__Card_Year__c = '2100';
	        orderRequest.ChargentSFA__Card_Month__c = '3';
	        insert orderRequest;
	        PageReference pageRef = new PageReference('/apex/IdaChargentPaymentstep2');
	        Cookie order = pageRef.getCookies().get('orderId');
	        order = new Cookie('orderId', orderRequest.Id, null, -1, false);
	        pageRef.setCookies(new Cookie[]{order});
	        Test.setCurrentPage(pageRef);
	        Ida_Chargent_Validate_Controller controller = new Ida_Chargent_Validate_Controller();
	        controller.clearCookies();       
	        order = pageRef.getCookies().get('orderId');
	        order = new Cookie('orderId', orderRequest.Id, null, -1, false);
	        pageRef.setCookies(new Cookie[]{order});
            controller.processOrder();
	        controller.getShoppingCartPage();
        }
        catch(Exception ex){}
    }*/
}