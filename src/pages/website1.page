<apex:page sidebar="false" showHeader="false" >
    
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"/>
    
    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
    <html lang="en" class="no-js">
      <head>
        <title>Tbilisi Photography  Multimedia Museum | TPMM.GE</title>
        <base href="https://tpmm.ge/" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"/>
    
              <link rel="alternate" hreflang="ka" href="https://tpmm.ge/ka/" />
        
        <meta name="google-site-verification" content="DVVfyMnBthPmnHvJpA31j1jmdpkJv-3kg0WEW2GVdzA" />
        <!-- DON'T FORGET TO UPDATE -->
        <meta name="description" content="The Tbilisi Photography  Multimedia Museum is the first institution in Georgia dedicated entirely to contemporary images in all forms: photography, new media and video. "/>
        <meta name="keywords"  content="The Tbilisi Photography  Multimedia Museum, the first institution in Georgia, dedicated entirely to contemporary, photography, new media and video."/>
        <meta name="resource-type" content="document"/>
    
        <meta property="og:title" content="Tbilisi Photography Multimedia Museum | TPMM.GE" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://tpmm.ge/en/" />
        <meta property="og:image" content="https://tpmm.ge/img/facebook-logo.png" />
        <meta property="og:site_name" content="default_site_name" />
    
        <link href="css/main.css" rel="stylesheet" type="text/css" />
        <link rel="canonical" href="https://tpmm.ge/en/" />
        <link rel="icon" type="image/png" href="img/Fav1.ico"/>
    
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css?family=Gabriela" rel="stylesheet"/>
      </head>
      <body class="preload">
    
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=927234504005569";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
    
    
    <div class="page-wrap page-home">
      <div class="disable-landscape"><h6>Please rotate your phone</h6></div>
      <div class="page-content">
        <header>
        <div class="box">
        <a href="https://tpmm.ge/en/" class="logo disable-refresh">
          <div class="logo-part"><img src="img/logo-part-1.svg" alt="Tbilisi Photography & Multimedia Museum" title="Tbilisi Photography & Multimedia Museum" /></div>
          <div class="logo-part"><img src="img/logo-part-2.svg" alt="Tbilisi Photography & Multimedia Museum" title="Tbilisi Photography & Multimedia Museum" /></div>
          <div class="logo-part"><img src="img/logo-part-3.svg" alt="Tbilisi Photography & Multimedia Museum" title="Tbilisi Photography & Multimedia Museum" /></div>
          <div class="logo-part clearfix">
            <div class="part-4"><img src="img/logo-part-4.svg" alt="Tbilisi Photography & Multimedia Museum" title="Tbilisi Photography & Multimedia Museum" /></div>
            <div class="part-5"><img src="img/logo-part-5.svg" alt="Tbilisi Photography & Multimedia Museum" title="Tbilisi Photography & Multimedia Museum" /></div>
            <div class="part-6"><img src="img/logo-part-6.svg" alt="Tbilisi Photography & Multimedia Museum" title="Tbilisi Photography & Multimedia Museum" /></div>
            <div class="part-7"><img src="img/logo-part-7.svg" alt="Tbilisi Photography & Multimedia Museum" title="Tbilisi Photography & Multimedia Museum" /></div>
            <div class="part-8"><img src="img/logo-part-8.svg" alt="Tbilisi Photography & Multimedia Museum" title="Tbilisi Photography & Multimedia Museum" /></div>
          </div>
        </a>
    
        <div class="nav md-none">
          <nav>
                             <a href="https://tpmm.ge/en/" class="item active disable-refresh">Home Page</a>
                                           <a href="https://tpmm.ge/en/museum" class="item  disable-refresh">Museum</a>
                                           <a href="https://tpmm.ge/en/mediatheque/" class="item  disable-refresh">Mediatheque</a>
                                           <a href="https://tpmm.ge/en/phei" class="item  disable-refresh">PHEI</a>
                                          <div class="item sub">
                i-mediatheque			      <div class="dropdown">
                                <a href="https://tpmm.ge/en/video-archive" class="item  disable-refresh">Video Archive</a>
                                <a href="https://tpmm.ge/en/artists" class="item  disable-refresh">Artists</a>
                              </div>
              </div>
                                           <a href="https://tpmm.ge/en/exhibitions" class="item  disable-refresh">Exhibitions</a>
                                           <a href="http://tpmm.ge/en/education" class="item  disable-refresh">Education</a>
                                           <a href="https://tpmm.ge/en/events" class="item  disable-refresh">Events</a>
                                           <a href="https://tpmm.ge/en/blogs" class="item  disable-refresh">DARKROOM</a>
                                           <a href="https://tpmm.ge/en/contact" class="item  disable-refresh">Contact</a>
                              </nav>
        </div>
    
        <nav class="minimized md-none">
                      <a href="https://tpmm.ge/en/" class="item active disable-refresh">Home Page</a>
                                <a href="https://tpmm.ge/en/museum" class="item  disable-refresh">Museum</a>
                                <a href="https://tpmm.ge/en/mediatheque/" class="item  disable-refresh">Mediatheque</a>
                                <a href="https://tpmm.ge/en/phei" class="item  disable-refresh">PHEI</a>
                                <div class="item sub">
              i-mediatheque		      <div class="dropdown">
                            <a href="https://tpmm.ge/en/video-archive" class="item  disable-refresh">Video Archive</a>
                            <a href="https://tpmm.ge/en/artists" class="item  disable-refresh">Artists</a>
                          </div>
            </div>
                                <a href="https://tpmm.ge/en/exhibitions" class="item  disable-refresh">Exhibitions</a>
                                <a href="http://tpmm.ge/en/education" class="item  disable-refresh">Education</a>
                                <a href="https://tpmm.ge/en/events" class="item  disable-refresh">Events</a>
                                <a href="https://tpmm.ge/en/blogs" class="item  disable-refresh">DARKROOM</a>
                                <a href="https://tpmm.ge/en/contact" class="item  disable-refresh">Contact</a>
                      </nav>
    
        <div class="lang md-none">
          <div class="line"></div>
          <p>Switch to <a href="https://tpmm.ge/ka/" class="disable-refresh">Georgian</a></p>
        </div>
    
        <div class="search">
          <img src="img/search-icon.svg" alt="earch" title="Search" />
        </div>
    
        <div class="resp-menu-btn upper xl-none md-block"><span>Menu</span><img src="img/exit-gray.svg" alt="Close" title="Close"/></div>
    
        <div class="resp-menu xl-none md-block">
          <div class="menu">
            <nav>
                                  <a href="https://tpmm.ge/en/" class="item active disable-refresh">Home Page</a>
                                                <a href="https://tpmm.ge/en/museum" class="item  disable-refresh">Museum</a>
                                                <a href="https://tpmm.ge/en/mediatheque/" class="item  disable-refresh">Mediatheque</a>
                                                <a href="https://tpmm.ge/en/phei" class="item  disable-refresh">PHEI</a>
                                                <div class="item sub">
                  i-mediatheque				      <div class="dropdown">
                                    <a href="https://tpmm.ge/en/video-archive" class="item  disable-refresh">Video Archive</a>
                                    <a href="https://tpmm.ge/en/artists" class="item  disable-refresh">Artists</a>
                                  </div>
                </div>
                                                <a href="https://tpmm.ge/en/exhibitions" class="item  disable-refresh">Exhibitions</a>
                                                <a href="http://tpmm.ge/en/education" class="item  disable-refresh">Education</a>
                                                <a href="https://tpmm.ge/en/events" class="item  disable-refresh">Events</a>
                                                <a href="https://tpmm.ge/en/blogs" class="item  disable-refresh">DARKROOM</a>
                                                <a href="https://tpmm.ge/en/contact" class="item  disable-refresh">Contact</a>
                                  </nav>
    
            <div class="lang">
              <p>Switch to <a href="https://tpmm.ge/ka/" class="disable-refresh">Georgian</a></p>
            </div>
          </div>
        </div>
      </div>
    </header>
    <section class="landing">
      <audio controls='autoplay loop' id="bg-audio" style="display: none">
        <source src="img/audio.mp3" type="audio/mpeg"/>
        <source src="img/audio.mp3" type="audio/ogg"/>
      </audio>
    
      <div id="video-wrapper"></div>
    
      <div class="home-tablet-bg xl-none"></div>
      <div class="home-mobile-bg xl-none"></div>
      <a href="https://tpmm.ge/en/museum/" class="home-tablet-logo xl-none parallax">
        <img class="desktop-home-logo-small-2" src="img/landing-white-logo-small-2.svg" alt="" title="" />
        <div class="resp-white-circle layer" data-depth="1.6"><span></span></div>
      </a>
      <a href="https://tpmm.ge/en/museum/" class="home-mobile-logo xl-none parallax">
        <img class="desktop-home-logo-small-2" src="img/landing-white-logo-small-2.svg" alt="" title="" />
        <div class="resp-white-circle layer" data-depth="1.6"><span></span></div>
      </a>
    
      <div class="landing-white-logo">
        <img class="desktop-home-logo" src="img/landing-white-logo.svg" alt="" title="" />
        <img class="desktop-home-logo-small-1" src="img/landing-white-logo-small-1.svg" alt="" title="" />
        <img class="desktop-home-logo-small-2" src="img/landing-white-logo-small-2.svg" alt="" title="" />
      </div>
      <div class="white-circle">
        <div class="circle"></div>
        <div class="circle-img"><span><span></span></span></div>
      </div>
    
      <div class="left-line"></div>
    
      <div class="waves white">
        <div class="line"></div>
        <p>Switch to <a href="https://tpmm.ge/ka/" class="disable-refresh">Georgian</a></p>
        <div class="equalizer equalizer1">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    
      <div class="box">
        <div class="logo-img">
          <a href="https://tpmm.ge/en/museum/" style="display: inline;" class="disable-refresh">
            <img class="transparent-logo desktop-home-logo" src="img/transparent-logo.svg" alt="" title="" />
            <img class="transparent-logo desktop-home-logo-small-1" src="img/transparent-logo-small-1.svg" alt="" title="" />
            <img class="transparent-logo desktop-home-logo-small-2" src="img/transparent-logo-small-2.svg" alt="" title="" />
          </a>
          <div class="bottom"></div>
          <div class="dot"></div>
    
          <div class="waves">
            <div class="line"></div>
            <p>Switch to <a href="https://tpmm.ge/ka/" class="disable-refresh">Georgian</a></p>
            <div class="equalizer equalizer2">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
        </div>
    
        <div class="right-cols clearfix">
                <a href="https://tpmm.ge/en/exhibitions" class="item onload disable-refresh">
            <div class="circle"></div>
            <div class="title">
              <h2 class="upper-2">
                            <span>s</span>
                            <span>n</span>
                            <span>o</span>
                            <span>i</span>
                            <span>t</span>
                            <span>i</span>
                            <span>b</span>
                            <span>i</span>
                            <span>h</span>
                            <span>x</span>
                            <span>E</span>
                          </h2>
            </div>
          </a>
                <a href="https://tpmm.ge/en/video-archive" class="item onload disable-refresh">
            <div class="circle"></div>
            <div class="title">
              <h2 class="upper-2">
                            <span> </span>
                            <span>e</span>
                            <span>u</span>
                            <span>q</span>
                            <span>e</span>
                            <span>h</span>
                            <span>t</span>
                            <span>a</span>
                            <span>i</span>
                            <span>d</span>
                            <span>e</span>
                            <span>m</span>
                            <span>-</span>
                            <span>i</span>
                          </h2>
            </div>
          </a>
                <a href="https://tpmm.ge/en/phei" class="item onload disable-refresh">
            <div class="circle"></div>
            <div class="title">
              <h2 class="upper-2">
                            <span>I</span>
                            <span>E</span>
                            <span>H</span>
                            <span>P</span>
                          </h2>
            </div>
          </a>
                <a href="https://tpmm.ge/en/museum/" class="item onload disable-refresh">
            <div class="circle"></div>
            <div class="title">
              <h2 class="upper-2">
                            <span>m</span>
                            <span>u</span>
                            <span>e</span>
                            <span>s</span>
                            <span>u</span>
                            <span>M</span>
                          </h2>
            </div>
          </a>
                <div class="out-circle"></div>
        </div>
    
        <div class="scroll-arrow noSwipe">
          <div class="line"></div>
          <div class="tringle"></div>
        </div>
      </div>
    
      <div class="white-bg"></div>
    
      <div class="landing-menu noSwipe">
    
        <div class="close noSwipe xl-none sm-block"><img src="img/exit.svg" alt="Close" title="Close"/></div>
    
        <div class="menu-logo noSwipe">
          <div><img src="img/menu-logo.svg" alt="MENU" title="MENU" /></div>
        </div>
    
        <nav>
                       <a href="https://tpmm.ge/en/" class="item active disable-refresh">Home Page</a>
                                <a href="https://tpmm.ge/en/museum" class="item  disable-refresh">Museum</a>
                                <a href="https://tpmm.ge/en/mediatheque/" class="item  disable-refresh">Mediatheque</a>
                                <a href="https://tpmm.ge/en/phei" class="item  disable-refresh">PHEI</a>
                                <div class="item sub">
              i-mediatheque          <div class="dropdown">
                            <a href="https://tpmm.ge/en/video-archive" class="item  disable-refresh">Video Archive</a>
                            <a href="https://tpmm.ge/en/artists" class="item  disable-refresh">Artists</a>
                          </div>
            </div>
                                <a href="https://tpmm.ge/en/exhibitions" class="item  disable-refresh">Exhibitions</a>
                                <a href="http://tpmm.ge/en/education" class="item  disable-refresh">Education</a>
                                <a href="https://tpmm.ge/en/events" class="item  disable-refresh">Events</a>
                                <a href="https://tpmm.ge/en/blogs" class="item  disable-refresh">DARKROOM</a>
                                <a href="https://tpmm.ge/en/contact" class="item  disable-refresh">Contact</a>
                      </nav>
    
        <div class="waves white xl-none sm-block">
          <div class="line"></div>
          <p>Switch to <a href="https://tpmm.ge/ka/" class="disable-refresh">Georgian</a></p>
          <div class="equalizer equalizer3">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      </div>
    
    </section>
    
    <footer class="fadeInUp animated a-dur-12 a-del-15">
      <div class="row">
        <div class="col-xl-3 col-lg-6 col-sm-12">
          <div class="box">
            <h2>Tbilisi Photography Multimedia Museum</h2>
            <address>Stamba Hotel  <br/>14, M. Kostava St. Tbilisi 0108, Georgia</address>
            <a class="phone" href="tel:Office: +995 599 311 768">Office: +995 599 311 768</a>
            <br/>
            <a class="mail" href="mailto:info@tpmm.ge" target="_top">info@tpmm.ge</a>
            <!-- <div class="open-today">Mediatheque working hours
    Monday-Friday 12.00-19.00</div> -->
            <!-- <div class="open-today">Mediatheque working hours
    Monday-Friday 12.00-19.00</div> -->
            <!-- <a href="https://tpmm.ge/en/plan-your-visit" class="privacy-policy" style="color:#6950a2;width:100%;">_plan your visit</a> -->
          </div>
        </div>
            <div class="col-xl-3 col-lg-6 col-sm-12">
          <div class="box">
                                                      <h2>Mediatheque</h2>
                      <div class="visitors-box">
                                                <div class="open-today">Mediatheque is temporarily closed until further notice</div>
                                                              </div>
                          </div>
        </div>
        <div class="footerFixer"></div>
        <div class="col-xl-3 col-lg-6 col-sm-12">
          <div class="box">
            <h2>Terms and conditions</h2>
            <div class="copyright">&copy; 2020 Tbilisi Photography  Multimedia Museum</div>
            <div class="privacy-policy-text">
                      </div>
            <a href="https://tpmm.ge/en/privacy-policy/" class="privacy-policy disable-refresh">privacy-policy</a>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6">
          <div class="box last">
            <h2>Stay in touch</h2>
            <form action="" method="post" class="subscribe">
              <input type="email" required="true" name="email" placeholder="Type your e-mail address" />
              <input type="submit" name="subscribe" value="subscribe" />
            </form>
                    <div class="row">
              <div class="col-xl-8">
                <div class="follow">
                  <p>Follow</p>
                                <a href="https://www.facebook.com/tbilisiphotographyandmultimediamuseum/?ref=bookmarks" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                              <a href="https://twitter.com/tbilisi_pmm" target="_blank"><i class="fab fa-twitter"></i></a>
                                              <a href="https://www.instagram.com/tbilisi_pmm/" target="_blank"><i class="fab fa-instagram"></i></a>
                                              <a href="https://www.youtube.com/channel/UCLuu4FOZ1iDwL6oK8-aHpEQ?view_as=subscriber" target="_blank"><i class="fab fa-youtube"></i></a>
                                            </div>
              </div>
              <div class="col-xl-4 text-right">
                <a href="https://www.connect.ge/" target="_blank" class="connect-logo"><img src="img/connect-logo-black.svg" alt="CONNECT" title="CONNECT" /></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
    
        <script src="js/libs/jquery.js"></script>
        <script src="js/libs/jquery.mobile.js"></script>
        <script src="js/libs/jquery.touchSwipe.min.js"></script>
        <script src="js/libs/smooth-scroll.js"></script>
        <script src="js/libs/bootstrap.min.js"></script>
        <script src="js/libs/owl.carousel.min.js"></script>
        <script src="js/libs/lightgallery.js"></script>
        <script src="js/libs/lg-fullscreen.js"></script>
        <script src="js/libs/lg-video.js"></script>
        <script src="js/libs/lg-autoplay.js"></script>
        <script src="js/libs/lg-thumbnail.js"></script>
        <script src="js/libs/parallax.js"></script>
        <script src="js/custom/events.js"></script>
        <script src="js/custom/load-more.js"></script>
    <script>
      var SITE_URL = "https://tpmm.ge/en/";
    </script>      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
        ga('create', 'UA-123756591-1', 'auto');
        
        ga('send', 'pageview');
        </script>
      
    </div>
    
        </div>
    
      </body>
    </html>
         
      

</apex:page>